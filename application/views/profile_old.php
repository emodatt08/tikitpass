<?php
if($this->session->userdata['logged_in']!=''){
	$email = ($this->session->userdata['logged_in']['email']);
}else{
	redirect('login','refresh');
}
?>
<div class="content_bg">
	<div class="login-bg" style="height:546px;">
    	<div class="login_sec">
         <div style="padding:20px;"></div>
			<?php 
            if($this->session->flashdata('email_sent') !=""){
            ?>
            <b style="color:#F00; margin-left: 25px;">
            <?php 	
            echo $this->session->flashdata('email_sent');
            ?>
            </b>
            <?php
            } 
            ?>
         <?php foreach($profile as $pr) {?>
        	<div class="col-md-12">
            
            		<div class="col-md-3">
                        <strong>
                        <?php 
                        if($this->session->flashdata('propic_change') !=""){
						?>
                        <p style="color:#0F0; margin-left: 25px;">
                        <?php 	
                        echo $this->session->flashdata('propic_change');
                        ?>
						</p>
                        <?php
                        } 
                        ?>
                        </strong>
                    	<div class="box" align="center">
                        <form method="post" action="<?php echo base_url();?>profile/changeprofileimage" id="uploadFileForm" enctype="multipart/form-data">
            			<input type="hidden" name="oldimg" value="<?php echo $pr->imagename?>" />
                        <?php if($pr->imagename == '') {?>
                        <img src="<?php echo base_url()?>uploads/nopic.png" style="height: 170px; width: 100%;"/>
                        <?php } else {?>
                        <img src="<?php echo base_url()?>uploads/profileimage/<?php echo $pr->imagename?>" style="height: 170px; width: 100%;"/>
                        <?php }?>
                        <div class="fileUpload btn btn-primary" align="center">
                        <span>Upload</span>
                       	<input type="file" class="upload" name="userfile" id="file-2"/>
                        </div>
                        </form> 
                        <?php $this->load->view('sidebar');?>
                    </div>
                    </div>
                   
                    <div class="col-md-9">
                    	<div class="box"><?php echo $pr->description?></div>
                        <div class="box boxnewtwo">
                        	<p>Phone :<?php echo $pr->phone?> </p>
                            <p>Email : <?php echo $pr->email?> </p>
                            <p>Address :  <?php echo $pr->adrs?> </p>
                        </div>
						
                        <table class="table box">
                              <thead>
                                <tr>
                                  <th>Sl no.</th>
                                  <th>Event Name</th>
                                  <th>Event Date</th>
                                  <th>Event Time</th>
                                  <th>Ticket Type</th>
                                  <th>Ticket</th>
                                </tr>
                              </thead>
							<?php 
                            $ctn = 1;
							if(@$num_rows_tkt>0){
                            foreach($ticket as $tkt){ 
                            ?>
                              <tbody>
                                <tr>
                                  <th scope="row"><?php echo $ctn; ?></th>
                                  <?php foreach($eventdet as $evd){ ?>
                                  <td><?php echo $evd->event_name; ?></td>
                                  <td><?=date('Y-m-d', strtotime($evd->start_date_time))?></td>
                                  <td><?=date('h:i a', strtotime($evd->start_date_time))?></td>
                                  <?php } ?>
                                  <td><?php if($tkt->ticket_type==0){ echo "Free"; } else { echo "Paid"; } ?></td>
                                  <?php 
								  $ticketid = $tkt->tkt_id;
								  $result = $this->home_model->show_pdf($ticketid);
								  foreach($result as $pdf){ 
								  ?>
                                  <td><a  href="<?php echo base_url(); ?>uploads/ticketpdf/<?php echo $pdf->tckt_file; ?>" style="text-decoration:none;" ><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                 
                                
								  
                                  </td>
                                  <?php }?>
                                </tr>
                              </tbody>
                              <?php $ctn++;} }else{
								echo "<tbody><th><b style='color:#40e0d0;'>No Records Avialable!!!</b></th></tbody>";
							}
                            ?>
                            </table>
                    </div>
            </div>
         <?php } ?>   
        	<div style="padding:20px;"></div>
        </div>
    </div>
 </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#file-2").change(function() {
		$("#uploadFileForm").submit();
	});

});
</script>