<?php //$this->load->view ('header');?>
<?php 
 $usertype = $this->session->userdata['logged']['log_type'];
 if($usertype=='csa'){
	 redirect('administrator/home');
 }
 ?>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript">
$(function() {
setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
$('#btnclick').click(function() {
$('#testdiv').show();
setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
})
})
</script>
<!-- BEGIN CONTAINER -->

<div class="page-container">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
      <?php $this->load->view ('administrator/sidebar');?>
      <!-- END SIDEBAR MENU -->
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->
      <!-- END THEME PANEL -->
      <!-- BEGIN PAGE BAR -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>administrator panel</span> </li>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions <i class="fa fa-angle-down"></i> </button>
            <ul class="dropdown-menu pull-right" role="menu">
              <li> <a href="#"> <i class="icon-bell"></i> Action</a> </li>
              <li> <a href="#"> <i class="icon-shield"></i> Another action</a> </li>
              <li> <a href="#"> <i class="icon-user"></i> Something else here</a> </li>
              <li class="divider"> </li>
              <li> <a href="#"> <i class="icon-bag"></i> Separated link</a> </li>
            </ul>
          </div>
        </div>
      </div>
      <?php if (isset($success_msg)) { echo $success_msg; } ?>
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Add banner</div>
                    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form">
                    <form action="<?php echo base_url().'administrator/organizer/add_organizer' ?>" class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
                      <div class="form-body">
                      <!--<div class="form-group">
                          <label class="control-label col-md-3">Banner Image</label>
                          <div class="col-md-8">
                            <?php
								$file = array('id' => 'banner', 'type' => 'file','name' => 'userfile', 'onchange' => 'readURL(this);' );
								echo form_input($file);
            				?>
                            <span class="text-danger">
                            <?php if (isset($error)) { echo $error; } ?>
                            </span> </div>
                        </div>-->
                      
                      <div class="form-group last">
                                                <label class="control-label col-md-3">Profile Image</label>
                                                <div class="col-md-9">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                   
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                          
                                                            </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <!--<input type="file" name="...">-->
																<?php
                                                                $file = array('id' => 'banner', 'type' => 'file','name' => 'userfile', 'onchange' => 'readURL(this);' );
                                                                echo form_input($file);
                                                                ?>
                                                            </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix margin-top-10">
                                                        <span class="label label-danger">Format</span> jpg, jpeg, png, gif </div>
                                                </div>
                                            </div>
                        <div class="form-group">
                          <label class="control-label col-md-3"> Organizer Name</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'organizer_name', 'name' => 'organizer_name','class'=>'form-control','onkeyup'=>'leftTrim(this)' )); ?> <?php echo form_error('organizer_name'); ?> 
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3"> Organizer Phone</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'organizer_phone', 'name' => 'organizer_phone','class'=>'form-control','onkeyup'=>'leftTrim(this)' )); ?> 
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3"> Organizer Email</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'organizer_email', 'name' => 'organizer_email','class'=>'form-control','onkeyup'=>'leftTrim(this)' )); ?> <?php echo form_error('organizer_email'); ?> 
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Organizer Address</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'organizer_address', 'name' => 'organizer_address','class'=>'form-control','onkeyup'=>'leftTrim(this)' )); ?> 
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3"> Organizer Category</label>
                          <div class="col-md-8"> 
                          <select class="form-control">
                          <?php foreach($category as $j){?>
                              <option value="<?php echo $j->category_id?>"><?php echo $j->category_name?></option>
                              <?php }?>
						 </select>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3"> Organizer Website</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'organiser_website', 'name' => 'organiser_website','class'=>'form-control','onkeyup'=>'leftTrim(this)' )); ?> 
                          </div>
                        </div>
                        
                        
                      </div>
                      <div class="form-actions">
                        <div class="row">
                          <div class="col-md-offset-3 col-md-9"> <?php echo form_submit(array('id' => 'submit', 'name' => 'Submit' , 'value' => 'Submit' ,'class'=>'btn red')); ?>
                            <button type="button" class="btn default">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </form>
                    <script>
						$('INPUT[type="file"]').change(function () {
							var ext = this.value.match(/\.(.+)$/)[1];
							switch (ext) {
							case 'jpg':
							case 'jpeg':
							case 'png':
							case 'gif':
							$('#banner').attr('disabled', false);
							break;
							default:
							alert('This is not an allowed file type.');
							this.value = '';
							}
						});
						function leftTrim(element){
							if(element)
							element.value=element.value.replace(/^\s+/,"");
						}
            		</script>
                    <!-- END FORM-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
