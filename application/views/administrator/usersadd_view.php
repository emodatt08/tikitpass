<?php //$this->load->view ('header');?>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript">
$(function() {
setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
$('#btnclick').click(function() {
$('#testdiv').show();
setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
})
})
</script>
<div class="page-container">
  <div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
      <?php $this->load->view ('administrator/sidebar');?>
    </div>
  </div>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="<?php echo base_url(); ?>administrator/user/dashboard">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>Admin panel</span> </li>
        </ul>
      </div>
      <div class="alert alert-success alert-dismissable" style="padding:10px;">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>
        <strong>
        <?php 
				$last = end($this->uri->segments); 
				if($last=="success"){echo "Data Added Successfully ......";}
				if($last=="successdelete"){echo "Data Deleted Successfully ......";}
            ?>
        </strong> </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Add User</div>
                    <div class="tools"> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form action="<?php echo base_url().'administrator/users/add_users' ?>" class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                    
                    <!--<div class="form-group">
                    <label class="control-label col-md-3">Profile Image</label>
                    <div class="col-md-8">
                    <?php
                    $file = array('type' => 'file','name' => 'userfile' ,'id' => 'userfile' ,'onchange' => 'readURL(this);');
                    echo form_input($file);
                    ?>
                    <span class="text-danger"><?php if (isset($error)) { echo $error; } ?></span>
                    </div>
                    </div>-->
                    
                    <div class="form-group last">
                                                <label class="control-label col-md-3">Profile Image</label>
                                                <div class="col-md-9">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                   
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                          
                                                            </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <!--<input type="file" name="...">-->
																	<?php
                                                                    $file = array('type' => 'file','name' => 'userfile' ,'id' => 'userfile' ,'onchange' => 'readURL(this);');
                                                                    echo form_input($file);
                                                                    ?> 
                                                            </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix margin-top-10">
                                                        <span class="label label-danger">Format</span> jpg, jpeg, png, gif </div>
                                                </div>
                                            </div>
                    
                    <div class="form-group">
                    <label class="control-label col-md-3">Name</label>
                    <div class="col-md-8"> <?php echo form_input(array('id' => 'name', 'name' => 'name','class'=>'form-control')); ?>
                    <?php echo form_error('name'); ?>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label col-md-3">Phone</label>
                    <div class="col-md-8"> <?php echo form_input(array('id' => 'phone', 'name' => 'phone','class'=>'form-control')); ?>
                    <?php echo form_error('phone'); ?>
                    </div>
                    </div>
                    
                    <div class="form-group">
                    <label class="control-label col-md-3">Email</label>
                    <div class="col-md-8"> <?php echo form_input(array('id' => 'email', 'name' => 'email','type' => 'email', 'class'=>'form-control')); ?>
                    <?php echo form_error('email'); ?>
                    </div>
                    </div>
                    
                    <div class="form-group">
                    <label class="control-label col-md-3">Address</label>
                    <div class="col-md-8"> <?php echo form_textarea(array('id' => 'address', 'name' => 'address', 'class'=>'form-control')); ?>
                    <?php echo form_error('address'); ?>
                    </div>
                    </div>
                    
                    <div class="form-group">
                    <label class="control-label col-md-3">Password</label>
                    <div class="col-md-8"> <?php echo form_input(array('id' => 'password', 'name' => 'password', 'type' => 'password','class'=>'form-control')); ?>
                    <?php echo form_error('password'); ?>
                    </div>
                    </div>
                    
                    <div class="form-group">
                    <label class="control-label col-md-3">Username</label>
                    <div class="col-md-8"> <?php echo form_input(array('id' => 'username', 'name' => 'username','class'=>'form-control')); ?>
                    <?php echo form_error('username'); ?>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label col-md-3">Description</label>
                    <div class="col-md-8"> <?php echo form_textarea(array('id' => 'pagedes', 'name' => 'description','class'=>'form-control')); ?>
                    <?php echo form_error('description'); ?>
                    </div>
                    </div>
                    
                    <div class="form-group">
                    <label class="control-label col-md-3">User Type</label>
                    <div class="col-md-4">
                    <label class="control-label col-md-3">Vendor</label>  
                    <?php
                    $data = array(
                    'name'          => 'user_type',
                    'id'            => 'user_type',
                    'value'         => 'vendor',
                    'checked'       => FALSE
                    );
                    echo form_radio($data);
                    ?> 
                     <?php echo form_error('user_type'); ?>
                    </div>
                   
                    <div class="col-md-4">
                    <label class="control-label col-md-3">User</label>  
                    <?php
                    $datausr = array(
                    'name'          => 'user_type',
                    'id'            => 'user_type',
                    'value'         => 'user',
                    'checked'       => FALSE
                    );
                    echo form_radio($datausr);
                    ?> 
                    <?php echo form_error('user_type'); ?>
                    </div>
                    </div>
                    
                    </div>
                    <div class="form-actions">
                    <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                    <!--<button type="submit" class="btn red" name="submit" value="Submit"> <i class="fa fa-check"></i> Submit</button>-->
                    <?php echo form_submit(array('id' => 'submit', 'value' => 'Submit' ,'class'=>'btn red')); ?>
                    <button type="button" class="btn default">Cancel</button>
                    </div>
                    </div>
                    </div>
                    </form>
					<script>
                    $('INPUT[type="file"]').change(function () {
                    var ext = this.value.match(/\.(.+)$/)[1];
                    switch (ext) {
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'gif':
                    $('#userfile').attr('disabled', false);
                    break;
                    default:
                    alert('This is not an allowed file type.');
                    this.value = '';
                    }
                    });
                    </script>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php //$this->load->view ('footer');?>
