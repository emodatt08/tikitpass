<?php //$this->load->view ('header');?>

<div class="page-container">
  <div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
      <?php $this->load->view('administrator/sidebar');?>
    </div>
  </div>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="<?php echo base_url(); ?>administrator/user/dashboard">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>Admin Panel</span> </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>User View</div>
                    <div class="tools"> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php foreach($viewusers as $i){?>
                    <form method="post" class="form-horizontal form-bordered" action="<?php echo base_url().'administrator/user/edit_user '?>" enctype="multipart/form-data">
                      <div class="form-body">
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Profile Image</label>
                          <div class="col-md-7">
							
                            <div id='default_img'><img id="select" src="<?php echo base_url()?>uploads/profileimage/<?php echo $i->imagename;?>" alt="your image" style="width:150px;"/></div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Name</label>
                          <div class="col-md-5">
                            <?php echo $i->name;?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Phone No</label>
                          <div class="col-md-5">
                            <?php echo $i->phone;?>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Email</label>
                          <div class="col-md-5">
                            <?php echo $i->email;?>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Password</label>
                          <div class="col-md-5">
                            <?php echo base64_decode($i->password);?>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Username</label>
                          <div class="col-md-5">
                            <?php echo $i->username;?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Descriptions</label>
                          <div class="col-md-7">
                            <?php echo $i->description;?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Address</label>
                          <div class="col-md-7">
                            <?php echo $i->adrs;?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">User Type</label>
                          <div class="col-md-7">
                            <?php if($i->user_type=="o"){?><b style="color:#F00">Vendor</b><?php }?>
                            <?php if($i->user_type=="u"){?><b style="color:#0C0">User</b><?php }?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">User Status</label>
                          <div class="col-md-7">
                            <?php if($i->userstatus=='0'){?><b style="color:#F00">Inactive</b><?php }?>
                            <?php if($i->userstatus=='1'){?><b style="color:#0C0">Active</b><?php }?>
                          </div>
                        </div>
                      </div>
                    </form>
                    <?php }?>
                    <!-- END FORM-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
