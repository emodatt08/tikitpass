<?php //$this->load->view ('header');?>
<!-- BEGIN CONTAINER -->
<link href="<?php echo base_url(); ?>css/datepicker.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,300,500&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.9.1/highlight.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>js/datepicker.js"></script>
<script src="<?php echo base_url(); ?>js/datepicker.en.js"></script>
<div class="page-container">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
      <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
      <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
      <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
      <?php $this->load->view ('administrator/sidebar');?>
      <!-- END SIDEBAR MENU -->
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <!-- BEGIN PAGE BAR -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>administrator Panel</span> </li>
        </ul>
      </div>
      <!-- END PAGE BAR -->
      <!-- BEGIN PAGE TITLE-->
      <!-- END PAGE TITLE-->
      <!-- END PAGE HEADER-->
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Ticket edit</div>
                    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php foreach($eticket as $i){?>
                    <form method="post" class="form-horizontal form-bordered" action="<?php echo base_url()?>administrator/ticket/edit_ticket" enctype="multipart/form-data">
                      <div class="form-body">
                        <input type="hidden" name="ticket_id" value="<?=$i->id;?>">
                        <input type="hidden" name="urlid" value="<?php echo $urlid?>" />
                        <div class="form-group">
                          <label class="control-label col-md-3">Ticket Name</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="ticket_name" value="<?php echo $i->ticket_name;?>" name="ticket_name">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Ticket Price</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="ticket_price" value="<?php echo $i->ticket_price;?>" name="ticket_price">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Ticket Quantity</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="ticket_quantity" value="<?php echo $i->ticket_quantity;?>" name="ticket_quantity">
                          </div>
                        </div>
						<div class="form-group">
                          <label class="control-label col-md-3">Ticket Description</label>
                          <div class="col-md-5">
                            <textarea type="text" class="form-control" id="pagedes" name="ticket_description"><?php echo $i->ticket_description;?></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Status</label>
                          <div class="col-md-5">
                            <select name="status" class="form-control">
                               <option value="1" <?php if($i->status==1){?> selected="selected" <?php }?>>Active</option>
                               <option value="0" <?php if($i->status==0){?> selected="selected" <?php }?>>Inactive</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-actions">
                        <div class="row">
                          <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn red" id="submit" value="Submit" name="update">
                            <button class="btn default" type="button">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </form>
                    <?php }?>
                    <!-- END FORM-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
