<!-- BEGIN CONTAINER -->
<div class="page-container">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
      <!-- BEGIN SIDEBAR MENU -->
      <?php $this->load->view ('administrator/sidebar');?>
      <!-- END SIDEBAR MENU -->
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
  </div>
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <!-- BEGIN PAGE BAR -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="<?php echo base_url(); ?>administrator/user/dashboard">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>administrator Panel</span> </li>
        </ul>
      </div>
      <!-- END PAGE HEADER-->
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Gallery edit</div>
                    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php foreach($emember as $i){?>
                    <form method="post" class="form-horizontal form-bordered" action="<?php echo base_url().'administrator/memberdata/edit_member'?>" enctype="multipart/form-data" onsubmit="return Submit()">
                      <div class="form-body">
                        <input type="hidden" name="member_id" value="<?=$i->member_id;?>">
                        <input type="hidden" name="old_file" value="<?=$i->user_avatar;?>">
                        <div class="form-group">
                          <label class="control-label col-md-3">Member Avatar</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="gallery_name" value="<?php echo $i->user_avatar;?>" name="gallery_name" onkeyup="leftTrim(this)" />
                            <label id="errorBox"></label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Image Upload</label>
                          <div class="col-md-7">
                            <?php
								$file = array('type' => 'file','name' => 'userfile','id'=>'chkimg','onchange'=>'checkFile(this);');
								echo form_input($file);
                    		?>
                            <label>Please select jpg, png, jpeg or gif file.</label>
                            <?php //echo form_input(array('id' => 'name', 'name'=>'image','type' =>'file' ,'class'=>'form-control fileimage')); ?>
                            <div id='default_img'><img id="select" src="<?php echo base_url()?>/memberavatar/<?php echo $i->user_avatar;?>" alt="your image" style="width:150px;"/></div>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">First Name</label>
                          <div class="col-md-8"> <input type="text" name="first_name" value="<?php echo $i->first_name;?>" class="form-control" id="first_name" onkeyup="leftTrim(this)" />
                            <label id="errorBox2"></label>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Last Name</label>
                          <div class="col-md-8"> <input type="text" name="last_name" value="<?php echo $i->last_name;?>" class="form-control" id="last_name" onkeyup="leftTrim(this)" />
                            <label id="errorBox3"></label>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">DOB</label>
                          <div class="col-md-8"> <input type="text" name="dob" value="<?php echo date('d-m-Y',strtotime($i->dob));?>" class="form-control" id="dob" onkeyup="leftTrim(this)" />
                            <label id="errorBox4"></label>
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="control-label col-md-3">User Name</label>
                          <div class="col-md-8"> <input type="text"  readonly="readonly" name="username" value="<?php echo $i->username;?>" class="form-control" id="username" onkeyup="leftTrim(this)" />
                            <label id="errorBox5"></label>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Gender</label>
                          <div class="col-md-8"> 
                          <input type="radio" name="gender" value="<?php echo $i->gender;?>" <?php if($i->gender=='M'){?> checked="checked" <?php }?> /> Male &nbsp;
                          <input type="radio" name="gender" value="<?php echo $i->gender;?>" <?php if($i->gender=='F'){?> checked="checked" <?php }?> /> Female
                            <label id="errorBox5"></label>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Address</label>
                          <div class="col-md-8"> 
                          <input type="text" name="address" value="<?php echo $i->address;?>" class="form-control"/> 
                            <label id="errorBox6"></label>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">City</label>
                          <div class="col-md-8"> 
                          <input type="text" name="city" value="<?php echo $i->city;?>" class="form-control"/> 
                            <label id="errorBox6"></label>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Status</label>
                          <div class="col-md-8"> 
                          <select name="status">
                          	<option value="Yes" <?php if($i->status=='Yes'){?> selected="selected"<?php }?>>Active</option> 
                            <option value="No" <?php if($i->status=='No'){?> selected="selected"<?php }?>>Inactive</option> 
                          </select>
                          </div>
                        </div>
                        
                      </div>
                      <div class="form-actions">
                        <div class="row">
                          <div class="col-md-offset-3 col-md-9">
                            <!--<button type="submit" class="btn red" name="submit" value="Submit"> <i class="fa fa-check"></i> Submit</button>-->
                            <input type="submit" class="btn red" id="submit" value="Submit" name="update">
                            <a href="<?php echo base_url();?>gallery/show_gallery">
                            <button type="button" class="btn default">Cancel</button>
                            </a> </div>
                        </div>
                      </div>
                    </form>
                    <?php }?>
                    <!-- END FORM-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <!-- END QUICK SIDEBAR -->
</div>
<script>
		function Submit(){
		if($("#gallery_name").val() == "" ){
		$("#gallery_name").focus();
		$("#errorBox").html("Please Enter Gallery Name");
		return false;
		}else{
		$("#errorBox").html("");  
		}
		
		}
		
		function leftTrim(element){
		if(element)
		element.value=element.value.replace(/^\s+/,"");
		}
		
		$('INPUT[type="file"]').change(function () {
		// alert("jjjjj");
		var ext = this.value.match(/\.(.+)$/)[1];
		switch (ext) {
		case 'jpg':
		case 'jpeg':
		case 'png':
		case 'gif':
		$('#chkimg').attr('disabled', false);
		break;
		default:
		alert('This is not an allowed file type.');
		this.value = '';
		}
		});
</script>
<style>
#errorBox{
 color:#F00;
 }
 
 #errorBox1{
 color:#F00;
 }
 
 #errorBox2{
 color:#F00;
 }
</style>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
