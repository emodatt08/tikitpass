<style type="text/css">
.form-group::before, .form-group::after {
 content: "";
 display: block;
 clear: both;
}
</style>
<?php //$this->load->view ('header');?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
  <!-- BEGIN SIDEBAR -->
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse">
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <?php $this->load->view ('administrator/sidebar');?>
    <!-- END SIDEBAR MENU -->
    <!-- END SIDEBAR MENU -->
  </div>
  <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN THEME PANEL -->
    <div class="page-bar">
      <ul class="page-breadcrumb">
        <li> <a href="">Home</a> <i class="fa fa-circle"></i> </li>
        <li> <span>administrator Panel</span> <i class="fa fa-circle"></i> </li>
        <li> <a href="<?php echo base_url()?>index.php/gallery/show_Property"><span>View Property</span></a> </li>
      </ul>
      <div class="page-toolbar">
        <div class="btn-group pull-right">
          <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions <i class="fa fa-angle-down"></i> </button>
          <ul class="dropdown-menu pull-right" role="menu">
            <li> <a href="#"> <i class="icon-bell"></i> Action</a> </li>
            <li> <a href="#"> <i class="icon-shield"></i> Another action</a> </li>
            <li> <a href="#"> <i class="icon-user"></i> Something else here</a> </li>
            <li class="divider"> </li>
            <li> <a href="#"> <i class="icon-bag"></i> Separated link</a> </li>
          </ul>
        </div>
      </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
      <div class="col-md-12">
        <div class="tabbable-line boxless tabbable-reversed">
          <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
              <div class="portlet box blue-hoki">
                <div class="portlet-title">
                  <div class="caption"> <i class="fa fa-gift"></i>View Member</div>
                  <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                </div>
                <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                  <?php foreach($viewmember as $i){?>
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Image : </label>
                      <div class="controls">
                        <?php  
							if($i->user_avatar == ''){
								$pic = base_url()."gallery_img/noimage.jpg" ;
							}elseif(base_url()."memberavatar/".$i->user_avatar == ''){
								$pic = base_url()."gallery_img/noimage.jpg" ;
							} else{
								$pic = base_url()."memberavatar/".$i->user_avatar ; 
							}  
						?>
                        <label class="control-label" style="width:400px;height:290px;text-align:justify;">
                        <div class="fileupload-new thumbnail" style="width:400px;height:290px;"> <img src="<?=$pic ?>" alt="Image loading.." style="height:270px;"/> </div>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3 control-label">First Name</label>
                      <div class="col-md-4"> <?php echo $i->first_name; ?> </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Last Name</label>
                      <div class="col-md-4"> <?php echo $i->last_name; ?> </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Gender</label>
                      <div class="col-md-4"> 
					  	<?php if($i->gender=='M'){ 
							echo "Male";
							}else{ 
							echo "female";
							}?> </div>
                    </div>
                     <div class="form-group">
                      <label class="col-md-3 control-label">Gender</label>
                      <div class="col-md-4"> 
					  	<?php  if($i->gender=='M'){ 
					  				echo "Male";
								}else{ 
									echo "female";
								}
						?> 
                    </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Status</label>
                      <div class="col-md-4">
						<?php 
							$st = $i->status;
							if($st=="1"){ echo "<b style='color:#5A9700'>Active</b>"; }else{echo "<b style='color:#DF6C33'>InActive</b>"; }
						?>
                      </div>
                    </div>
                    <?php    }   ?>
                    <!-- END FORM-->
                    <!-- END FORM-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
