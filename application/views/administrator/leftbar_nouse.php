<?php 
$nl=$this->uri->segment(2); 
 $nl2=$this->uri->segment(3); 
?>
<div class="page-sidebar navbar-collapse collapse">
      <!-- BEGIN SIDEBAR MENU -->
      <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
  <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
  <li class="sidebar-toggler-wrapper hide"> 
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="sidebar-toggler"> </div>
    <!-- END SIDEBAR TOGGLER BUTTON --> 
  </li>
  <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
  <li class="sidebar-search-wrapper"> 
    
    <!-- END RESPONSIVE QUICK SEARCH FORM --> 
  </li>
  
  <li class="nav-item start "> <a href="<?php echo base_url()?>administrator/home" class="nav-link "> <span class="title">View Dashboard</span> </a>
    <ul class="sub-menu">
      <li class="nav-item start "> <a href="index.html" class="nav-link "> <i class="icon-bar-chart"></i> <span class="title">View Dashboard</span> </a> </li>
    </ul>
  </li>
  <li class="heading">
    <h3 class="uppercase">Features</h3>
  </li>
  
  <!--Supratim Sen (CMS Section Start- 20/04/2016)-->
  <li class="nav-item <?php if($nl=="cms"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">CMS Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="cms" && $nl2!="view_addcms" ){?>active open<?php }?>"> <a href="<?php echo base_url()?>administrator/cms/show_cms" class="nav-link "> <span class="title">Manage CMS Page</span> </a> </li>
      <!--<li class="nav-item  <?php if($nl2=="view_addcms"){?>active open<?php }?>"> <a href="<?php echo base_url()?>cms/view_addcms" class="nav-link "> <span class="title">Add Page</span> </a> </li>-->
    </ul>
  </li>
   <li class="nav-item <?php if($nl=="event"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Event management</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_gallery"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/gallery/show_gallery" class="nav-link "> <span class="title">Manage Gallery</span> </a> </li>
      <li class="nav-item  <?php if($nl2=="addform"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/gallery/addform" class="nav-link "> <span class="title">Add Gallery</span> </a> </li>
    </ul>
  </li>
   <li class="nav-item  <?php if($nl=="member"){?>active open<?php  }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-user"></i> <span class="title">Member Management</span> <span class="arrow"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item <?php if($nl2 == "Add Member"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>administrator/memberdata/addmember" class="nav-link "> <span class="title">Add Member</span> </a> </li>
      <li class="nav-item <?php if($nl2 == "Show Member"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>administrator/memberdata/showmember" class="nav-link "> <span class="title">Show Member</span> </a> </li>
    </ul>
  </li>
  
  <li class="nav-item  <?php if($nl=="banner"){?>active open<?php  }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-diamond"></i> <span class="title">Banner Management</span> <span class="arrow"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item <?php if($nl2 == "Add Banner"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>administrator/banner/addbanner" class="nav-link "> <span class="title">Add Banner</span> </a> </li>
      <li class="nav-item <?php if($nl2 == "Show Banner"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>administrator/banner/showbanner" class="nav-link "> <span class="title">Show Banner</span> </a> </li>
    </ul>
  </li>
  
  <li class="nav-item <?php if($nl=="gallery"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Gallery Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_gallery"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/gallery/show_gallery" class="nav-link "> <span class="title">Manage Gallery</span> </a> </li>
      <li class="nav-item  <?php if($nl2=="addform"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/gallery/addform" class="nav-link "> <span class="title">Add Gallery</span> </a> </li>
    </ul>
  </li>
  
  <li class="nav-item <?php if($nl=="news"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">News Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
    
    <li class="nav-item  <?php if($nl2=="news_add_form"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/news/news_add_form" class="nav-link "> <span class="title">Add News</span> </a> </li>
    
      <li class="nav-item  <?php if($nl=="show_news"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/news/show_news" class="nav-link "> <span class="title">Manage News</span> </a> </li>
      
    </ul>
  </li>
  

   <li class="nav-item <?php if($nl=="contact"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Contact Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_contact"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/contact/show_contact" class="nav-link "> <span class="title">Manage Contact</span> </a> </li>
      
    </ul>
  </li> 
  
   <li class="nav-item <?php if($nl=="administratorprofile" ||$nl=="settings" ||$nl=="user" ){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Tools</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_administratorprofile_id"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/adminprofile/show_adminprofile_id/1" class="nav-link "> <span class="title">administrator profile</span> </a> </li>
      <li class="nav-item  <?php if($nl=="show_settings_id"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/settings/show_settings_id/1" class="nav-link "> <span class="title">Settings</span> </a> </li>
      
       <li class="nav-item  <?php if($nl=="show_reset_pass"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/user/show_reset_pass/1" class="nav-link "> <span class="title">Change Password</span> </a> </li>
       
       <li class="nav-item  <?php if($nl=="logout"){?>active open<?php }?>"> <a href="<?php echo base_url()?>administrator/main/logout" class="nav-link "> <span class="title">Log out</span> </a> </li>  
</ul>
  </li> 
  
  
</ul>
      <!-- END SIDEBAR MENU -->
      <!-- END SIDEBAR MENU -->
    </div>