<div class="content_bg">
	<div class="login-bg">
    	<div class="login_sec">
         <div style="padding:20px;"></div>
        <div class="login_heading"><img src="<?php echo base_url(); ?>login_css/image/create_account.png"> Create Account</div>
        
        <form class="" action="<?php echo base_url();?>signup/registration" method="post" name="signupform" onSubmit="return Submit()">
    	<div class="login_box">
        <div style="text-align:center;"> 
            <strong>
                <?php 
                    if($this->session->flashdata('success') !=""){
                        echo $this->session->flashdata('success');
                    } 
					
					if($this->session->flashdata('email_sent') !=""){
                        echo $this->session->flashdata('email_sent');
                    }
                ?>
           </strong>
		</div>
        	<label class="login_label">Name</label>
            <label id="errorBox1" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                <input type="text" name="name" id="myname" value="" class="login_input" placeholder="Name" onkeyup="leftTrim(this)">
          	</div>
            
            <label class="login_label">Email</label>
            <label id="errorBox2" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                <input type="email" name="email" id="email" value="" class="login_input" placeholder="Email" onkeyup="leftTrim(this)">
          	</div>
            
            <label class="login_label">Confirm Email</label>
            <label id="errorBoxce" style="color:#ff0000;"></label>
            <label id="errorBoxe" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                <input type="email" name="con_email" id="con_email" value="" class="login_input" placeholder="Confirm Email" onkeyup="leftTrim(this)">
          	</div>
            
            <label class="login_label">Mobile</label>
            <label id="errorBox4" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                <input type="tel" name="phone" id="phone" value="" class="login_input" placeholder="Mobile" onkeyup="leftTrim(this)">
          	</div>
            
            <label class="login_label">Password</label>
            <label id="errorBox5" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                <input type="password" name="password" id="password" value="" class="login_input" placeholder="Password" onkeyup="leftTrim(this)">
          	</div>
            <label class="login_label">Confirm Password</label>
            <label id="errorBox8" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                <input type="password" name="conf_password" id="conf_password" value="" class="login_input" placeholder="Confirm Password" onkeyup="leftTrim(this)">
          	</div>
            
<!--            <label class="login_label">Your Account</label>
            <label id="errorBox7" style="color:#ff0000;"></label>
            <div class="clearfix"></div>
          	<label><input type="radio" name="user_type" id="u1" value="u"> User</label>
            <label><input type="radio" name="user_type" id="u2" value="o"> Organizer</label>-->
            <input type="submit" class="login_btn" value="Create Account" onclick="return Validate()" id="signup">
            <a href="<?php echo base_url();?>login" class="forget_pass">Back to Login</a>
        </div>
        </form>
        <div style="padding:20px;"></div>
        </div>
    </div>
     
 </div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script>
	function Submit(){
		var emailRegex = /^[A-Za-z0-9._]*\@[A-Za-z]*\.[A-Za-z]{2,5}$/;
		var formemail = $("#email").val();
		var myname = $("#myname").val();
		
		if($("#myname").val() == "" ){
			$("#myname").focus();
			$("#errorBox1").html("enter the Name");
			return false;
		}else{
			$("#errorBox1").html("");
			}
  
		if($("#email").val() == "" ){
			$("#email").focus();
			$("#errorBox2").html("enter the email");
			return false;
		}
		if(!emailRegex.test(formemail)){
			$("#email").focus();
			$("#errorBox2").html("Please enter the valid email");
			return false;
		}else{
			$("#errorBox2").html("");
			}
		
		if($("#con_email").val() == "" ){
			$("#con_email").focus();
			$("#errorBoxce").html("enter the confirm email");
			return false;
		}
		if(!emailRegex.test(formemail)){
			$("#con_email").focus();
			$("#errorBoxce").html("Please enter the valid email");
			return false;
		}else{
			$("#errorBoxce").html("");
			}
  
		if($("#username").val() == "" ){
			$("#username").focus();
			$("#errorBox3").html("enter the UserName");
			return false;
		}else{
			$("#errorBox3").html("");
			}
  
		if($("#phone").val() == "" ){
			$("#phone").focus();
			$("#errorBox4").html("enter the Phone Number");
			return false;
		}else{
			$("#errorBox4").html("");
			}
  
		if($("#password").val() == "" ){
			$("#password").focus();
			$("#errorBox5").html("enter the Password");
			return false;
		}else{
			$("#errorBox5").html("");
			}
  
		if($("#conf_password").val() == "" ){
			$("#conf_password").focus();
			$("#errorBox6").html("enter the Confirm Password");
			return false;
		}else{
			$("#errorBox6").html("");
			}
  
		if($("#user_type").val() == "" ){
			$("#user_type").focus();
			$("#errorBox7").html("enter the Account type");
			return false;
		}else{
			$("#errorBox7").html("");
			}
 }

</script> 

<script>	
function leftTrim(element){
if(element)
element.value=element.value.replace(/^\s+/,"");
}
</script>
 <script type="text/javascript">
	$(document).ready(function() {
		$("#phone").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
					 // let it happen, don't do anything
					 return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});
	});
  </script>
	<script type="text/javascript">
		function Validate() {
			var password = document.getElementById("password").value;
			var confirmPassword = document.getElementById("conf_password").value;
			var mail = document.getElementById("email").value;
			var con_mail = document.getElementById("con_email").value;
			if (mail != con_mail) {
				$("#con_email").focus();
				$("#errorBoxe").html("Email and Confirm Email Does Not match!!!");
				return false;
			}else{
				$("#errorBoxe").html("");
				}
			if (password != confirmPassword) {
				$("#conf_password").focus();
				$("#errorBox8").html("Password and Confirm password Does Not match!!!");
				return false;
			}else{
				$("#errorBox8").html("");
				}
		}
    </script>
