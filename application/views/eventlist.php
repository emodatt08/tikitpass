﻿<style>
p {
 float:right;	
 margin: 0 0 10px;
}
</style>
<!-- Body Content -->
<div class="content_bg">
  	<div class="container">
      <!-- Breadcrumb -->
      <ol class="breadcrumb cu_breadcrumb">
        <li><a href="<?php echo base_url()?>">Home</a></li>
        <li class="active"><a href="<?php echo base_url()?>event"><?php echo $title;?></a></li>
      </ol>
      <!-- End Breadcrumb -->
    	<div class="row">
        	<div class="col-sm-3">
            	<div class="left_sec">
                	<div class="box">
                    	<h3 class="box_title">Hot Tickets</h3>
                        <ul class="list">
                        	<?php foreach($hotcategory as $hc){?>
                        	<li><a href="<?php echo base_url()?>eventdetails/show_event_details/<?php echo $hc->event_id?>"><?php echo $hc->event_name?></a></li>
                            <?php }?>
                      </ul>
                    </div>
                    <div class="box">
                    	<h3 class="box_title">Last Minute Deals</h3>
                    </div>
                    <div class="box cu_accordion">
                    	<h3 class="box_title">Events</h3>
                        <!-- Accordion -->
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php
						$ctn=1;
						foreach($latestevent as $ev){?>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading<?php echo $ctn; ?>">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $ctn; ?>" aria-expanded="true" aria-controls="collapse<?php echo $ctn; ?>">
                                      <?php echo $ev->event_name; ?>
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapse<?php echo $ctn; ?>" class="panel-collapse collapse <?php if($ctn == 1) {?>in<?php }?>" role="tabpanel" aria-labelledby="heading<?php echo $ctn; ?>">
                                  <div class="panel-body">
                                    <a href="<?php echo base_url();?>eventdetails/show_event_details/<?php echo $ev->event_id; ?>"><img src="<?php echo base_url()?>uploads/event/<?php echo $ev->event_image; ?>" class="img-responsive"></a>
                                  </div>
                                </div>
                              </div>
                         <?php 
						 $ctn++;
						 }?>     
                             <!-- <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                      Collapsible Group Item 2
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                  <div class="panel-body">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      Collapsible Group Item 3
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                  </div>
                                </div>
                              </div>-->
                            </div>
                        <!-- End Accordion -->
                    </div>
                    <div class="box">
                    	<h3 class="box_title">Movies</h3>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box">
                	<a href="javascript:void(0);"><img src="<?php echo base_url()?>images/events1.jpg" class="img-responsive"></a>
                </div>
            	<div class="box">
                	<h3 class="box_title">Latest Events</h3>
                	<div class="tab">
                      <!-- Tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Hot Tickets</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Just Announced</a></li>
                      </ul>
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                        	<?php foreach($hotcategory as $hc){?>
                                <h4 class="eventsShowList"><a href="<?php echo base_url()?>eventdetails/show_event_details/<?php echo $hc->event_id?>"><?php echo $hc->event_name?></a></h4>
                                <p class="events_text"><?php echo substr($hc->event_description,0,10)?> <a href="<?php echo base_url()?>eventdetails/show_event_details/<?php echo $hc->event_id?>" class="buy_tickets_btn">Buy Tickets</a></p>
                                <div class="divider"></div>
                            <?php }?>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div role="tabpanel" class="tab-pane fade" id="profile">
                        	<?php foreach($latestevent as $lc){?>
                        	<h4 class="eventsShowList"><a href="<?php echo base_url()?>eventdetails/show_event_details/<?php echo $lc->event_id?>"><?php echo $lc->event_name?></a></h4>
                            <p class="events_text"><?php echo substr($lc->event_description,0,10)?></p>
                            <div class="divider"></div>
                            <?php }?>
                            <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="box">
                	<div class="featured_events">
                    <h3 class="box_title">Upcoming Events</h3>
                    	<?php foreach($latestevent as $lc){?>
                    	<div class="featuredEventMod">
                            <a href="#">
                            <?php
							if($lc->event_image==''){
							?>
                             <img src="<?php echo base_url()?>images/comeingsoon.png" class="img-responsive">
                            <?php }else{?>
                            <img src="<?php echo base_url()?>uploads/event/<?php echo $lc->event_image?>" class="img-responsive">
                            <?php }?>
                            </a>
                            <div class="text">
                                <strong><a href="<?php echo base_url();?>eventdetails/show_event_details/<?php echo $ev->event_id; ?>"><?php echo $lc->event_name?></a></strong><br>
                                    <?php echo substr($lc->event_description,0,10)?><br>
                              <a href="<?php echo base_url();?>eventdetails/show_event_details/<?php echo $ev->event_id; ?>" class="more">Buy Tickets</a> </div>
                            <div class="clearfix"></div>
                          </div>
                        <?php }?>  
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>