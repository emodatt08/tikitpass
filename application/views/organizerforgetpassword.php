<div class="login-bg">
    	<div class="login_sec">
        <div style="padding:20px;"></div>
        <div class="login_heading"><img src="<?php echo base_url(); ?>uploads/forget_password.png"> Forget Password</div>
        
    	<div class="login_box">
        <form method="post" action="<?php echo base_url().'organizerforgetpassword/email_validation' ?>">
			<?php
            if($this->session->flashdata('logerror')!=''){
            ?>	
            <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span> <?php echo $this->session->flashdata('logerror'); ?> </span>
            </div>
            <?php   
            }
            ?>
            <div style="text-align:center;"> <strong><?php 
            if($this->session->flashdata('success') !=""){
            echo $this->session->flashdata('success');
            } ?></strong>
            </div>
        	<label class="login_label">Enter Your Email</label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                <input type="text" class="login_input" placeholder="Enter Your Email" name="organizer_email">
          	</div>
            <input type="submit" class="login_btn" value="Submit">
            <a href="<?php echo base_url(); ?>organizerlogin" class="forget_pass">Back to Login</a>
        </div>
        </div>
        <div style="padding:20px;"></div>
    </div>