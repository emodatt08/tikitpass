<div class="login-bg">
    	<div class="login_sec">
         <div style="padding:20px;"></div>
        <div class="login_heading"><img src="<?php echo base_url(); ?>login_css/image/login.png"> Login</div>
        
    	<div class="login_box">
        	<form method="post" action="<?php echo base_url().'login/login_validation' ?>">
			<?php
			if($this->session->flashdata('logerror')!=''){
			?>	
				<div class="alert alert-danger">
            		<button class="close" data-close="alert"></button>
           		 	<span> <?php echo $this->session->flashdata('logerror'); ?> </span>
            	</div>
             <?php   
			}
			?>
			<?php if(validation_errors()){?>
            <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span> <?php echo validation_errors(); ?>  </span>
            </div>
            <?php }?>
            
            <?php 
            if(@$error!=''){
            ?>
            <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            <span> <?php echo @$error; ?>  </span>
            </div>
            <?php }?>
            
            <label class="login_label">Enter Email</label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                <input type="text" class="login_input" placeholder="Enter Your Email Id" name="email">
          	</div>
            
            <label class="login_label">Enter Your Password</label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                <input type="password" class="login_input" placeholder="Enter Your Password" name="password">
          	</div>
            
            <!--<div class="login_label" style="text-align:left !important; padding-left:4%; font-size:14px; text-align:center;">
            <input type="radio" name="user_type" value="o" onClick="OptionSelected()" /><label class="login_label">Organizer </label> &nbsp;
            <input type="radio" name="user_type" value="u" onClick="OptionSelected()" /><label class="login_label">User </label>
            </div>-->
            
            <input type="submit" class="login_btn" value="Login" id="Login1" name="login">
  			<label class="remember_login"><input type="checkbox" name="eRemember" value="eRemember"> Remember Me</label>
            <div class="login_label" style="text-align:left !important; padding-left:42px;">
            <script src="http://code.jquery.com/jquery-1.7.1.js"></script>
            <script type="text/javascript">
            function OptionSelected() {
            document.getElementById("Login").disabled = false;
            var selected = $('input[name="user_type"]:checked').val();
            if (selected == "4") {
            document.getElementById("Login").disabled = true;;
            }
            }
            
            $(document).ready(function () {
            $("#Login").click(function (e) {
            if (!$('input[name="user_type"]').is(':checked')) {
            return false;
            }
            else {
            return true;
            }
            });
            });
            </script>
            &nbsp;
            </div>
            </form>
            <a href="<?php echo base_url();?>forgetpassword" class="forget_pass">Forget Password?</a>
        </div>
        <a href="<?php echo base_url();?>signup" class="create_btn">Create Account</a>
        </div>
        <div style="padding:20px;"></div>
    </div>
    