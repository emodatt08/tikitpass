<div class="content_bg">
  <div class="login-bg">
    <div class="login_sec">
      <div style="padding:20px;"></div>
      <?php foreach($info as $in){?>
      <div class="col-md-12">
        <div class="col-md-3">
                        <strong>
                        <?php 
                        if($this->session->flashdata('propic_change') !=""){
						?>
                        <p style="color:#0F0; margin-left: 25px;">
                        <?php 	
                        echo $this->session->flashdata('propic_change');
                        ?>
						</p>
                        <?php
                        } 
                        ?>
                        </strong>
                    	<div class="box" align="center">
                        <form method="post" action="<?php echo base_url();?>profile/changeprofileimage" id="uploadFileForm" enctype="multipart/form-data">
            			<input type="hidden" name="oldimg" value="<?php echo $in->imagename?>" />
                        <?php if($in->imagename == '') {?>
                        <img src="<?php echo base_url()?>uploads/nopic.png" style="height: 170px;"/>
                        <?php } else {?>
                        <img src="<?php echo base_url()?>uploads/profileimage/<?php echo $in->imagename?>" style="height: 170px;"/>
                        <?php }?>
                        <div class="fileUpload btn btn-primary" align="center">
                        <span>Upload</span>
                       	<input type="file" class="upload" name="userfile" id="file-2"/>
                        </div>
                        </form> 
                        <?php $this->load->view('sidebar');?>
                    </div>
                    </div>
        <div class="col-md-9">
          <div class="box">
          <?php if($this->session->flashdata('pass_change')!=''){?>
			<div class="alert alert-success text-center"><b style="color:#0C0;"><?php echo @$this->session->flashdata('pass_change');?></b></div>
	  	<?php }?>
            <form class="" action="<?php echo base_url();?>profile/reset_pass" method="post" name="signupform" onSubmit="return Submit()">
            
					<input type="hidden" id="org_pass" name="org_pass" value="<?php echo base64_decode($in->password);?>">	
                    <?php }?>
              <div class="login_box">
                <div style="text-align:center;"> <strong>
				
                  </strong> </div>
                <label class="login_label">Current Password</label>
                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
                  <input type="password" name="old_pass" id="old_pass" value="" class="new_input" placeholder="Old Password" onkeyup="leftTrim(this)">
                  <label id="errorBox" style="color:#ff0000;"></label>
                    <label id="errorBox8" style="color:#ff0000;"></label>
                </div>
                <label class="login_label">New Password</label>
                <label id="errorBox2" style="color:#ff0000;"></label>
                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
                  <input type="password" name="new_pass" id="new_pass" value="" class="new_input" placeholder="New password" onkeyup="leftTrim(this)">
                  <label id="errorBox1" style="color:#ff0000;"></label>
                </div>
                <label class="login_label">Confirm New Password</label>
                <label id="errorBox4" style="color:#ff0000;"></label>
                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
                  <input type="password" name="con_pass" id="con_pass" value="" class="new_input" placeholder="Confirm Password" onkeyup="leftTrim(this)">
                  <label id="errorBox2" style="color:#ff0000;"></label>
                 <label id="errorBox9" style="color:#ff0000;"></label>
                </div>
               
                <input type="submit" class="submit_btn_new" value="Change Password" onclick="return Validate()" id="signup">
                
                 </div>
            </form>
          </div>
        </div>
      </div>
      <div style="padding:20px;"></div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>	
function leftTrim(element){
if(element)
element.value=element.value.replace(/^\s+/,"");
}
</script>
<script>
function Submit(){
		if($("#old_pass").val() == "" ){
		$("#old_pass").focus();
		$("#errorBox").html("Please Enter the Old Password");
		return false;
  }else{
	  $("#errorBox").html("");
	  }
  
  if($("#new_pass").val() == "" ){
		$("#new_pass").focus();
		$("#errorBox1").html("Please Enter the New Password");
		return false;
  }else{
	  $("#errorBox1").html("");
	  }
  
  if($("#con_pass").val() == "" ){
		$("#con_pass").focus();
		$("#errorBox2").html("Please Enter the Password Once Again");
		return false;
  }else{
	  $("#errorBox2").html("");
	  }
}
</script>

<script type="text/javascript">
    function Validate() {
        var org_pass = document.getElementById("org_pass").value;
		var old_pass = document.getElementById("old_pass").value;
		var new_pass = document.getElementById("new_pass").value;
		var con_pass = document.getElementById("con_pass").value;
		/*alert(org_pass);
		alert(old_pass);
		alert(new_pass);
		alert(con_pass);
		return false;*/
		if (org_pass != old_pass) {
			//alert("Incorrect Old Password!!!");
			$("#errorBox8").html("Incorrect Old Password!!!");
			$("#old_pass").focus();
			return false;
		}else{
	  		$("#errorBox8").html("");
	 	 }
		if (new_pass != con_pass) {
			//alert("Password and Confirm password Does Not match!!!");
			$("#errorBox9").html("Password and Confirm password Does Not match!!!");
			$("#con_pass").focus();
			return false;
		}else{
	  		$("#errorBox9").html("");
	 	 }	
    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#file-2").change(function() {
		$("#uploadFileForm").submit();
	});

});
</script>
