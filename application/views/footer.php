  <!-- Footer -->
  <div class="footer">
  	<div class="container">
    	<div class="row">
        	<div class="col-sm-3">
            	<h4 class="footer_title">Menu</h4>
                <ul class="f_menu">
                	<li><a href="<?php echo base_url();?>pages/about">ABOUT US</a></li>
                    <li><a href="<?php echo base_url()?>pages/whatwedo">What We Do</a></li>
                    <li><a href="<?php echo base_url();?>contact">Contact Us</a></li>
                    <li><a href="<?php echo base_url()?>pages/career">Carers</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
            	<h4 class="footer_title">New Media</h4>
                <ul class="f_menu">
                	<li><a href="http://www.tickets.com/emailoptin.html" target="_blank">Get Email Updates</a></li>
                    <li><a href="https://twitter.com/Ticketsdotcom" target="_blank">Follow Us on Twitter</a></li>
                    <li><a href="https://www.facebook.com/ticketsdotcom" target="_blank">Become a Fan on Facebook</a></li>
                    <li><a href="http://blog.tickets.com/" target="_blank">Read our Blog</a></li>
                    <li><a href="http://youtube.tickets.com/" target="_blank">Visit Us on YouTube</a></li>
                    <li><a href="https://twitter.com/ticketsdotcom" target="_blank">Get RSS Feeds</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
				<?php 
                    $result = $data['cmsabt'] = $this->home_model->cmsfetch();
					foreach($result as $abt){
                ?>
            	<h4 class="footer_title">About Us</h4>
                <?=substr(strip_tags($abt->description),0,300)?> ..<a href="<?php echo base_url()?>pages/about">Read More</a>
                <?php }?>
            </div>
            <div class="col-sm-3">
            <?php 
                    $resultmap = $data['map'] = $this->home_model->mapfetch();
					foreach($resultmap as $map){
                ?>
            	<h4 class="footer_title">Maps</h4>
                <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4046298.9897194295!2d-3.2756887032222135!3d7.950923016283521!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfd75acda8dad6c7%3A0x54d7f230d093d236!2sGhana!5e0!3m2!1sen!2sin!4v1481114306640" width="250" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                <?php echo $map->map; ?>
                <?php }?>
            </div>
        </div>
    </div>
  </div>
  <div class="footer_bottom">
  	<div class="container">
    	<p class="cl">Tikitpass 2016</p>
        <p class="goigi">Designed & Developed by <a href="http://www.goigi.com/" target="_blank">goigi</a></p>
    </div>
  </div>
  <!-- End Footer -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>js/jquery.bxslider.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
    <script>
	$(document).ready(function(){
	  $('.bxslider').bxSlider({
		  auto: true,
		  captions: true,
		  mode: 'fade',
		  });
	});
	</script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script>
	$(document).ready(function(){
	  $('.bxslider').bxSlider({
		  auto: true,
		  captions: true,
		  mode: 'fade',
		  });
	});
	
	
	
	function leftTrim(element){
if(element)
element.value=element.value.replace(/^\s+/,"");
}
	
	</script>
  </body>
</html>