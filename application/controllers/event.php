<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//include_once (dirname(__FILE__) . "/my_controller.php");
class Event extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	//$this->load->model('home_model');
	$this->load->database();
	$this->load->model('home_model');
	$this->load->model('event_model');
}
	public function index(){
		$hotquery = $this->event_model->show_hot_event();
		$data['hotcategory']=$hotquery;
		
		$latestevent=$this->event_model->show_latest_event();
		$data['latestevent']=$latestevent;
		
		$data['title'] = "Event List";
	    $this->load->view('header',$data);
		$this->load->view('eventlist',$data);
		$this->load->view('footer',$data);
	}
}
