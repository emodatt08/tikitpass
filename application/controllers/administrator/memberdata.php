<?php
class Memberdata extends CI_Controller {
		//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('administrator/home', 'refresh');
			}
			$this->load->model('administrator/member_model');
			$this->load->model('administrator/banner_model');
			$this->load->library('image_lib');
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			
			//****************************backtrace prevent*** END HERE*************************
			
		}
		//============Constructor to call Model====================
		function index(){
			if($this->session->userdata('is_logged_in')){
				redirect('administrator/memberadd_view');
			}else{
				$this->load->view('administrator/main/login');	
			}
		}
		
		
		//=======================Insert Page Data============
		function add_member(){
			$config = array(
			'upload_path' => "memberavatar/",
			'upload_url' => base_url() . "memberavatar/",
			'allowed_types' => "gif|jpg|png|jpeg"
			);
			
			//load upload class library
        	$this->load->library('upload', $config);
				//=====================+++++++++++++++++++++++===================
				$this->form_validation->set_rules('first_name','First Name', 'required|min_length[1]');
				$this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[1]');
				$this->form_validation->set_rules('dob', 'Banner Description', 'required');
				$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
				//=====================+++++++++++++++++++++++===================
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('administrator/header');
					$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';
					$this->load->view('administrator/header');
            		$this->load->view('administrator/memberadd_view',$data);
					$this->load->view('administrator/footer');
					//redirect('banner/addbanner',$data);
				}else{
					if (!$this->upload->do_upload('userfile')){
            			$data['success_msg'] = '<div class="alert alert-success text-center">You Must Select An Image File!</div>';
						$this->load->view('administrator/header');
            			$this->load->view('administrator/memberadd_view', $data);
						$this->load->view('administrator/footer');
        			}else{
						 $data['userfile'] = $this->upload->data();
						 $filename=$data['userfile']['file_name'];
						 $data = array(
							'first_name' => $this->input->post('first_name'),
							'last_name' => $this->input->post('last_name'),
							'dob' => date('Y-m-d',strtotime($this->input->post('dob'))),
							'username' => $this->input->post('username'),
							'password' => $this->input->post('password'),
							'gender' => $this->input->post('gender'),
							'address' => $this->input->post('address'),
							'city' => $this->input->post('city'),
							'zipcode' => $this->input->post('zipcode'),
							'usercreationdate' => date('Y-m-d'),
							'user_avatar' => $filename,
							'status' => 1
						);
						
						$this->member_model->insert_member($data);
            			$upload_data = $this->upload->data();
						$query = $this->member_model->show_member();
						$this->session->set_flashdata('add_message', '<div class="alert alert-success text-center">Member Successfully Added !</div>');
						redirect('administrator/memberdata/showmember');
						/*$this->load->view('administrator/header',$data);
						$this->load->view('administrator/showmemberlist',$data);
						$this->load->view('administrator/footer');*/
				
					}
				}
		}
		//=======================Insert Page Data============
  		//=======================Insertion Success message=========
		function success(){
			$data['h1title'] = 'Data Inserted Successfully';
			$data['title'] = 'Add banner';
			$this->load->view('administrator/header');
			$this->load->view('administrator/banneradd_view',$data);
			$this->load->view('administrator/footer');
		}
		//=======================Insertion Success message=========	
		
		
		//================Add banner form=============
		function addmember(){
			
			$data['title'] = "Member add";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/memberadd_view');
			$this->load->view('administrator/footer');
		}
		//================View Individual Data List=============
		
		//================View Individual Data List=============
  		//================Show Individual by Id=================
		function show_member_id($id) {
			$id = $this->uri->segment(4); 
			$data['title'] = "Edit Member";
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/member_model');
			//Transfering data to Model
			$query = $this->member_model->show_member_id($id);
			$data['emember'] = $query;
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/member_edit', $data);
			$this->load->view('administrator/footer');
		}
		
		
		function statusbanner (){
		$stat= $this->input->get('stat'); 
		$id= $this->input->get('id');   
		$this->load->model('administrator/banner_model');
		$this->banner_model->updt($stat,$id);
		}
   		//================Show Individual by Id=================
  	 	//================Update Individual ====================
		function edit_member(){
			 //============================================
		 $old_file = $this->input->post('old_file');
			 $config = array(
				'upload_path' => "memberavatar/",
				'upload_url' => base_url() . "memberavatar/",
				'allowed_types' => "gif|jpg|png|jpeg"
			);
			$this->load->library('upload', $config);
			if ($this->upload->do_upload("userfile")) {
				//echo $path = base_url(). "banner/";exit();
				//echo $path1 = "banner/"; 
				@unlink("memberavatar/".$old_file);
				
				//echo $image_data = $this->upload->data();
				$data['img'] = $this->upload->data();
				//*********************************
				//============================================
				$datalist = array(			
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'dob' => date('Y-m-d',strtotime($this->input->post('dob'))),
					'username' => $this->input->post('username'),
					'gender' => $this->input->post('gender'),
					'address' => $this->input->post('address'),
					'city' => $this->input->post('city'),
					'user_avatar' => $data['img']['file_name'],
					'status' => $this->input->post('status')
				);
				//print_r($datalist); exit();
				$user_avatar = $this->input->post('userfile');
				//====================Post Data===================
				
				$id = $this->input->post('member_id');
				$data['title'] = "Member Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/member_model');
				//Transfering data to Model
				$query = $this->member_model->member_edit($id,$datalist,$user_avatar);
				$data1['message'] = 'Data Update Successfully';
				//$query = $this->member_model->show_bannerlist();
				$this->session->set_flashdata('edit_message', '<div class="alert alert-success text-center">Member Successfully Updated !</div>');
				redirect('administrator/memberdata/showmember');
				//*********************************
		
			}else{
				$datalist = array(			
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'dob' => date('Y-m-d',strtotime($this->input->post('dob'))),
					'username' => $this->input->post('username'),
					'gender' => $this->input->post('gender'),
					'address' => $this->input->post('address'),
					'city' => $this->input->post('city'),
					'status' => $this->input->post('status')
				);
			
				
				//====================Post Data===================
				$user_avatar = $this->input->post('userfile');
				$id = $this->input->post('member_id');
				$data['title'] = "Member Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/member_model');
				//Transfering data to Model
				$query = $this->member_model->member_edit($id,$datalist,$user_avatar);
				$data1['message'] = 'Data Update Successfully';
				$this->session->set_flashdata('edit_message', '<div class="alert alert-success text-center">Member Successfully Updated !</div>');
				//$query = $this->member_model->show_bannerlist();
				redirect('administrator/memberdata/showmember');
			}
			
		}
		//================Update Individual ====================
		//===================Member View=======================
		function member_view($id){
			$id = $this->uri->segment(4);
			$this->load->database();
			$query = $this->member_model->member_view($id);
			$data['viewmember'] = $query;
			//print_r($query);
			//exit();
			$data['title'] = "Member View";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/member_view',$data);
			$this->load->view('administrator/footer');
	}
		//===================Member View=======================
  		
		//=======================Delete Individual==============
		function delete_member($id){
			$id = $this->uri->segment(4);
			$this->load->database();
			$query = $this->member_model->member_view($id);
			$member_image = $result[0]->user_avatar;
			$this->member_model->delete_member($id,$member_image);
			$this->session->set_flashdata('del_message', '<div class="alert alert-success text-center">Member Successfully Deleted !</div>');
			redirect('administrator/memberdata/showmember');
		}
		//======================Delete Individual===============
		
		//======================Show CMS========================
		function showmember(){
			//Loading Database
			$this->load->database();
			//Transfering data to Model
			$query = $this->member_model->show_member();
			$data['emember'] = $query;
			$data['title'] = "Member List";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/showmemberlist',$data);
			$this->load->view('administrator/footer',$data);
	}
		//======================Show CMS========================
		
		//=====================DELETE BANNER====================

		
			function delete_banner() {
			$id = $this->uri->segment(4);
			$result=$this->banner_model->show_banner_id($id);
			//print_r($result);
			$banner_img = $result[0]->banner_img; 
			//echo $banner_img;exit();
			//Loading Database
			$this->load->database();

			//Transfering data to Model
			$query = $this->banner_model->delete_banner($id,$banner_img);
			$data['ecms'] = $query;
			$this->load->view('administrator/header',$data);
			//$this->load->view('showbannerlist', $data);
			redirect('administrator/banner/showbanner');
			$this->load->view('administrator/footer');
		}

		//=====================DELETE BANNER====================
		
		//======================Logout==========================
		public function Logout(){
        	$this->session->sess_destroy();
        	redirect('administrator/login');
    	}
		//======================Logout==========================
}

?>

