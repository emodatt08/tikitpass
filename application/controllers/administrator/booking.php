<?php
class Booking extends CI_Controller {
		//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('administrator/home', 'refresh');
			}
			$this->load->model('administrator/booking_model');
			$this->load->library('image_lib');
			
				//****************************backtrace prevent*** START HERE*************************
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			
			//****************************backtrace prevent*** END HERE*************************
		}
		//============Constructor to call Model====================
		function index()
	{
		if($this->session->userdata('is_logged_in')){
			redirect('administrator/bookingadd_view');
        }else{
        	$this->load->view('administrator/login');	
        }
		
		
	}
//======================Show Add form for booking add **** START HERE========================	
function booking_add_form(){
				$data['title'] = "Add Booking";
				$this->load->view('administrator/header',$data);
				$this->load->view('administrator/bookingadd_view');
				$this->load->view('administrator/footer');
     }
		
//====================== Show Add form for booking add ****END HERE========================
		//=======================Insert Page Data============
		function add_booking(){
			$my_date = date("Y-m-d", time()); 
			$config = array(
			'upload_path' => "booking/",
			'upload_url' => base_url() . "booking/",
			'allowed_types' => "gif|jpg|png|jpeg"
			);
        	$this->load->library('upload', $config);
				//=====================+++++++++++++++++++++++===================
				$this->form_validation->set_rules('booking_title','Booking Title', 'required');
				$this->form_validation->set_rules('designation', 'Designation', 'required|min_length[1]|max_length[100]');
				$this->form_validation->set_rules('booking_desc', 'Booking Description', 'required|min_length[1]|max_length[100000]');
				$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
				//=====================+++++++++++++++++++++++===================
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('administrator/header');
					$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';
					$this->load->view('administrator/header');
            		$this->load->view('administrator/bookingadd_view',$data);
					$this->load->view('administrator/footer');
					//redirect('banner/addbanner',$data);
				}else{
					if (!$this->upload->do_upload('userfile')){
            			$data['success_msg'] = '<div class="alert alert-success text-center">You Must Select An Image File!</div>';
						$this->load->view('administrator/header');
            			$this->load->view('administrator/bookingadd_view', $data);
						$this->load->view('administrator/footer');
        			}else{
						 $data['userfile'] = $this->upload->data();
						 $filename=$data['userfile']['file_name'];
						 $data = array(
							'booking_title' => $this->input->post('booking_title'),
							'designation' => $this->input->post('designation'),
							'booking_image' => $filename,
							'booking_desc' => $this->input->post('booking_desc'),
							'date' => $my_date,
							'booking_status' => 1
						);
						$this->booking_model->insert_booking($data);
            			$upload_data = $this->upload->data();
						$query = $this->booking_model->show_booking();
						$data['ecms'] = $query;
						$this->session->set_flashdata('add_message', 'Booking Added Successfully !!!!');
            			$data['success_msg'] = '<div class="alert alert-success text-center">Your file <strong>' . $upload_data['file_name'] . '</strong> was successfully uploaded!</div>';
						/*$this->load->view('administrator/header',$data);
						$this->load->view('administrator/showbookinglist',$data);
						$this->load->view('administrator/footer');*/
						$this->load->view('administrator/header',$data);
			//$this->load->view('showbannerlist', $data);
			redirect('administrator/booking/show_booking');
			$this->load->view('administrator/footer');
				
					}
				}
		}
		//=======================Insert Page Data============

//=======================Insertion Success message for Booking *** START HERE=========
		function success(){
			$data['h1title'] = 'Add Booking';
			
			$data['title'] = 'Add Booking';
			$this->session->set_flashdata('add_message', 'Booking Added Successfully !!!!');
			$this->load->view('administrator/header');
			$this->load->view('administrator/bookingadd_view',$data);
			$this->load->view('administrator/footer');
		}
//=======================Insertion Success message *** END HERE=========	
//======================Show Booking List **** START HERE========================
		function show_booking(){
		//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/booking_model');
			//Transfering data to Model
			$query = $this->booking_model->show_booking_join();
			$data['ebook'] = $query;
			$query1 = $this->booking_model->show_booking_stat();
			$data['estat'] = $query1;
			$data['title'] = "Booking List";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/showbookinglist');
			$this->load->view('administrator/footer');
		
	}
//======================Show Booking List **** END HERE========================
//======================Status change **** START HERE========================
	function statusbooking ()
		{
			     $stat= $this->input->get('stat'); 
				 $id= $this->input->get('id');   
		$this->load->model('administrator/booking_model');
		//echo $this->db->last_query();
		$this->booking_model->updt($stat,$id);
		}

//=======================Status change **** END HERE========================	
//================Show Individual by Id for Booking *** START HERE=================
		function show_booking_id($id) {
			 $id = $this->uri->segment(4); 
			//exit();
			$data['title'] = "Edit Booking";
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/booking_model');
			//Transfering data to Model
			$query = $this->booking_model->show_booking_id($id);
			$data['ecms'] = $query;
			$query1 = $this->booking_model->show_booking_stat();
			$data['estat'] = $query1;
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/booking_edit', $data);
			$this->load->view('administrator/footer');
		}
		
//================Show Individual by Id for Booking *** END HERE=================
//================Update Individual booking***** START HERE ====================
		function edit_booking(){
				$datalist = array(			
				'pay_status' => $this->input->post('pay_status'),
				);
				$id = $this->input->post('booking_id');
				$data['title'] = "Booking Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/booking_model');
				//Transfering data to Model
				$query = $this->booking_model->booking_edit($id,$datalist);
				$data1['message'] = 'Data Update Successfully';
				$query = $this->booking_model->show_bookinglist();
				$data['ecms'] = $query;
				$query1 = $this->booking_model->show_booking_stat();
				$data['estat'] = $query1;
				$data['title'] = "Booking Page List";
				$this->session->set_flashdata('edit_message', 'Booking Updated Successfully !!!!');
				redirect('administrator/booking/show_booking');
		}
//================Update Individual  Booking ***** END HERE====================

		//=====================DELETE NEWS====================

		
			function delete_booking() {
			$id = $this->uri->segment(4);
			$result=$this->booking_model->show_booking_id($id);
			//print_r($result);
			//echo $banner_img;exit();
			//Loading Database
			$this->load->database();

			//Transfering data to Model
			$query = $this->booking_model->delete_booking($id);
			$data['ecms'] = $query;
			$this->session->set_flashdata('delete_message', 'Booking Deleted Successfully !!!!');
			$this->load->view('administrator/header',$data);
			//$this->load->view('showbannerlist', $data);
			redirect('administrator/booking/show_booking');
			$this->load->view('administrator/footer');
		}

		//=====================DELETE NEWS====================

		//====================MULTIPLE DELETE=================
		function delete_multiple(){
		$ids = ( explode( ',', $this->input->get_post('ids') ));
		$this->booking_model->delete_mul($ids);
		$this->session->set_flashdata('delete_message1', 'Booking Deleted Successfully !!!!');
		$this->load->view('administrator/header',$data);
			//$this->load->view('showbannerlist', $data);
			redirect('administrator/booking/show_booking');
			$this->load->view('administrator/footer');
		}
		
		function booking_view($id){
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/booking_model');
			//Transfering data to Model
			$query = $this->booking_model->booking_view($id);
			$data['viewbooking'] = $query;
			//echo $sql = $this->db->last_query();
			//exit();
			$data['title'] = "Booking View";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/booking_view',$data);
			$this->load->view('administrator/footer');
			
		
	}
		//====================MULTIPLE DELETE=================
//======================Logout==========================
		public function Logout(){
        	$this->session->sess_destroy();
        	redirect('administrator/login');
    	}
//======================Logout==========================
}

?>