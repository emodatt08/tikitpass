<?php
class Users extends CI_Controller {
		//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('administrator/home', 'refresh');
			}
			$this->load->model('administrator/userst_model');
			$this->load->library('image_lib');
			
				//****************************backtrace prevent*** START HERE*************************
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			
			//****************************backtrace prevent*** END HERE*************************
		}
		//============Constructor to call Model====================
		function index()
	{
		if($this->session->userdata('is_logged_in')){
			redirect('administrator/usersadd_view');
        }else{
        	$this->load->view('administrator/login');	
        }
		
		
	}
//======================Show Add form for users add **** START HERE========================	
function users_add_form(){
				$query = $this->userst_model->show_countries();
				$data['cntry'] = $query;
				$data['title'] = "Add users";
				$this->load->view('administrator/header',$data);
				$this->load->view('administrator/usersadd_view');
				$this->load->view('administrator/footer');
     }
		
//====================== Show Add form for users add ****END HERE========================
		//=======================Insert Page Data============
		function add_users(){
			$my_date = date("Y-m-d", time()); 
			$config = array(
			'upload_path' => "uploads/profileimage/",
			'upload_url' => base_url() . "uploads/profileimage/",
			'allowed_types' => "gif|jpg|png|jpeg"
			);
			//echo 'working';exit();
        	$this->load->library('upload', $config);
				//=====================+++++++++++++++++++++++===================
				//$this->form_validation->set_rules('name','users Title', 'required');
				//$this->form_validation->set_rules('user_type', 'User Type', 'required');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]');
				//$this->form_validation->set_rules('password', 'Password', 'required');
				//$this->form_validation->set_rules('description', 'users Description', 'required|min_length[1]|max_length[100000]');
				$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
				//=====================+++++++++++++++++++++++===================
				if ($this->form_validation->run() == FALSE) {
					//echo "aa"; exit();
					$this->load->view('administrator/header');
					$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';
					$this->load->view('administrator/header');
            		$this->load->view('administrator/usersadd_view',$data);
					$this->load->view('administrator/footer');
					//redirect('banner/addbanner',$data);
				}else{
					if (!$this->upload->do_upload('userfile')){
						//cho "aassss"; exit();
            			$data['success_msg'] = '<div class="alert alert-success text-center">You Must Select An Image File!</div>';
						$this->load->view('administrator/header');
            			$this->load->view('administrator/usersadd_view', $data);
						$this->load->view('administrator/footer');
        			}else{
						//echo "www"; exit();
						 $data['userfile'] = $this->upload->data();
						 $filename=$data['userfile']['file_name'];
						 
						 $data = array(
							'name' => $this->input->post('name'),
							'email' => $this->input->post('email'),
							'password' => base64_encode($this->input->post('password')),
							'phone' => $this->input->post('phone'),
							'adrs' => $this->input->post('address'),
							'user_type' => $this->input->post('user_type'),
							'username' => $this->input->post('username'),
							'imagename' => $filename,
							'description' => $this->input->post('description'),
							'dateofjoin' => $my_date,
							'userstatus' => 1
						);
						//print_r($data); exit();
						$this->userst_model->insert_users($data);
						//echo $ddd=$this->db->last_query(); exit();
            			$upload_data = $this->upload->data();
						$query = $this->userst_model->show_users();
						$data['ecms'] = $query;
						$this->session->set_flashdata('add_message', 'users Added Successfully !!!!');
            			$data['success_msg'] = '<div class="alert alert-success text-center">Your file <strong>' . $upload_data['file_name'] . '</strong> was successfully uploaded!</div>';
						/*$this->load->view('administrator/header',$data);
						$this->load->view('administrator/showuserslist',$data);
						$this->load->view('administrator/footer');*/
						$this->load->view('administrator/header',$data);
			//$this->load->view('showbannerlist', $data);
			redirect('administrator/users/show_users');
			$this->load->view('administrator/footer');
				
					}
				}
		}
		//=======================Insert Page Data============

//=======================Insertion Success message for users *** START HERE=========
		function success(){
			$data['h1title'] = 'Add users';
			
			$data['title'] = 'Add users';
			$this->session->set_flashdata('add_message', 'users Added Successfully !!!!');
			$this->load->view('administrator/header');
			$this->load->view('administrator/usersadd_view',$data);
			$this->load->view('administrator/footer');
		}
//=======================Insertion Success message *** END HERE=========	
//======================Show users List **** START HERE========================
		function show_users(){
		//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/userst_model');
			//Transfering data to Model
			$query = $this->userst_model->show_users();
			$data['ecms'] = $query;
			$data['title'] = "users List";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/showuserslist');
			$this->load->view('administrator/footer');
		
	}
//======================Show users List **** END HERE========================
//======================Status change **** START HERE========================
	function statususers ()
		{
			     $stat= $this->input->get('stat'); 
				 $id= $this->input->get('id');   
		$this->load->model('administrator/userst_model');
		$this->userst_model->updt($stat,$id);
		}

//=======================Status change **** END HERE========================	
//================Show Individual by Id for users *** START HERE=================
		function show_users_id($id) {
			 $id = $this->uri->segment(4); 
			//exit();
			$query = $this->userst_model->show_countries();
			$data['cntry'] = $query;
			$data['title'] = "Edit users";
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/userst_model');
			//Transfering data to Model
			$query = $this->userst_model->show_users_id($id);
			$data['ecms'] = $query;
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/users_edit', $data);
			$this->load->view('administrator/footer');
		}
		
//================Show Individual by Id for users *** END HERE=================
//================Update Individual users***** START HERE ====================
		function edit_users(){
			 $imagename = $this->input->post('imagename');
			 $config = array(
				'upload_path' => "uploads/profileimage/",
				'upload_url' => base_url() . "uploads/profileimage/",
				'allowed_types' => "gif|jpg|png|jpeg"
			);
			$this->load->library('upload', $config);
			if ($this->upload->do_upload("userfile")) {
				@unlink("users/".$imagename);
				//echo $image_data = $this->upload->data();
				$data['img'] = $this->upload->data();
			 	//echo $data['img']['file_name'];
				//exit();
				//*********************************
				//============================================
				$datalist = array(			
							'name' => $this->input->post('name'),
							'email' => $this->input->post('email'),
							'password' => base64_encode($this->input->post('password')),
							'phone' => $this->input->post('phone'),
							'adrs' => $this->input->post('address'),
							'user_type' => $this->input->post('user_type'),
							'username' => $this->input->post('username'),
							'imagename' => $filename,
							'description' => $this->input->post('description')
				);
				//echo $gallery_name=$_POST['gallery_name'];
				//====================Post Data===================
				//print_r($datalist);
				//exit();
				$id = $this->input->post('users_id');
				$data['title'] = "users Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/userst_model');
				//Transfering data to Model
				$query = $this->userst_model->users_edit($id,$datalist);
				// echo $ddd=$this->db->last_query();
				
				$data1['message'] = 'Data Update Successfully';
				$query = $this->userst_model->show_userslist();
				$data['ecms'] = $query;
				$data['title'] = "users Page List";
				/*$this->load->view('administrator/header',$data);
				$this->load->view('administrator/showuserslist', $data1);
				$this->load->view('administrator/footer');*/
				$this->load->view('administrator/header',$data);
			//$this->load->view('showbannerlist', $data);
			redirect('administrator/users/show_users');
			$this->load->view('administrator/footer');
				//*********************************
		
			}else{
				$datalist = array(			
							'name' => $this->input->post('name'),
							'email' => $this->input->post('email'),
							'password' => base64_encode($this->input->post('password')),
							'phone' => $this->input->post('phone'),
							'adrs' => $this->input->post('address'),
							'user_type' => $this->input->post('user_type'),
							'username' => $this->input->post('username'),
							'description' => $this->input->post('description')
				
				);
				//print_r($datalist); exit();
				$id = $this->input->post('user_id');
				$data['title'] = "users Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/userst_model');
				//Transfering data to Model
				$query = $this->userst_model->users_edit($id,$datalist);
				//echo $ddd=$this->db->last_query();
				//exit();
				$data1['message'] = 'Data Update Successfully';
				$query = $this->userst_model->show_userslist();
				$data['ecms'] = $query;
				$this->session->set_flashdata('edit_message', 'users Updated Successfully !!!!');
				$data['title'] = "users Page List";
				/*$this->load->view('administrator/header',$data);
				$this->load->view('administrator/showuserslist', $data1);
				$this->load->view('administrator/footer');*/
				$this->load->view('administrator/header',$data);
			//$this->load->view('showbannerlist', $data);
			redirect('administrator/users/show_users');
			$this->load->view('administrator/footer');
			}
			
		}
//================Update Individual  users ***** END HERE====================

		//=====================DELETE NEWS====================

		
			function delete_users() {
			$id = $this->uri->segment(4);
			$result=$this->userst_model->show_users_id($id);
			//print_r($result);
			$imagename = $result[0]->imagename; 
			//echo $banner_img;exit();
			//Loading Database
			$this->load->database();

			//Transfering data to Model
			$query = $this->userst_model->delete_users($id,$imagename);
			$data['ecms'] = $query;
			$this->session->set_flashdata('delete_message', 'users Deleted Successfully !!!!');
			$this->load->view('administrator/header',$data);
			//$this->load->view('showbannerlist', $data);
			redirect('administrator/users/show_users');
			$this->load->view('administrator/footer');
		}

		//=====================DELETE NEWS====================

		//====================MULTIPLE DELETE=================
		function delete_multiple(){
		$ids = ( explode( ',', $this->input->get_post('ids') ));
		$this->userst_model->delete_mul($ids);
		$this->session->set_flashdata('delete_message1', 'users Deleted Successfully !!!!');
		$this->load->view('administrator/header',$data);
			//$this->load->view('showbannerlist', $data);
			redirect('administrator/users/show_users');
			$this->load->view('administrator/footer');
		}
		
		function users_view($id){
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/userst_model');
			//Transfering data to Model
			$query = $this->userst_model->users_view($id);
			$data['viewusers'] = $query;
			
			//echo $sql = $this->db->last_query();
			//exit();
			$data['title'] = "users View";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/users_view',$data);
			$this->load->view('administrator/footer');
			
		
	}
		//====================MULTIPLE DELETE=================
//======================Logout==========================
		public function Logout(){
        	$this->session->sess_destroy();
        	redirect('administrator/login');
    	}
//======================Logout==========================
}

?>