<?php
class Organizer extends CI_Controller {
		//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('administrator/home', 'refresh');
			}
			$this->load->model('administrator/organizer_model');
		$this->load->model('administrator/event_model');
			$this->load->library('image_lib');
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			
			//****************************backtrace prevent*** END HERE*************************
			
		}
		//============Constructor to call Model====================
		function index(){
			if($this->session->userdata('is_logged_in')){
				redirect('administrator/organizer/addorganizer');
			}else{
				$this->load->view('administrator/main/login');	
			}
		}
		
		//=======================Insert Page Data============
		function add_organizer(){
			$config = array(
			'upload_path' => "uploads/organizer",
			'upload_url' => base_url() . "uploads/organizer/",
			'allowed_types' => "gif|jpg|png|jpeg"
			);
			
			//load upload class library
        	$this->load->library('upload', $config);
				//=====================+++++++++++++++++++++++===================
				$this->form_validation->set_rules('organizer_name','Organizer Name', 'required');
				$this->form_validation->set_rules('organizer_email', 'Organizer Email', 'required|valid_email|is_unique[organizer.organizer_email]');
				$this->form_validation->set_error_delimiters('<div class="error" style="color:#ff0000;">', '</div>');
				//=====================+++++++++++++++++++++++===================
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('administrator/header');
					$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';
					$this->load->view('administrator/header');
            		$this->load->view('administrator/organizeradd_view',$data);
					$this->load->view('administrator/footer');
				}else{
					if (!$this->upload->do_upload('userfile')){
            			$data['success_msg'] = '<div class="alert alert-success text-center">You Must Select An Image File!</div>';
						$this->load->view('administrator/header');
            			$this->load->view('administrator/organizeradd_view', $data);
						$this->load->view('administrator/footer');
        			}else{
						 $data['userfile'] = $this->upload->data();
						 $filename=$data['userfile']['file_name'];
						 $data = array(
							'organizer_name' => $this->input->post('organizer_name'),
							'organizer_phone' => $this->input->post('organizer_phone'),
							'organizer_avatar' => $filename,
							'organizer_email' => $this->input->post('organizer_email'),
							'organizer_address' => $this->input->post('organizer_address'),
							'added_date' => date('Y-m-d H:i:s',strtotime($this->input->post('added_date'))),
							'organizer_category' => $this->input->post('organizer_category'),
							'organiser_website' => $this->input->post('organiser_website'),	
							'status' => 1
						);
						$this->organizer_model->insert_organizer($data);
            			$upload_data = $this->upload->data();
						$query = $this->organizer_model->show_organizer();
						$data['eorg'] = $query;
            			$data['success_msg'] = '<div class="alert alert-success text-center">Organizer Successfully Added!</div>';
						$this->session->set_flashdata('success_message', 'Organizer Added Susccessfully !!!');
						redirect('administrator/organizer/showorganizer',TRUE);
						/*$this->load->view('administrator/header',$data);
						$this->load->view('administrator/showorganizerlist',$data);
						$this->load->view('administrator/footer');*/
				
					}
				}
		}
		//=======================Insert Page Data============
		//================Add banner form=============
		function addorganizer(){
			$data['title'] = "Organizer add";
			$query1 = $this->event_model->show_category();
			$data['category'] = $query1;
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/organizeradd_view');
			$this->load->view('administrator/footer');
		}
		//================View Individual Data List=============
		
		//================View Individual Data List=============
  		//================Show Individual by Id=================
		function show_organizer_id($id) {
			$id = $this->uri->segment(4); 
			$data['title'] = "Edit Organizer";
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/organizer_model');
			//Transfering data to Model
			$query = $this->organizer_model->show_organizer_id($id);
			$data['eorg'] = $query;
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/organizer_edit', $data);
			$this->load->view('administrator/footer');
		}
		
		function statusbanner (){
			$stat= $this->input->get('stat'); 
			$id= $this->input->get('id');   
			$this->load->model('administrator/banner_model');
			$this->banner_model->updt($stat,$id);
		}
   		//================Show Individual by Id=================
  	 	//================Update Individual ====================
		function edit_organizer(){
			 //============================================
		  $organizer_avatar = $this->input->post('organizer_avatar');
		
			 $config = array(
				'upload_path' => "uploads/organizer/",
				'upload_url' => base_url() . "uploads/organizer/",
				'allowed_types' => "gif|jpg|png|jpeg"
			);
			$this->load->library('upload', $config);
			if ($this->upload->do_upload("userfile")) {
				@unlink("uploads/organizer/".$organizer_avatar);
				$data['img'] = $this->upload->data();
				//*********************************
				//============================================
				$datalist = array(			
					'organizer_name' => $this->input->post('organizer_name'),
					'organizer_avatar' => $data['img']['file_name'],
					'organizer_phone' => $this->input->post('organizer_phone'),
					'organizer_email' => $this->input->post('organizer_email'),
					'organizer_address' => $this->input->post('organizer_address'),
					'added_date' => $this->input->post('added_date'),
					'organizer_category' => $this->input->post('organizer_category'),
					'organiser_website' => $this->input->post('organiser_website'),
					'status'=>$this->input->post('status')
				);
				//print_r($datalist);
				//exit();
				$organizer_avatar = $this->input->post('organizer_avatar');
				//====================Post Data===================
				$id = $this->input->post('organizer_id');
				$data['title'] = "Organizer Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/banner_model');
				//Transfering data to Model
				$query = $this->organizer_model->organizer_edit($id,$datalist,$organizer_avatar);
				//echo $this->db->last_query();
				//exit();
				$data1['message'] = 'Data Update Successfully';
				$this->session->set_flashdata('success_message', 'Organizer Updated Susccessfully !!!');
				redirect('administrator/organizer/showorganizer',TRUE);
				//*********************************
		
			}else{
				$datalist = array(			
					'organizer_name' => $this->input->post('organizer_name'),
					'organizer_phone' => $this->input->post('organizer_phone'),
					'organizer_email' => $this->input->post('organizer_email'),
					'organizer_address' => $this->input->post('organizer_address'),
					'added_date' => $this->input->post('added_date'),
					'organizer_category' => $this->input->post('organizer_category'),
					'organiser_website' => $this->input->post('organiser_website'),
					'status'=>$this->input->post('status')
				);
				//====================Post Data===================
				$organizer_avatar = $this->input->post('organizer_avatar');
				$id = $this->input->post('organizer_id');
				$data['title'] = "Organizer Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/organizer_model');
				//Transfering data to Model
				$query = $this->organizer_model->organizer_edit($id,$datalist,$organizer_avatar);
				$this->session->set_flashdata('success_message', 'Organizer Updated Susccessfully !!!');
				redirect('administrator/organizer/showorganizer',TRUE);
			}
			
		}
		//=======================Delete Individual==============
		function delete_organizer($id){
			//Loading  Database
			$id = $this->uri->segment(4);
			$this->load->database();
			$query = $this->organizer_model->show_organizer_id($id);
			$organizer_img = $query[0]->organizer_avatar;
			//Transfering data to Model
			$this->organizer_model->delete_organizer($id,$organizer_img);
			$this->session->set_flashdata('success_message', 'Organizer Deleted Susccessfully !!!');
			redirect('administrator/organizer/showorganizer',TRUE);
		}
		//======================Delete Individual===============
		//======================Show CMS========================
		function showorganizer(){
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/organizer_model');
			//Transfering data to Model
			$query = $this->organizer_model->show_organizer();
			$data['eorg'] = $query;
			$data['title'] = "OrganizerList";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/showorganizerlist');
			$this->load->view('administrator/footer');
		
		}
				
		//======================Logout==========================
		public function Logout(){
        	$this->session->sess_destroy();
        	redirect('administrator/login');
    	}
		//======================Logout==========================
}

?>

