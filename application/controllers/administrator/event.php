<?php
class Event extends CI_Controller {
		//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('administrator/home', 'refresh');
			}
			$this->load->model('administrator/event_model');
			$this->load->database();
			$this->load->library('image_lib');
				//****************************backtrace prevent*** START HERE*************************
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			
			
			//****************************backtrace prevent*** END HERE*************************
		}
		//============Constructor to call Model====================
		function index(){
		if($this->session->userdata('is_logged_in')){
			redirect('event/event_view');
        }else{
        	$this->load->view('administrator/main/login');	
        }
	}
  //=======================Insert Page Data============
		/*function add_event(){
			$my_date = date("Y-m-d", time()); 
			 $config = array(
				'upload_path' => "course/",
				'upload_url' => base_url() . "course/",
				'allowed_types' => "gif|jpg|png|jpeg|pdf"
			);
			 $this->load->library('upload', $config);
			 if ($this->upload->do_upload("userfile")) {
				 //echo $image_data = $this->upload->data();
				 $data['img'] = $this->upload->data();
			 	 $data['img']['file_name'];
				//exit();
				//*********************************
				//============================================
				
				$data = array(
					'gallery_name' => $this->input->post('gallery_name'),
					'gallery_image' => $data['img']['file_name'],
					'gallery_status' => 1,
					'date' =>  $my_date
				);
				//Transfering data to Model
				$this->gallery_model->insert_gallery($data);
				$data1['message'] = 'Data Inserted Successfully';
				redirect('administrator/gallery/success');
			 }
			 else{
				   	$data = array(
					'gallery_name' => $this->input->post('gallery_name'),
					'gallery_image' => $data['img']['file_name'],
					'gallery_status' => 1,
					'date' =>  $my_date
				);
				//Transfering data to Model
				$this->gallery_model->insert_gallery($data);
				$data1['message'] = 'Data Inserted Successfully';
				redirect('administrator/gallery/success');
			}
		}*/
        
		//=====************Event Category Section*************=====
		function addcategoryview(){
			$data['title'] = "Gallery add";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/categoryadd_view');
			$this->load->view('administrator/footer');
		}
		//====================Insert Page Data===========
		//====================Add Category===============
		function add_category(){
			$this->form_validation->set_rules('category_name', 'Category Name', 'required|min_length[1]');
			if ($this->form_validation->run() == FALSE) {
			$data['success_msg'] = '<div class="alert alert-success text-center">Category Name Can Not Be Blank</div>';
			$data['title'] = "Add Category";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/categoryadd_view',$data);
			$this->load->view('footer');
			}else {
			//Setting values for tabel columns
			$data = array(
				'category_name' => $this->input->post('category_name'),
				'status' => 1
			 );
			//Transfering data to Model
			$this->event_model->insert_category($data);
			$this->session->set_flashdata('message_name', 'This is my message');
			$data1['message'] = 'Data Inserted Successfully';
			$this->session->set_flashdata('success_add', 'Category Added Successfully !!!!');
			redirect('administrator/event/showcategory');
			}
		}
		//====================Add Category===============
		
		//==================Show Category================
		
		function showticketsales(){
			$last = end($this->uri->segments); 
			$this->load->model('administrator/event_model');
			$queryfs = $this->event_model->show_free_sales($last);
			$queryps = $this->event_model->show_paid_sales($last);
			$queryft = $this->event_model->show_free_total($last);
			$querypt = $this->event_model->show_paid_total($last);
			$querysm = $this->event_model->show_paid_amount($last);
			$data['queryfs'] = $queryfs;
			$data['queryps'] = $queryps;
			$data['queryft'] = $queryft;
			$data['querypt'] = $querypt;
			$data['querysm'] = $querysm;
			$data['title'] = "Ticket Sales";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/ticketsales',$data);
			$this->load->view('administrator/footer');
		}
		//==================Show Category================
		//==================Show Category================
		
		function showcategory(){
			$data['title'] = "Category List";
			$query = $this->event_model->show_category();
			$data['category'] = $query;
			$data['title'] = "Category List";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/categorylist',$data);
			$this->load->view('administrator/footer');
		}
		//==================Show Category================
		//==================Edit category================
		function edit_category(){
			//====================Post Data=====================
			$datalist = array(
				'category_name' =>$this->input->post('category_name'),
				'status' => $this->input->post('status')
			);
			//====================Post Data===================
			$id = $this->input->post('category_id');
			$data['title'] = "Category List";
			$query = $this->event_model->edit_category($id,$datalist);
			$this->session->set_flashdata('success_update', 'Category Updated Successfully !!!!');
			redirect('administrator/event/showcategory');
			//****************************
			
			
		}
		
		//================Show Category By Id================
		function show_category_id($id) {
		$id = $this->uri->segment(4); 
		$data['title'] = "Edit Category";
		$query = $this->event_model->show_category_id($id);
		$data['ecategory'] = $query;
		$this->load->view('administrator/header',$data);
		$this->load->view('administrator/category_edit', $data);
		$this->load->view('administrator/footer');
	}
   	//================Show Category By Id================
	//================Delete Category=================	
		function delete_category($id){
			$id = $this->uri->segment(4);
			$result=$this->event_model->delete_category($id); 
			$this->session->set_flashdata('success_delete', 'Category Deleted Successfully !!!!');
			redirect('administrator/event/showcategory');
		}
	//================Delete Category=================	
	
	//=====************Event Category Section*************=====
		function addtypeview(){
			$data['title'] = "Add Event Type";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/typeadd_view');
			$this->load->view('administrator/footer');
		}
		//====================Insert Page Data===============
		//====================Add Event Type=================
		function add_type(){
			$this->form_validation->set_rules('type_name', 'Type Name', 'required|min_length[1]');
			if ($this->form_validation->run() == FALSE) {
				$data['success_msg'] = '<div class="alert alert-success text-center">Type Name Can Not Be Blank</div>';
				$data['title'] = "Add Event Type";
				$this->load->view('administrator/header',$data);
				$this->load->view('administrator/typeadd_view',$data);
				$this->load->view('footer');
			}else {
			//Setting values for tabel columns
				$data = array(
					'type_name' => $this->input->post('type_name'),
					'status' => '1'
				 );
				//Transfering data to Model
				$this->event_model->insert_type($data);
				$this->session->set_flashdata('success_add', 'Event Type Added Successfully !!!!');
				redirect('administrator/event/showtype');
			}
		}
		//====================Add Event Type=================
		//==================Show Event Type==================
		function showtype(){
			$data['title'] = "Event Type List";
			$query = $this->event_model->show_type();
			$data['type'] = $query;
			$data['title'] = "Event Type List";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/typelist',$data);
			$this->load->view('administrator/footer');
		}
		//==================Show Event Type==================
		//==================Edit category====================
		function edit_type(){
			//====================Post Data=====================
			$datalist = array(
				'type_name' =>$this->input->post('type_name'),
				'status' => $this->input->post('status')
			);
			//====================Post Data===================
			$id = $this->input->post('type_id');
			$data['title'] = "Event Type List";
			$query = $this->event_model->edit_type($id,$datalist);
			$this->session->set_flashdata('success_update', 'Event Type Updated Successfully !!!!');
			redirect('administrator/event/showtype');
			//****************************
		}
		
		//================Show Category By Id================
		function show_type_id($id) {
			$id = $this->uri->segment(4); 
			$data['title'] = "Edit Event Type";
			$query = $this->event_model->show_type_id($id);
			$data['etype'] = $query;
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/type_edit', $data);
			$this->load->view('administrator/footer');
		}
   		//================Show Category By Id================
		//================Delete Category====================	
		function delete_type($id){
			$id = $this->uri->segment(4);
			$result=$this->event_model->delete_type($id); 
			$this->session->set_flashdata('success_delete', 'Event Type Deleted Successfully !!!!');
			redirect('administrator/event/showtype');
		}
	
	//=====************Event Type Section*****************=====
	
	
	//*********===============Event Section===============********//
		function addeventview(){
			$data['title'] = "Add Event";
			$query = $this->event_model->show_type();
			$data['type'] = $query;
			
			$query1 = $this->event_model->show_category();
			$data['category'] = $query1;
			
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/eventadd_view',$data);
			$this->load->view('administrator/footer');
		}
		//====================Insert Page Data===============
		//====================Add Event Type=================
		function add_event(){
			$config = array(
				'upload_path' => "uploads/event/",
				'upload_url' => base_url() . "uploads/event/",
				'allowed_types' => "gif|jpg|png|jpeg"
			);
			
			//load upload class library
        	$this->load->library('upload', $config);
				//=====================+++++++++++++++++++++++===================
				$this->form_validation->set_rules('event_name','Event name', 'required');
				$this->form_validation->set_rules('location', 'Location', 'required|min_length[1]');
				$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
				//=====================+++++++++++++++++++++++===================
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('administrator/header');
					$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';
					$this->load->view('administrator/header');
            		$this->load->view('administrator/eventadd_view',$data);
					$this->load->view('administrator/footer');
					//redirect('banner/addbanner',$data);
				}else{
					if (!$this->upload->do_upload('userfile')){
            			$data['success_msg'] = '<div class="alert alert-success text-center">You Must Select An Image File!</div>';
						$this->load->view('administrator/header');
            			$this->load->view('administrator/eventadd_view', $data);
						$this->load->view('administrator/footer');
        			}else{
						 $data['userfile'] = $this->upload->data();
						 $filename=$data['userfile']['file_name'];
						 $data = array(
							'event_name' => $this->input->post('event_name'),
							'start_date_time' => date('Y-m-d H:i:s',strtotime($this->input->post('start_date_time'))),
							'end_date_time' => date('Y-m-d H:i:s',strtotime($this->input->post('end_date_time'))),
							'location' => $this->input->post('location'),
							'organiser' => $this->input->post('organiser'),
							'post_date' => date('Y-m-d H:i:s'),
							'event_price' => $this->input->post('event_price'),
							'event_type' => $this->input->post('event_type'),
							'hot_status' => $this->input->post('hot_status'),
							'event_category' => $this->input->post('event_category'),
							'ticket_quantity' => $this->input->post('event_quantity'),
							'event_description' => $this->input->post('event_description'),
							'event_image' => $filename,
							'status' => 1
						);
						$this->event_model->insert_event($data);
            			$upload_data = $this->upload->data();
						$this->session->set_flashdata('success_add', 'Event  Added Successfully !!!!');
						redirect('administrator/event/showevent');
				
					}
				}
		}
		//====================Add Event Type=================
		//==================Show Event Type==================
		function showevent(){
			$data['title'] = "Event Type List";
			$query = $this->event_model->show_event();
			$data['event'] = $query;
			$data['title'] = "Event List";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/eventlist',$data);
			$this->load->view('administrator/footer');
		}
		//==================Show Event Type==================
		//==================Edit category====================
		function edit_event(){
		//============================================
		 $event_img = $this->input->post('event_image');
			$config = array(
				'upload_path' => "uploads/event/",
				'upload_url' => base_url() . "uploads/event/",
				'allowed_types' => "gif|jpg|png|jpeg"
			);
			$this->load->library('upload', $config);
			if ($this->upload->do_upload("userfile")) {
				//echo $path = base_url(). "banner/";exit();
				//echo $path1 = "banner/"; 
				@unlink("uploads/event/".$event_img);
				
				//echo $image_data = $this->upload->data();
				$data['img'] = $this->upload->data();
				//*********************************
				//============================================
				$datalist = array(			
				//**********************************************
				'event_name' => $this->input->post('event_name'),
				'start_date_time' => date('Y-m-d H:i:s',strtotime($this->input->post('start_date_time'))),
				'end_date_time' => date('Y-m-d H:i:s',strtotime($this->input->post('end_date_time'))),
				'location' => $this->input->post('location'),
				'organiser' => $this->input->post('organiser'),
				'post_date' => date('Y-m-d H:i:s'),
				'event_price' => $this->input->post('event_price'),
				'event_type' => $this->input->post('event_type'),
				'hot_status' => $this->input->post('hot_status'),
				'event_category' => $this->input->post('event_category'),
				'ticket_quantity' => $this->input->post('ticket_quantity'),
				'event_description' => $this->input->post('event_description'),
				'event_image' => $data['img']['file_name'],
				'status' => $this->input->post('status')
				//**********************************************
				);
				//print_r($datalist); exit();
				 $event_img = $this->input->post('event_image');
				//====================Post Data===================
				
				$id = $this->input->post('event_id');
				$data['title'] = "Edit Event";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/event_model');
				//Transfering data to Model
				$query = $this->event_model->event_edit($id,$datalist);
				$data1['message'] = 'Data Update Successfully';
				$query = $this->event_model->show_event();
				$data['eevent'] = $query;
				$data['title'] = "Event Page List";
				$this->session->set_flashdata('success_update', 'Event Updated Successfully !!!!');
				redirect('administrator/event/showevent',TRUE);
				/*$this->load->view('administrator/header',$data);
				$this->load->view('administrator/showeventlist', $data1);
				$this->load->view('administrator/footer');*/
				//*********************************
		
			}else{
				$datalist = array(			
				//**********************************************
				'event_name' => $this->input->post('event_name'),
				'start_date_time' => date('Y-m-d H:i:s',strtotime($this->input->post('start_date_time'))),
				'end_date_time' => date('Y-m-d H:i:s',strtotime($this->input->post('end_date_time'))),
				'location' => $this->input->post('location'),
				'organiser' => $this->input->post('organiser'),
				'post_date' => date('Y-m-d H:i:s'),
				'event_price' => $this->input->post('event_price'),
				'event_type' => $this->input->post('event_type'),
				'hot_status' => $this->input->post('hot_status'),
				'event_category' => $this->input->post('event_category'),
				'ticket_quantity' => $this->input->post('ticket_quantity'),
				'event_description' => $this->input->post('event_description'),
				'status' => $this->input->post('status')
				//**********************************************
				);
				//print_r($datalist);
				//exit();
				//====================Post Data===================
				$event_img = $this->input->post('event_img');
				$id = $this->input->post('event_id');
				$data['title'] = "Event Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/event_model');
				//Transfering data to Model
				$query = $this->event_model->event_edit($id,$datalist);
				$data1['message'] = 'Data Update Successfully';
				$query = $this->event_model->show_event();
				$data['eevent'] = $query;
				$data['title'] = "Event Page List";
				$this->session->set_flashdata('success_update', 'Event Updated Successfully !!!!');
				redirect('administrator/event/showevent',TRUE);
			}
			
		}
		
		//================Show Category By Id================
		function show_event_id($id) {
			$id = $this->uri->segment(4); 
			$data['title'] = "Edit Event Type";
			
			$query = $this->event_model->show_event_id($id);
			$data['eevent'] = $query;
			
			$query1 = $this->event_model->show_type();
			$data['type'] = $query1;
			
			$query2 = $this->event_model->show_category();
			$data['category'] = $query2;
			
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/event_edit', $data);
			$this->load->view('administrator/footer');
		}
   		//================Show Category By Id================
		//================Delete Category====================	
		function delete_event($id){
			$id = $this->uri->segment(4);
			$result=$this->event_model->delete_event($id); 
			$this->session->set_flashdata('success_delete','Event Deleted Successfully !!!!');
			redirect('administrator/event/showevent');
		}
	//*********===============Event Section===============********//
	
	//======================Logout==========================
	function logout(){
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('administrator/home', 'refresh');
	}
		//======================Logout==========================
}

?>
