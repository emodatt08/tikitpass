<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Organizersignup extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	$this->load->database();
	$this->load->library('session');
	$this->load->model('organisation_model');
	$this->load->model('administrator/event_model');
	$this->load->model('signup_model');
	$this->load->model('home_model');
}
	public function index(){
		$data['title'] = "Organizer Signup";
		
		$this->load->model('administrator/event_model');
		
		$query = $this->event_model->show_type();
		$data['type'] = $query;
		
		$query1 = $this->event_model->show_category();
		$data['category'] = $query1;
		
	    $this->load->view('headerlogin',$data);
		$this->load->view('signuporganisation',$data);
		$this->load->view('footerlogin',$data);
	}
function registration(){
		$date1 = date('d-m-Y g:i:s a');  
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[organizer.organizer_email]');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata("email_sent","Email Or UserName Allready Exists !!!"); 
			redirect('organizersignup');
		}else{
			$data = array(
				'organizer_name' => $this->input->post('name'),
				'organizer_email' => $this->input->post('email'),
				'organizer_phone' => $this->input->post('phone'),
				'organizer_password' => base64_encode($this->input->post('password')),
				'added_date'=> date('Y-m-d H:i:s'),
				'status' => '1'
			);	
			$query = $this->signup_model->register_org($data);
			$from_email = "info@tikitpass.com";
			$to_email = $this->input->post('email');
			 $data1 = array(
						'organizer_name' => $this->input->post('name'),
						'organizer_email' => $this->input->post('email'),
						'organizer_phone' => $this->input->post('phone'),
						'organizer_password' => base64_encode($this->input->post('password')),
						'added_date'=> date('Y-m-d H:i:s')
				   );
			$msg = $this->load->view('mailtemplate',$data1,TRUE);
		    $config['mailtype'] = 'html';
		    $this->load->library('email');
		    $this->email->initialize($config);
		    $msg = $msg;
		    $subject = 'Registration Success Message From TicketPass.';   
            $this->email->from($from_email, 'TicketPass Administrator'); 
		    $this->email->to($to_email);
            $this->email->subject($subject); 
            $this->email->message($msg);
			   
            if($this->email->send()){
               //$query = $this->signup_model->register($data);
				$this->session->set_flashdata("success", "<b style='color:green;'>You Have Registered Successfully !!!</b>");
			    redirect('organizersignup');
		        }
          else{          
		        // $query = $this->signup_model->register($data);
				 $this->session->set_flashdata("success", "<b style='color:blue;'>You Have Registered Successfully !!!</b>");
			     redirect('organizersignup');
	          }
	     }
				
	 }
	//==============================Logout==============================================
    public function logout(){
		$this->session->sess_destroy();
		redirect('login/login');
	}
	//==============================Logout==============================================

}
?>