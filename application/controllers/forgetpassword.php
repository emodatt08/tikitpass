<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Forgetpassword extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	$this->load->database();
	$this->load->model('forgetpassword_model');
	$this->load->model('home_model');
}
	public function index(){
		$data['title'] = "Forget Password";
	    $this->load->view('headerlogin',$data);
		$this->load->view('forgetpassword',$data);
		$this->load->view('footerlogin',$data);
	}
//==================================Form validation===========================================
	public function email_validation(){
		$this->load->model('forgetpassword_model');
		$this->load->database();
		//$query = $this->forgetpassword_model->user_forgetpass($this->input->post('email'));
		$query = $this->forgetpassword_model->get_user_forgetpass($this->input->post('email'));
		if(@$query!=''){
			$password = base64_decode($query[0]->password);
			$from_email = "info@tikitpass.com";
			$to_email = $query[0]->email;
			$data1 = array(
				'password' => base64_encode($password)
			);
			$msg = $this->load->view('mailtemplatefp',$data1,TRUE);
			$config['mailtype'] = 'html';
			$this->load->library('email');
			$this->email->initialize($config);
			$msg = $msg;
			$subject = 'Password Change Message From TicketPass.';   
			$this->email->from($from_email, 'TicketPass Administrator'); 
			$this->email->to($to_email);
			$this->email->subject($subject); 
			$this->email->message($msg);
			if($this->email->send()){
				$this->session->set_flashdata("success", "<b style='color:green;'>Password Sent to Your Registered Email Successfully !!!</b>");
					redirect('forgetpassword');
			}else{          
				$this->session->set_flashdata("success", "<b style='color:blue;'>Some Problem Occurred , Please try again later</b>");
				redirect('forgetpassword');
			}
		}else{
			$this->session->set_flashdata('logerror', 'Invalid Email !!!');
			redirect('forgetpassword','refresh');
		}
	}
	//================================Form Validation==========================================
	//==============================Logout==============================================
    public function logout(){
		$this->session->sess_destroy();
		redirect('forgotpassword/forgotpassword');
	}
	//==============================Logout==============================================
}

?>