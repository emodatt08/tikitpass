<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//include_once (dirname(__FILE__) . "/my_controller.php");
class Home extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	$this->load->model('administrator/banner_model');
	$this->load->model('event_model');
	//$this->load->model('home_model');
	$this->load->database();
	$this->load->model('home_model');
}
	public function index(){

		$query = $this->banner_model->show_banner();
		$data['ebanner'] = $query;
		
		$query1 = $this->home_model->show_ad();
		$data['ead'] = $query1;
		
		$query2 = $this->home_model->show_event();
		$data['eevent'] = $query2;
		
		$query3 = $this->home_model->show_social();
		$data['esocial'] = $query3;
		
		$hotquery = $this->event_model->show_hot_event();
		$data['hotcategory']=$hotquery;
		
		$latestevent=$this->event_model->show_latest_event();
		$data['latestevent']=$latestevent;
		
		$data['title'] = "Home";

	    $this->load->view('header',$data);
		$this->load->view('index',$data);
		$this->load->view('footer',$data);
	}
}
