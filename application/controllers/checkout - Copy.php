<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//include_once (dirname(__FILE__) . "/my_controller.php");
class Checkout extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	//$this->load->model('home_model');
	$this->load->database();
	$this->load->model('home_model');
	$this->load->model('event_model');
	$this->load->model('checkout_model');
	$this->load->model('login_model');
	$this->load->library('paypal_lib');
	$this->load->library('Pdf');
	$this->load->library('email');
	//$this->load->model('eventdetails_model');
}
	public function index(){
		
		$data['title'] = "Event Details";
	    $this->load->view('headerlogin',$data);
		$this->load->view('checkout',$data);
		$this->load->view('footerlogin',$data);
	}
	
	public function book_ticket(){
		$this->load->library(array('form_validation','session'));
		
		if(!$this->session->userdata('is_userlogged_id')){
		redirect('login', 'refresh');
		}
		$this->session->userdata('is_userlogged_id');
		$data = array(
			'eventid' => $this->input->post('eventid'),
			'ticketid' => $this->input->post('ticketid'),
			'ticketno' => $this->input->post('ticketno'),
			'eventname' => $this->input->post('eventname'),
			'eventdate' => $this->input->post('eventdate'),
			'ticketprice' => $this->input->post('ticketprice'),
			'tickettype' => $this->input->post('ticket_type'),
			'ticketquantitypaid' => $this->input->post('ticketquantitypaid'),
			'ticketquantityfree' => $this->input->post('ticketquantityfree')
		);
		//print_r($data); exit();
		
		$data['title'] = "Event Details";
	    $this->load->view('headerlogin',$data);
		$this->load->view('checkout',$data);
		$this->load->view('footerlogin',$data);
	}
	public function payment(){
		
		$this->load->library(array('form_validation','session'));
		
		if(!$this->session->userdata('is_userlogged_id')){
		redirect('login', 'refresh');
		}
		$this->session->userdata('is_userlogged_id');
		$rand = random_string('alnum', 8);
		$ordernum = $this->session->userdata('is_userlogged_id').$rand.$this->input->post('ticket_num');
		$data = array(
			'eve_id' => $this->input->post('eve_id'),
			'order_no' => $ordernum,
			'tic_id' => $this->input->post('tic_id'),
			'ticket_num' => $this->input->post('ticket_num'),
			'ticket_price' => $this->input->post('ticket_price'),
			'event_name' => $this->input->post('event_name'),
			'user_id' => $this->input->post('user_id'),
			'customer_name' => $this->input->post('customer_name'),
			'customer_email' => $this->input->post('customer_email'),
			'customer_phone' => $this->input->post('customer_phone'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'postalcode' => $this->input->post('postalcode'),
			'address' => $this->input->post('address'),
			'status' => 1
		);
		//print_r($data); echo "<pre>";
		$databook = array(
			'event_id' => $this->input->post('eve_id'),
			'orderno' => $ordernum,
			'tkt_id' => $this->input->post('ticket_num'),
			'amount' => $this->input->post('ticket_price'),
			'ticket_type' => $this->input->post('ticket_type'),
			'pay_status' => 2,
			'uid' => $this->input->post('user_id'),
			'date_of_booking' => date('Y-m-d h:i:s'),
			'booking_status' => 1
		);
		//print_r($databook); exit();
		
		$query = $this->checkout_model->insert($data);
		$query = $this->checkout_model->insertbook($databook);
		//echo $this->db->last_query(); exit();
		
		if($this->input->post('ticket_price')==0){
		 $eventid = $this->input->post('eve_id'); 
		//print_r($data); exit();
	    /*$this->load->view('header');
		$this->load->view('freeticket');
		$this->load->view('footer');*/
		$sesid =  $this->session->userdata('is_userlogged_id');;
		$sesusrname = $this->login_model->show_sesname($sesid);
		$data['name'] = $sesusrname;
		$data['username'] = $sesusrname[0]->name;
		$data['useremail'] = $sesusrname[0]->email;
			
			$hotquery = $this->event_model->show_my_event($eventid);
			$data['event'] = $hotquery;
			$data['evid'] = $this->input->post('eve_id'); 
			$data['quantity'] = $this->input->post('ticketquantity');
			//echo $this->db->last_query(); exit();
			$data['location'] = $hotquery[0]->location;
			$data['ticid'] = $hotquery[0]->tkt_id;
			$data['eventname'] = $hotquery[0]->event_name;
			$data['paystatus'] = "FREE";
			$data['start_date_time'] = date('l, jS F, Y \f\r\o\m h:i A',strtotime($hotquery[0]->start_date_time));
			$data['orderno'] = $hotquery[0]->orderno;
			$data['from_time'] = date('H:i A',strtotime($hotquery[0]->start_date_time));
			$data['to_time'] = date('H:i A',strtotime($hotquery[0]->end_date_time));
			$data['price'] = $hotquery[0]->amount;
			$data['image'] = $hotquery[0]->event_image;
			//$data['username'] = $this->session->set_userdata('is_userlogged_in',$email);
		    $data['title']="Ticket";
			$this->load->view('pdfticket',$data);
		
		//redirect('pdfexample',$eventid);
		
		}else{
		$returnURL = base_url().'paypal/success'; //payment success url
		$cancelURL = base_url().'paypal/cancel'; //payment cancel url
		$notifyURL = base_url().'paypal/ipn'; //ipn url
		//get particular product data
		//$logo = base_url().'assets/images/codexworld-logo.png';
		
		$this->paypal_lib->add_field('return', $returnURL);
		$this->paypal_lib->add_field('cancel_return', $cancelURL);
		$this->paypal_lib->add_field('notify_url', $notifyURL);
		$this->paypal_lib->add_field('item_name',$this->input->post('event_name'));
		$this->paypal_lib->add_field('custom', $this->input->post('user_id'));
		$this->paypal_lib->add_field('item_number', $this->input->post('ticket_num'));
		$this->paypal_lib->add_field('amount', $this->input->post('ticket_price'));		
		
		$this->paypal_lib->paypal_auto_form();
		}
		}
}
