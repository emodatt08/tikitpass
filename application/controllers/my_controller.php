<?php
Class MY_Controller Extends CI_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->database();
		$this->load->model('home_model');
    }
	public function menu() {
    	$data['menu_list'] = $this->home_model->get_menu();
    	$data['submenu_list'] = $this->home_model->get_submenu();
    	$this->load->view('menu', $data);
	}
}
?>