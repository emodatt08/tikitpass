<?php

/**
 * Created by PhpStorm.
 * User: Ripper
 * Date: 3/4/2017
 * Time: 7:31 PM
 */
class curl_get
{

    public static function curl_post($url, $params){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (TransFlow; OS 1_0_1) Version/2.0.2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        //curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        curl_setopt($ch, CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
        $result = curl_exec($ch);
       return $result;

    }

}