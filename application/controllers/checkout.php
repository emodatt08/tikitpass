<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/curl_get.php");
class Checkout extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	//$this->load->model('home_model');
	$this->load->database();
	$this->load->model('home_model');
	$this->load->model('event_model');
	$this->load->model('checkout_model');
	$this->load->model('login_model');
     $this->load->model('user_model');
	$this->load->library('paypal_lib');
	$this->load->library('Pdf');
	$this->load->library('email');
	//$this->load->model('eventdetails_model');
}
	public function index(){
		
		$data['title'] = "Event Details";
	    $this->load->view('headerlogin',$data);
		$this->load->view('checkout',$data);
		$this->load->view('footerlogin',$data);
	}
	
	public function book_ticket(){
		$this->load->library(array('form_validation','session'));
		
		if(!$this->session->userdata('is_userlogged_id')){
		redirect('login', 'refresh');
		}
		$this->session->userdata('is_userlogged_id');
		$data = array(
			'eventid' => $this->input->post('eventid'),
			'ticketid' => $this->input->post('ticketid'),
			'ticketno' => $this->input->post('ticketno'),
			'eventname' => $this->input->post('eventname'),
			'eventdate' => $this->input->post('eventdate'),
			'ticketprice' => $this->input->post('ticketprice'),
			'tickettype' => $this->input->post('ticket_type'),
			'ticketquantitypaid' => $this->input->post('ticketquantitypaid'),
			'ticketquantityfree' => $this->input->post('ticketquantityfree')
		);
		//print_r($data); exit();
		
		$data['title'] = "Event Details";
	    $this->load->view('headerlogin',$data);
		$this->load->view('checkout',$data);
		$this->load->view('footerlogin',$data);
	}
	public function payment(){
		
		$this->load->library(array('form_validation','session'));
		
		if(!$this->session->userdata('is_userlogged_id')){
		redirect('login', 'refresh');
		}
		$this->session->userdata('is_userlogged_id');
		$rand = random_string('alnum', 8);
		$ordernum = $this->session->userdata('is_userlogged_id').$rand.$this->input->post('ticket_num');
		$data = array(
			'eve_id' => $this->input->post('eve_id'),
			'order_no' => $ordernum,
			'tic_id' => $this->input->post('tic_id'),
			'ticket_num' => $this->input->post('ticket_num'),
			'ticket_price' => $this->input->post('ticket_price'),
			'event_name' => $this->input->post('event_name'),
			'user_id' => $this->input->post('user_id'),
			'customer_name' => $this->input->post('customer_name'),
			'customer_email' => $this->input->post('customer_email'),
			'customer_phone' => $this->input->post('customer_phone'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'postalcode' => $this->input->post('postalcode'),
			'address' => $this->input->post('address'),
			'status' => 1
		);
		//print_r($data); echo "<pre>";
		$databook = array(
			'event_id' => $this->input->post('eve_id'),
			'orderno' => $ordernum,
			'tkt_id' => $this->input->post('ticket_num'),
			'amount' => $this->input->post('ticket_price'),
			'ticket_type' => $this->input->post('ticket_type'),
			'pay_status' => 2,
			'uid' => $this->input->post('user_id'),
			'date_of_booking' => date('Y-m-d h:i:s'),
			'booking_status' => 1
		);
		//print_r($databook); exit();
		
		$query = $this->checkout_model->insert($data);
		$query = $this->checkout_model->insertbook($databook);
		//echo $this->db->last_query(); exit();
		
		if($this->input->post('ticket_price')==0){
		 $eventid = $this->input->post('eve_id'); 
		//print_r($data); exit();
	    /*$this->load->view('header');
		$this->load->view('freeticket');
		$this->load->view('footer');*/
		$sesid =  $this->session->userdata('is_userlogged_id');;
		$sesusrname = $this->login_model->show_sesname($sesid);
		$data['name'] = $sesusrname;
		$data['username'] = $sesusrname[0]->name;
		$data['useremail'] = $sesusrname[0]->email;
			
			$hotquery = $this->event_model->show_my_event($eventid);
			$data['event'] = $hotquery;
			$data['evid'] = $this->input->post('eve_id'); 
			//echo $this->input->post('ticketquantity'); exit();
			$data['quantity'] = $this->input->post('ticketquantity');
			//echo $this->db->last_query(); exit();
			$data['location'] = $hotquery[0]->location;
			$data['ticid'] = $hotquery[0]->tkt_id;
			$data['eventname'] = $hotquery[0]->event_name;
			$data['paystatus'] = "FREE";
			$data['start_date_time'] = date('l, jS F, Y \f\r\o\m h:i A',strtotime($hotquery[0]->start_date_time));
			$data['orderno'] = $hotquery[0]->orderno;
			$data['from_time'] = date('H:i A',strtotime($hotquery[0]->start_date_time));
			$data['to_time'] = date('H:i A',strtotime($hotquery[0]->end_date_time));
			$data['price'] = $hotquery[0]->amount;
			$data['image'] = $hotquery[0]->event_image;
			//$data['username'] = $this->session->set_userdata('is_userlogged_in',$email);
		    $data['title']="Ticket";
			$this->load->view('pdfticket',$data);
		
		//redirect('pdfexample',$eventid);
		
		}else{
//----------------Payment option check------------------------//		
		if($this->input->post('choosepaymentoption')==2){
			$returnURL = base_url().'paypal/success'; //payment success url
			$cancelURL = base_url().'paypal/cancel'; //payment cancel url
			$notifyURL = base_url().'paypal/ipn'; //ipn url
			//get particular product data
			//$logo = base_url().'assets/images/codexworld-logo.png';
			
			$this->paypal_lib->add_field('return', $returnURL);
			$this->paypal_lib->add_field('cancel_return', $cancelURL);
			$this->paypal_lib->add_field('notify_url', $notifyURL);
			$this->paypal_lib->add_field('item_name',$this->input->post('event_name'));
			$this->paypal_lib->add_field('custom', $this->input->post('user_id'));
			$this->paypal_lib->add_field('item_number', $this->input->post('ticket_num'));
			$this->paypal_lib->add_field('amount', $this->input->post('ticket_price'));		
			
			$this->paypal_lib->paypal_auto_form();
		}elseif($this->input->post('choosepaymentoption')==1){
			$data['customer_name']=$this->input->post('customer_name');
			$data['customer_email']=$this->input->post('customer_email');
			$data['eve_id']=$this->input->post('eve_id');
			$data['tic_id']=$this->input->post('tic_id');
			$data['ticket_price']=$this->input->post('ticket_price');
			$this->load->view('header',$data);
			$this->load->view('paystripe',$data);
			$this->load->view('footer');
		}elseif($this->input->post('choosepaymentoption')==3){

            $data['customer_name']=$this->input->post('customer_name');
            $data['customer_email']=$this->input->post('customer_email');
            $data['eve_id']=$this->input->post('eve_id');
            $data['eventname']= $this->input->post('event_name');
            $data['tic_id']=$this->input->post('tic_id');
            $data['ticket_price']=$this->input->post('ticket_price');
            $this->load->model('user_model');
            $mobile=$this->user_model->userdetails($this->input->post('customer_email'));
            $data['user_mobile'] = $mobile;
            $this->load->view('header',$data);
            $this->load->view('paymtn',$data);
            $this->load->view('footer');



        }
	  }
		}
	public function charge(){
	try {
		require_once('Stripe/lib/Stripe.php');
		Stripe::setApiKey("sk_test_nw5775lExDCwMa52kpWEkNV0");

		$charge = Stripe_Charge::create(array(
			"amount" =>$_POST['amount'],
			"currency" => "USD",
			"card" => $_POST['stripeToken'],
			"description" => "Charge for Facebook Login code."
		));
//echo "<pre>";
//print_r($charge);
	
	//echo "<h1>Your payment has been completed</h1>";
  //echo $_POST['stripeEmail'];
}

catch(Stripe_CardError $e) {
	
}
 catch (Stripe_InvalidRequestError $e) {
} catch (Stripe_AuthenticationError $e) {
  
} catch (Stripe_ApiConnectionError $e) {
  
} catch (Stripe_Error $e) {

} catch (Exception $e) {
}	
    //echo ">>>>>>>>>>>>>>>>>". [_values:protected][captured] ;

$chargeArray = $charge->__toArray(true);
		
 if($chargeArray['captured']==1)
 {
 	$payid=$this->session->userdata('lastid');
	$evid=$this->session->userdata('evt_id');
	$qty=$this->session->userdata('qty');
	$data = array(
			'pay_status' => '1',
	);
	$upquery = $this->event_model->update_pay_status($payid,$data);
	//echo $this->db->last_query();
	//exit();
	$eventid = $this->db->insert_id();
	//$this->session->set_flashdata('success', 'Payment Done Successfully !!');
	//redirect('eventdetails/show_event_details/'.$evid.'','refresh');
	redirect('eventdetails/paysuccess','refresh');
   //echo "Payment successfull";
 }
	else
	{
	//$this->session->set_flashdata('success', 'Error occure');
	//echo "Payment unsuccessfull";
	$payid=$this->session->userdata('lastid');
	$evid=$this->session->userdata('evt_id');
	$data = array(
			'pay_status' => '3',
	);
	$upquery = $this->event_model->update_pay_status($payid,$data);
	//echo $this->db->last_query();
	//exit();
	//$this->session->set_flashdata('success', 'Your Payment is under processing !!');
	//redirect('eventdetails/show_event_details/5/'.$evid.'','refresh');
	redirect('eventdetails/payreject','refresh');
	}
 //redirect('userdashboard/ratecard');	

}
public function mobilemoney(){
    $mobileno = $this->input->post('mobileno');
    $remove = substr($mobileno,-10);
    $newno = "233".$remove;
   // var_dump($newno);die();
    $paymenttype = $this->input->post('eventname');
    $price = $this->input->post('price');

    $params = array(
        'name'=>'Ticketpass',
        'info' => 'Payment for '.$paymenttype. ' ticket',
        'amt' => '0.01',
        'mobile' => $newno,
        'mesg' => 'Payment for '.$paymenttype. ' ticket',
        'expiry'=>'',
        'billprompt'=>'2',
        'thirdpartyID'=>'23233535353535',
        'username'=>'f3fQozNEc',
        'password'=>'HUcly3rpy',
        'op'=>'postInvoice',
        'env'=>'live'
    );

    foreach($params as $key=>$value):
        $new[$key] = base64_encode($value);
    endforeach;
    $url = 'http://payments.api.transflowservices.com/html/TFmethods.php';
   $setup = curl_get::curl_post($url,$new);
    $ans= json_decode($setup);

   if($ans->result->responseCode == "0000") {

       $this->load->model('checkout_model');
       $data = array(
           'ticket_price' => $price,
           'mm_amount' => $params['amt'],
           'mm_number' => $params['mobile'],
           'mm_message' => $params['mesg'],
           'customer_name' => $this->input->post('cusname'),
           'customer_email' => $this->input->post('cusemail'),
           'eve_name' => $this->input->post('evename'),
           'invoice_no' => $ans->result->invoiceNo,
           'status' => 'Pending'


       );
       $insert = $this->checkout_model->mtn_entry($data);

       if ($this->checkout_model->mtn_check($ans->result->invoiceNo)[0]->status == "Pending") {

           $data['title'] = "Mobile Money Payment Pending";
           $data['topsuccess'] = "Thank You For paying with mobile money";
           $data['bottomsuccess'] = "Please Check Your phone and follow the bill prompt or message to pay for your ticket";
           $this->load->view('header', $data);
           $this->load->view('paypendingmtn', $data);
           $this->load->view('footer');
       } elseif ($this->checkout_model->mtn_check($ans->result->invoiceNo)[0]->status == "Paid") {

           $data['title'] = "Mobile Money Payment Successful";
           $data['topsuccess'] = "Thank you for paying with mobile money";
           $data['bottomsuccess'] = "Please Check Your Account To Download Your Ticket";
           $this->load->view('header', $data);
           $this->load->view('paypending', $data);
           $this->load->view('footer');
       }
   }else{
       $data['title'] = "Mobile Money Payment Error";
       $data['topsuccess'] = "Something went wrong please contact the administrator";
       $data['bottomsuccess'] = "Error";
       $this->load->view('header', $data);
       $this->load->view('paypending', $data);
       $this->load->view('footer');

   }


}
    public function mobilecallback()
    {
    	header('Content-type: application/json');
		header("access-control-allow-origin: *");
        $data = json_decode(trim(file_get_contents('php://input')));
        if($data){

        $transactionId = $data->transactionId;
        $IdinvoiceNo = $data->IdinvoiceNo;
        $subscriberId = $data->subscriberId;
        $responseCode = $data->responseCode;
		if($transactionId && $IdinvoiceNo && $subscriberId && $responseCode){
        if($responseCode==01){
            $params = array('status' => 'Paid');
            $this->checkout_model->mtn_update($IdinvoiceNo, $params);
			return $this->output
				->set_content_type('application/json')
				->set_status_header(200)
				->set_output(json_encode(array('responseCode'=>'000', 'responseMsg'=>'Success')));
        }
    }else{
			return $this->output
				->set_content_type('application/json')
				->set_status_header(500)
				->set_output(json_encode(array('responseCode'=>'003', 'responseMsg'=>'Data fields empty')));
        	
        }
	}else{
			return $this->output
				->set_content_type('application/json')
				->set_status_header(500)
				->set_output(json_encode(array('responseCode'=>'006', 'responseMsg'=>'No data')));
        	
        }
}
	public function paysuccess(){
		$data['title']="Payment Success";
		$data['topsuccess']="Thank You For Your Payment";
		$data['bottomsuccess']="Please Check Your Account To Download Your Ticket";
		$this->load->view('header',$data);
		$this->load->view('paysuccess',$data);
		$this->load->view('footer');
	}


}
