<?php
class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','html'));
        $this->load->library('session');
        $this->load->database();
        $this->load->model('profile_model');
		$this->load->model('home_model');
    }

    function index(){
        $details = $this->profile_model->get_user_by_id($this->session->userdata('uid'));
        echo $data['uname'] = $details[0]->name . " " . $details[0]->email;
		$data['profile']=$details;
		$data['title']="Profile ".$details[0]->name;
        $data['uemail'] = $details[0]->phone;
		$this->load->view('header', $data);
        $this->load->view('profile', $data);
		$this->load->view('footer', $data);
    }
	function changepassword(){
		$data['title'] = "Change Password";
		$this->load->view('header',$data);
		$this->load->view('changepassword',$data);
		$this->load->view('footer');
	}
	function bookedticket(){
		$data['title'] = "Booked Ticket";
		$this->load->view('header',$data);
		$this->load->view('bookedticket',$data);
		$this->load->view('footer');
	}
	
	function editprofile(){
		$data['title'] = "Edit Profile";
		$this->load->view('header',$data);
		$this->load->view('editprofile',$data);
		$this->load->view('footer');
	}
}
?>