<?php 
class Checkout_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert($data)
	{
		$this->load->database();
		$this->db->insert('orderdetails', $data);
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}


	public function insertbook($databook)
	{
		$this->load->database();
		$this->db->insert('booking', $databook);
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}

	public function mtn_mm($params)
	{
		$this->load->database();
		$this->db->insert('mtn_mobile', $params);
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}

	public function mtn_entry($params)
	{
		$this->load->database();
		$this->db->insert('mtn_mobile', $params);
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}

	public function mtn_update($inv, $params)
	{
		$this->load->database();
		$this->db->where('invoice_no', $inv);
		$this->db->update('mtn_mobile', $params);
	}
	public function mtn_check($inv)
	{
		$this->load->database();
		$this->db->select('status');
		$this->db->from('mtn_mobile');
		$this->db->where('invoice_no', $inv);
		return $this->db->get()->result();
	}
}
?>
