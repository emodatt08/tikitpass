<?php
class User_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function userdetails($email)
    {
        $this->db->select('phone');
        $this->db->from('user');
        $this->db->where('email', $email);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

}