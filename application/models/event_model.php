<?php 
class Event_model extends CI_Model{
	function __construct() {
        parent::__construct();
		$this->load->database();
   }
   
// ================================================Hot Event Start Here=====================================   
public function show_hot_event()
	{   $this->db->select('*');
		$this->db->from('event');
		$this->db->where('status','1');
		$this->db->where('hot_status','1');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
// ================================================Hot Event Ends Here=====================================   
// ================================================Just Announced Event Start Here=====================================   
public function show_latest_event(){   
		$this->db->select('*');
		$this->db->from('event');
		$this->db->where('status','1');
		$this->db->order_by("event_id","desc");
		$this->db->limit(5);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
	
// ================================================Just Announced Event Start Here=====================================   
// ================================================For CMS section**********Start here=====================================   
 function show_cms()
	{   $this->db->select('*');
		$this->db->from('cms');
		$this->db->where('id',4);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===========================================For CMS section*******End here==========================================	
// ================================================For NEWS section**********Start here=====================================   
 function show_news()
	{   $this->db->select('*');
		$this->db->from('news');
		$this->db->where('news_status',1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===========================================For NEWS section*******End here==========================================		
// ================================================For CMS2 section**********Start here=====================================   
 function show_cms2()
	{   $this->db->select('*');
		$this->db->from('cms');
		$this->db->where('id',5);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===========================================For CMS 2 section*******End here==========================================

// ================================================For Gallery section**********Start here=====================================   
 function show_gallery()
	{   $this->db->select('*');
		$this->db->from('gallery');
		$this->db->where('gallery_status',1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===========================================For Gallery section*******End here==========================================

// ================================================For Gallery section**********Start here=====================================   
 public function get_menu() {
    $query = $this->db->get_where('category', array('parent_id' => 0));
    if ($query->num_rows() > 0):
        return $query;
    endif;
}

public function get_submenu() {
    $query = $this->db->get_where('category', array('parent_id' => 0));
    foreach ($query->result() as $row):
        $cat_id = $row->category_id;
    endforeach;
    if ($cat_id != FALSE):
        $this->db->from('category');
        $this->db->where('parent_id', $cat_id);
        $query = $this->db->get();
        return $query;
    else:
        return FALSE;
    endif;
}
//===========================================For Gallery section*******End here==========================================
	public function  show_my_event($eventid){
		
		$this->db->select ('event.*,booking.*'); 
		$this->db->from('event');
		$this->db->join('booking', 'booking.event_id = event.event_id');
		$this->db->where('event.event_id',$eventid);
		$this->db->where('booking.ticket_type',0);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	public function update_pay_status($id,$data){
		$this->db->where('booking_id', $id);
		$this->db->update('booking',$data);
	}
}
?>
