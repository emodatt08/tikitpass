<?php 
class Eventdetails_model extends CI_Model{
	function __construct() {
        parent::__construct();
		$this->load->database();
   }
   
// ================================================Hot Event Start Here=====================================   
public function show_hot_event()
	{   $this->db->select('*');
		$this->db->from('event');
		$this->db->where('status','1');
		$this->db->where('hot_status','1');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
// ================================================Hot Event Ends Here=====================================   
// ================================================Just Announced Event Start Here=====================================   
public function show_latest_event(){   
		$this->db->select('*');
		$this->db->from('event');
		$this->db->where('status','1');
		$this->db->order_by("event_id","desc");
		$this->db->limit(5);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
public function show_eventdetails($eventid){   
		$this->db->select ('event.*,ticket.*'); 
		$this->db->from('event');
		$this->db->join('ticket', 'ticket.event_id = event.event_id');
		$this->db->where('event.event_id',$eventid);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//=======================For Paidticket==========================	
public function show_ticketcount($eventid){
		$this->db->select('*');
		$this->db->from('ticket');
		$this->db->where('event_id',$eventid);
		$this->db->where('ticket_type',1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;

	}
public function show_ticketbookedcount($eventid){
		$this->db->select('*');
		$this->db->from('booking');
		$this->db->where('event_id',$eventid);
		$this->db->where('pay_status','1');
		$this->db->where('ticket_gen_status','1');
		$this->db->where('ticket_type',1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;

	}
//=======================For Paidticket==========================	
//=======================For Freeticket==========================	
public function show_ticketcountfree($eventid){
		$this->db->select('*');
		$this->db->from('ticket');
		$this->db->where('event_id',$eventid);
		$this->db->where('ticket_type',0);
		$query = $this->db->get();
		$result = $query->result();
		return $result;

	}
public function show_ticketbookedcountfree($eventid){
		$this->db->select('*');
		$this->db->from('booking');
		$this->db->where('event_id',$eventid);
		$this->db->where('ticket_gen_status','1');
		$this->db->where('ticket_type',0);
		$query = $this->db->get();
		$result = $query->result();
		return $result;

	}
//=======================For Freeticket==========================	
public function show_ticketdet($eventid){   
		$this->db->select('*');
		$this->db->from('ticket');
		$this->db->where('event_id',$eventid);
		$this->db->where('ticket_type',1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

public function show_ticketdetails($eventid){   
		$this->db->select('*');
		$this->db->from('ticket');
		$this->db->where('event_id',$eventid);
		$this->db->where('ticket_type',0);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
// ================================================Just Announced Event Start Here=====================================   
// ================================================For CMS section**********Start here=====================================   
 function show_cms()
	{   $this->db->select('*');
		$this->db->from('cms');
		$this->db->where('id',4);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===========================================For CMS section*******End here==========================================	
// ================================================For NEWS section**********Start here=====================================   
 function show_news()
	{   $this->db->select('*');
		$this->db->from('news');
		$this->db->where('news_status',1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===========================================For NEWS section*******End here==========================================		
// ================================================For CMS2 section**********Start here=====================================   
 function show_cms2()
	{   $this->db->select('*');
		$this->db->from('cms');
		$this->db->where('id',5);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===========================================For CMS 2 section*******End here==========================================

// ================================================For Gallery section**********Start here=====================================   
 function show_gallery()
	{   $this->db->select('*');
		$this->db->from('gallery');
		$this->db->where('gallery_status',1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===========================================For Gallery section*******End here==========================================

// ================================================For Gallery section**********Start here=====================================   
 public function get_menu() {
    $query = $this->db->get_where('category', array('parent_id' => 0));
    if ($query->num_rows() > 0):
        return $query;
    endif;
}

public function get_submenu() {
    $query = $this->db->get_where('category', array('parent_id' => 0));
    foreach ($query->result() as $row):
        $cat_id = $row->category_id;
    endforeach;
    if ($cat_id != FALSE):
        $this->db->from('category');
        $this->db->where('parent_id', $cat_id);
        $query = $this->db->get();
        return $query;
    else:
        return FALSE;
    endif;
}
//===========================================For Gallery section*******End here==========================================
}
?>
