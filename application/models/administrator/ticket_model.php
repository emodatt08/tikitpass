<?php 
 class Ticket_model extends CI_Model{
	//================Insert Category================ 
	public function insert_category($data) {
		$this->load->database();
	    $this->db->insert('event_category', $data); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}
	//================Insert Category================
	
	//================Show Member================
	function show_category(){
		$sql ="select * from event_category";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
	//================Show Member================
	
	//================Show Member By Id==============
	function member_view($id){
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('member_id', $id);
		$query = $this->db->get();
		return($query->num_rows() > 0) ? $query->result(): NULL;
		
	}
	//================Show Member By Id==============
	//================Num rows one===================
	function check_paidfree($id){
		$this->db->select('*');
		$this->db->from('ticket');
		$this->db->where('event_id', $id);
		$this->db->where('ticket_type',1);
		$query = $this->db->get();
		return $query->num_rows();
	}
	//================Num Rows One===================
	
	//================Num rows one===================
	function check_paid($id){
		$this->db->select('*');
		$this->db->from('ticket');
		$this->db->where('event_id', $id);
		$this->db->where('ticket_type',0);
		$query1 = $this->db->get();
		return $query1->num_rows();
	}
	//================Num Rows One===================
	
	//================Delete Member==============
	function delete_category($id){
	  $this->db->where('category_id',$id);
	  $this->db->delete('event_category'); 
	}
	//================Delete member==============
	//=============Show Member By Id=============
	function show_ticket_id($id){
		$this->db->select('*');
		$this->db->from('ticket');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	//=============Show Member By Id=============
	//=============Member Edit===================
	function edit_category($id, $datalist){
		$this->db->where('category_id', $id);
		$this->db->update('event_category',$datalist);
	}
	//=============Member Edit===================
	
	//***************============Event Type Section============****************
	
	//===============Insert=====================
	public function insert_type($data) {
		$this->load->database();
	    $this->db->insert('event_type', $data); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}
	//================Insert Type================
	//================Show Type==================
	function show_type(){
		$sql ="select * from event_type";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
	//================Show Type==================
	//================Delete Member==============
	function delete_type($id){
	  $this->db->where('type_id',$id);
	  $this->db->delete('event_type'); 
	}
	//================Delete member==============
	//=============Show Member By Id=============
	function show_type_id($id){
		$this->db->select('*');
		$this->db->from('event_type');
		$this->db->where('type_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	//=============Show Member By Id=============
	//*********===============Event Section===============********//
	
	public function insert_ticket($data) {
		$this->load->database();
	    $this->db->insert('ticket', $data); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}
	//================Insert Type================
	//================Show Type==================
	function show_ticket($id){
		$this->db->select('*');
		$this->db->from('ticket');
		$this->db->where('event_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	//================Show Type==================
	//================Check Paid Free============
	function check_paidfree1($id){
		$this->db->select('*');
		$this->db->from('ticket');
		$this->db->where('event_id', $id);
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}
	//=============== Check Paid Free============ 
	//================Delete Member==============
	function delete_ticket($id){
	  $this->db->where('id',$id);
	  $this->db->delete('ticket'); 
	}
	//================Delete member==============
	//=============Show Member By Id=============
	function show_event_id($id){
		$this->db->select('*');
		$this->db->from('event');
		$this->db->where('event_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
	//*********===============Event Section===============********//
	//=============Member Edit===================
	function ticket_edit($id, $datalist){
		$this->db->where('id', $id);
		$this->db->update('ticket',$datalist);
	}
	//=============Member Edit===================
	//***************============Event Type Section============****************
}



?>