<?php 
class User_model extends CI_Model{
	function __construct() {
        parent::__construct();
   }
public function insert_user($data) {
		$this->load->database();
	    $this->db->insert('user', $data); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}
function show_user()
	{
		$sql ="select * from user ";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
function show_user_id($id){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('uid', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
function user_view($id){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('uid', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
function user_edit($id, $data){
	
		$this->db->where('uid', $id);
		$this->db->update('user',$data);
	}
function updt($stat,$id){
	 
		$sql ="update user set userstatus=$stat where uid=$id ";
		$query = $this->db->query($sql);
		//return($query->num_rows() > 0) ? $query->result(): NULL;
	}
function show_userlist()
	{
		$sql ="select * from user";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
function delete_user($id,$user_image){
		$this->db->where('uid', $id);
		unlink("uploads/profileimage/".$user_image);
		$this->db->delete('user');	
		}
function delete_mul($ids)//Delete Multiple user
		{
			$ids = $ids;
			$count = 0;
			foreach ($ids as $id){
			$did = intval($id).'<br>';
			$this->db->where('uid', $did);
			unlink("uploads/profileimage/".$user_image);
			$this->db->delete('user');  
			$count = $count+1;
			}
			
			echo'<div class="alert alert-success" style="margin-top:-17px;font-weight:bold">
			'.$count.' Item deleted successfully
			</div>';
			$count = 0;		
		}
	}
?>