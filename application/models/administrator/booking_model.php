<?php 
class Booking_model extends CI_Model{
	function __construct() {
        parent::__construct();
   }
public function insert_booking($data) {
		$this->load->database();
	    $this->db->insert('booking', $data); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}
function show_booking()
	{
		$sql ="select * from booking ";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
	
function show_booking_stat()
	{
		$sql ="select * from booking_status ";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}


function show_booking_id($id){
		$this->db->select('*');
		$this->db->from('booking');
		$this->db->where('booking_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
function show_booking_join(){	
$this->db->select('*');
            $this->db->from('booking b'); 
            $this->db->join('user u', 'u.uid=b.uid', 'left');
            $this->db->join('event e', 'e.event_id=b.event_id', 'left');
            //$this->db->where('b.booking_id',$id);
            $this->db->order_by('b.booking_id','asc');         
            $query = $this->db->get(); 
			$result = $query->result();
			return $result;
	}
function booking_view($id){
		$this->db->select('*');
		$this->db->from('booking');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
function booking_edit($id, $data){
	
		$this->db->where('booking_id', $id);
		$this->db->update('booking',$data);
	}
function updt($stat,$id){
	 
		$sql ="update booking set booking_status=$stat where booking_id=$id ";
		$query = $this->db->query($sql);
		//return($query->num_rows() > 0) ? $query->result(): NULL;
	}
function show_bookinglist()
	{
		$sql ="select * from booking";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
function delete_booking($id){
		$this->db->where('booking_id', $id);
		unlink("booking/".$booking_image);
		$this->db->delete('booking');	
		}
function delete_mul($ids)//Delete Multiple Booking
		{
			$ids = $ids;
			$count = 0;
			foreach ($ids as $id){
			$did = intval($id).'<br>';
			$this->db->where('booking_id', $did);
			unlink("booking/".$booking_image);
			$this->db->delete('booking');  
			$count = $count+1;
			}
			
			echo'<div class="alert alert-success" style="margin-top:-17px;font-weight:bold">
			'.$count.' Item deleted successfully
			</div>';
			$count = 0;		
		}
	}
?>