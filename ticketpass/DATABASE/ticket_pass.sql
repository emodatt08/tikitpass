-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2016 at 12:57 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticket_pass`
--

-- --------------------------------------------------------

--
-- Table structure for table `ad`
--

CREATE TABLE `ad` (
  `id` int(11) NOT NULL,
  `ad_heading` varchar(255) NOT NULL,
  `ad_img` varchar(255) NOT NULL,
  `ad_link` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ad`
--

INSERT INTO `ad` (`id`, `ad_heading`, `ad_img`, `ad_link`, `status`) VALUES
(3, 'Advertisement I', 'ad1.jpg', 'www.google.co.in', '1'),
(4, 'Advertisement II', 'ad11.jpg', '', '1'),
(5, 'Advertisement III', 'ad12.jpg', '', '1'),
(6, 'Test', 'Desert.jpg', '', '1'),
(7, 'Test', 'Desert1.jpg', 'www.google.co.in', '1');

-- --------------------------------------------------------

--
-- Table structure for table `administrator_detail`
--

CREATE TABLE `administrator_detail` (
  `AdminId` int(11) NOT NULL,
  `UserName` varchar(255) NOT NULL,
  `UserPassword` varchar(255) NOT NULL,
  `log_type` char(50) NOT NULL,
  `UserStatus` enum('Active','InActive') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrator_detail`
--

INSERT INTO `administrator_detail` (`AdminId`, `UserName`, `UserPassword`, `log_type`, `UserStatus`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'ad', 'Active'),
(2, 'servicesagent', '202cb962ac59075b964b07152d234b70', 'csa', 'Active'),
(3, 'servicemanager', '202cb962ac59075b964b07152d234b70', 'csm', 'Active'),
(4, 'systemadmin', '202cb962ac59075b964b07152d234b70', 'sa', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `admin_mail`
--

CREATE TABLE `admin_mail` (
  `MailId` int(11) NOT NULL,
  `MailAddress` varchar(255) NOT NULL,
  `UserImage` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_mail`
--

INSERT INTO `admin_mail` (`MailId`, `MailAddress`, `UserImage`) VALUES
(1, 'info@tikitpass.com', 'Tulips.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `banner_sub_heading` varchar(255) NOT NULL,
  `banner_img` varchar(255) NOT NULL,
  `banner_description` blob NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `banner_heading`, `banner_sub_heading`, `banner_img`, `banner_description`, `status`) VALUES
(6, 'Busticket Banner', 'Busticket Banner', 'bus_ticket.jpg', 0x3c703e4c6f72656d20497073756d2069732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e3c2f703e, '1'),
(7, 'Event Booking', 'Event Booking', 'event.jpg', 0x3c703e4c6f72656d20497073756d2069732073696d706c792064756d6d7920746578743c2f703e, '1'),
(8, 'Flight Booking', 'Flight Booking', 'flight.jpg', 0x3c703e4c6f72656d20497073756d2069732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720696e6475737472792e3c2f703e, '1'),
(9, '2016 Presidential Debate - IEA Ghana ', '1st Presidential Debate, Tamale 08-11-2016', 'ieadebate1.jpg', 0x3c703e54686520496e73746974757465206f662045636f6e6f6d69632041666661697273202849454129266e6273703b736179732074686973207965617226727371756f3b7320707265736964656e7469616c20646562617465732077696c6c20636f6d65206f666620696e204e6f76656d62657220382c20323031362e3c2f703e, '1');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `blog_image` varchar(255) NOT NULL,
  `blog_title` text NOT NULL,
  `posted_by` varchar(255) NOT NULL,
  `blog_desc` text NOT NULL,
  `blog_status` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `blog_image`, `blog_title`, `posted_by`, `blog_desc`, `blog_status`, `date`) VALUES
(1, 'images345.jpg', 'Raj', 'Ramen', 'Hi.................................hello', '1', '2016-09-09'),
(2, 'images55.jpg', 'ABC xyz', 'Admin', 'HELLO.............................................', '1', '2016-09-09'),
(3, 'imp.png', 'New blog test', 'New blog', 'New blog', '1', '2016-09-14');

-- --------------------------------------------------------

--
-- Table structure for table `bookedticket`
--

CREATE TABLE `bookedticket` (
  `bookticketid` int(11) NOT NULL,
  `evnt_id` int(11) NOT NULL,
  `tckt_id` varchar(255) NOT NULL,
  `ordr_no` varchar(255) NOT NULL,
  `tckt_file` varchar(255) NOT NULL,
  `add_dt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookedticket`
--

INSERT INTO `bookedticket` (`bookticketid`, `evnt_id`, `tckt_id`, `ordr_no`, `tckt_file`, `add_dt`) VALUES
(1, 9, 'Ks7IvnBSvOcx3Ekp9', '1nYdjomprKs7IvnBSvOcx3Ekp9', '1nYdjomprKs7IvnBSvOcx3Ekp9.pdf', '2016-11-22');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `booking_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `orderno` varchar(255) NOT NULL,
  `tkt_id` varchar(255) NOT NULL,
  `event_id` int(11) NOT NULL,
  `date_of_booking` datetime NOT NULL,
  `amount` varchar(255) NOT NULL,
  `ticket_type` int(11) NOT NULL,
  `pay_status` int(11) NOT NULL,
  `ticket_gen_status` tinyint(4) NOT NULL,
  `booking_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`booking_id`, `uid`, `orderno`, `tkt_id`, `event_id`, `date_of_booking`, `amount`, `ticket_type`, `pay_status`, `ticket_gen_status`, `booking_status`) VALUES
(1, 1, '1nYdjomprKs7IvnBSvOcx3Ekp9', 'Ks7IvnBSvOcx3Ekp9', 9, '2016-11-22 12:18:05', '0', 0, 2, 1, 1),
(2, 10, '10SubfkedVKs7IvnBSvOcx3Ekp9', 'Ks7IvnBSvOcx3Ekp9', 9, '2016-11-22 12:45:05', '1368', 1, 2, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking_status`
--

CREATE TABLE `booking_status` (
  `id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_status`
--

INSERT INTO `booking_status` (`id`, `status`) VALUES
(1, 'paid'),
(2, 'unpaid'),
(3, 'cancelled');

-- --------------------------------------------------------

--
-- Table structure for table `ci_cookies`
--

CREATE TABLE `ci_cookies` (
  `id` int(11) NOT NULL,
  `cookie_id` varchar(255) DEFAULT NULL,
  `netid` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `orig_page_requested` varchar(120) DEFAULT NULL,
  `php_session_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('c24596863ea0a315fc469ff9476df32f', '182.74.1.54', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', 1479116627, 'a:1:{s:9:"logged_in";a:3:{s:5:"email";s:11:"arrow@a.com";s:3:"uid";s:1:"8";s:16:"is_userlogged_in";i:1;}}'),
('ad1e896ae10201abe86524b1c6a72154', '182.74.1.54', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36', 1479116859, 'a:1:{s:9:"logged_in";a:3:{s:5:"email";s:13:"abc@gmail.com";s:3:"uid";s:1:"9";s:16:"is_userlogged_in";i:1;}}');

-- --------------------------------------------------------

--
-- Table structure for table `cms`
--

CREATE TABLE `cms` (
  `id` int(11) NOT NULL,
  `cms_pagetitle` varchar(255) NOT NULL,
  `cms_heading` varchar(255) NOT NULL,
  `cms_sub_heading` varchar(255) NOT NULL,
  `cms_img` varchar(255) NOT NULL,
  `description` blob NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms`
--

INSERT INTO `cms` (`id`, `cms_pagetitle`, `cms_heading`, `cms_sub_heading`, `cms_img`, `description`, `status`) VALUES
(1, 'Home', 'Home', 'Home', 'demo.jpg', '', ''),
(2, 'About Us', 'Tikitpass Mobile and Web Ticketing Services', 'The most Secure, Efficient and Convenient way to buy Tickets', 'aboutus.jpg', 0x3c703e3c7374726f6e673e546865207465616d2061742054696b69747061737320636f6d62696e6573207374617465206f66207468652061727420746563686e6f6c6f677920616e6420666972737420636c61737320637573746f6d6572207365727669636520746f2070726f7669646520796f75206169726c696e65732c206576656e74206f7267616e69736572732c2070617373656e6765727320616e64206576656e7420617474656e64656573207769746820746865206d6f7374207365637572652c20656666696369656e7420616e6420636f6e76656e69656e742077617920746f2073656c6c20616e6420627579207469636b65747320666f72207472616e73706f727420616e64206576656e74732e20266e6273703b3c2f7374726f6e673e3c2f703e0d0a3c703e3c7374726f6e673e54696b6974706173733c2f7374726f6e673e3c7374726f6e673e266e6273703b3c2f7374726f6e673e3c7374726f6e673e70726f76696465732061206f6e652073746f702073686f7020666f722075702d746f2d6461746520696e666f726d6174696f6e2061626f75742074726176656c7320616e64206576656e747320746f2077686963682077652073656c6c207469636b6574732e3c2f7374726f6e673e3c2f703e0d0a3c703e3c7374726f6e673e57652070726f766964652070617373656e6765727320616e64206576656e7420617474656e6465657320776974682075702d746f2d646174652072656c6576616e7420696e666f726d6174696f6e206f66206576656e747320616e64207472616e73706f72746174696f6e20657370656369616c6c7920696e2072656c6174696f6e20746f20746865697220707572636861736564207469636b6574732e266e6273703b3c2f7374726f6e673e3c2f703e, ''),
(3, 'Contact Us', 'Contact Us', 'Contact Us', 'Penguins.jpg', 0x3c703e49742069732061206c6f6e672065737461626c6973686564206661637420746861742061207265616465722077696c6c206265206469737472616374656420627920746865207265616461626c6520636f6e74656e74206f6620612070616765207768656e206c6f6f6b696e6720617420697473206c61796f75742e2054686520706f696e74206f66207573696e67204c6f72656d20497073756d2069732074686174206974206861732061206d6f72652d6f722d6c657373206e6f726d616c20646973747269627574696f6e206f66206c6574746572732c206173206f70706f73656420746f207573696e672027436f6e74656e7420686572652c20636f6e74656e742068657265272c206d616b696e67206974206c6f6f6b206c696b65207265616461626c6520456e676c6973682e204d616e79206465736b746f70207075626c697368696e67207061636b6167657320616e6420776562207061676520656469746f7273206e6f7720757365204c6f72656d20497073756d2061732074686569722064656661756c74206d6f64656c20746578742c20616e6420612073656172636820666f7220276c6f72656d20697073756d272077696c6c20756e636f766572206d616e7920776562207369746573207374696c6c20696e20746865697220696e66616e63792e20566172696f75732076657273696f6e7320686176652065766f6c766564206f766572207468652079656172732c20736f6d6574696d6573206279206163636964656e742c20736f6d6574696d6573206f6e20707572706f73652028696e6a65637465642068756d6f757220616e6420746865206c696b65292e3c2f703e, ''),
(4, 'Help', 'Help', 'Help', 'help.jpg', 0x3c703e4c6f72656d20497073756d266e6273703b69732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e3c2f703e0d0a3c703e4c6f72656d20497073756d266e6273703b69732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e3c2f703e, ''),
(5, 'Careers', 'Careers', 'Careers', 'workwithus1.jpg', 0x3c703e4c6f72656d20497073756d266e6273703b69732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e3c2f703e0d0a3c703e4c6f72656d20497073756d266e6273703b69732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e3c2f703e, ''),
(6, 'What We Do', 'What We Do', 'What We Do', 'whatwedo.jpg', 0x3c703e4c6f72656d20497073756d266e6273703b69732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e3c2f703e0d0a3c703e3c7370616e207374796c653d22666f6e742d66616d696c793a2027636f6d69632073616e73206d73272c2073616e732d73657269663b20666f6e742d73697a653a20313270743b20636f6c6f723a20236666393930303b223e3c7374726f6e673e4c6f72656d20497073756d266e6273703b69732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e3c2f7374726f6e673e3c2f7370616e3e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e3c2f703e, '');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`, `status`) VALUES
(1, 'Aruba', 1),
(2, 'Afghanistan', 1),
(3, 'Angola', 1),
(4, 'Anguilla', 1),
(5, 'Albania', 1),
(6, 'Andorra', 1),
(7, 'Netherlands Antilles', 1),
(8, 'United Arab Emirates', 1),
(9, 'Argentina', 1),
(10, 'Armenia', 1),
(11, 'American Samoa', 1),
(12, 'Antarctica', 1),
(13, 'French Southern territories', 1),
(14, 'Antigua and Barbuda', 1),
(15, 'Australia', 1),
(16, 'Austria', 1),
(17, 'Azerbaijan', 1),
(18, 'Burundi', 1),
(19, 'Belgium', 1),
(20, 'Benin', 1),
(21, 'Burkina Faso', 1),
(22, 'Bangladesh', 1),
(23, 'Bulgaria', 1),
(24, 'Bahrain', 1),
(25, 'Bahamas', 1),
(26, 'Bosnia and Herzegovina', 1),
(27, 'Belarus', 1),
(28, 'Belize', 1),
(29, 'Bermuda', 1),
(30, 'Bolivia', 1),
(31, 'Brazil', 1),
(32, 'Barbados', 1),
(33, 'Brunei', 1),
(34, 'Bhutan', 1),
(35, 'Bouvet Island', 1),
(36, 'Botswana', 1),
(37, 'Central African Republic', 1),
(38, 'Canada', 1),
(39, 'Cocos (Keeling) Islands', 1),
(40, 'Switzerland', 1),
(41, 'Chile', 1),
(42, 'China', 1),
(43, 'CÃ´te dâ€™Ivoire', 1),
(44, 'Cameroon', 1),
(45, 'Congo, The Democratic Republic', 1),
(46, 'Congo', 1),
(47, 'Cook Islands', 1),
(48, 'Colombia', 1),
(49, 'Comoros', 1),
(50, 'Cape Verde', 1),
(51, 'Costa Rica', 1),
(52, 'Cuba', 1),
(53, 'Christmas Island', 1),
(54, 'Cayman Islands', 1),
(55, 'Cyprus', 1),
(56, 'Czech Republic', 1),
(57, 'Germany', 1),
(58, 'Djibouti', 1),
(59, 'Dominica', 1),
(60, 'Denmark', 1),
(61, 'Dominican Republic', 1),
(62, 'Algeria', 1),
(63, 'Ecuador', 1),
(64, 'Egypt', 1),
(65, 'Eritrea', 1),
(66, 'Western Sahara', 1),
(67, 'Spain', 1),
(68, 'Estonia', 1),
(69, 'Ethiopia', 1),
(70, 'Finland', 1),
(71, 'Fiji Islands', 1),
(72, 'Falkland Islands', 1),
(73, 'France', 1),
(74, 'Faroe Islands', 1),
(75, 'Micronesia, Federated States o', 1),
(76, 'Gabon', 1),
(77, 'United Kingdom', 1),
(78, 'Georgia', 1),
(79, 'Ghana', 1),
(80, 'Gibraltar', 1),
(81, 'Guinea', 1),
(82, 'Guadeloupe', 1),
(83, 'Gambia', 1),
(84, 'Guinea-Bissau', 1),
(85, 'Equatorial Guinea', 1),
(86, 'Greece', 1),
(87, 'Grenada', 1),
(88, 'Greenland', 1),
(89, 'Guatemala', 1),
(90, 'French Guiana', 1),
(91, 'Guam', 1),
(92, 'Guyana', 1),
(93, 'Hong Kong', 1),
(94, 'Heard Island and McDonald Isla', 1),
(95, 'Honduras', 1),
(96, 'Croatia', 1),
(97, 'Haiti', 1),
(98, 'Hungary', 1),
(99, 'Indonesia', 1),
(100, 'India', 1),
(101, 'British Indian Ocean Territory', 1),
(102, 'Ireland', 1),
(103, 'Iran', 1),
(104, 'Iraq', 1),
(105, 'Iceland', 1),
(106, 'Israel', 1),
(107, 'Italy', 1),
(108, 'Jamaica', 1),
(109, 'Jordan', 1),
(110, 'Japan', 1),
(111, 'Kazakstan', 1),
(112, 'Kenya', 1),
(113, 'Kyrgyzstan', 1),
(114, 'Cambodia', 1),
(115, 'Kiribati', 1),
(116, 'Saint Kitts and Nevis', 1),
(117, 'South Korea', 1),
(118, 'Kuwait', 1),
(119, 'Laos', 1),
(120, 'Lebanon', 1),
(121, 'Liberia', 1),
(122, 'Libyan Arab Jamahiriya', 1),
(123, 'Saint Lucia', 1),
(124, 'Liechtenstein', 1),
(125, 'Sri Lanka', 1),
(126, 'Lesotho', 1),
(127, 'Lithuania', 1),
(128, 'Luxembourg', 1),
(129, 'Latvia', 1),
(130, 'Macao', 1),
(131, 'Morocco', 1),
(132, 'Monaco', 1),
(133, 'Moldova', 1),
(134, 'Madagascar', 1),
(135, 'Maldives', 1),
(136, 'Mexico', 1),
(137, 'Marshall Islands', 1),
(138, 'Macedonia', 1),
(139, 'Mali', 1),
(140, 'Malta', 1),
(141, 'Myanmar', 1),
(142, 'Mongolia', 1),
(143, 'Northern Mariana Islands', 1),
(144, 'Mozambique', 1),
(145, 'Mauritania', 1),
(146, 'Montserrat', 1),
(147, 'Martinique', 1),
(148, 'Mauritius', 1),
(149, 'Malawi', 1),
(150, 'Malaysia', 1),
(151, 'Mayotte', 1),
(152, 'Namibia', 1),
(153, 'New Caledonia', 1),
(154, 'Niger', 1),
(155, 'Norfolk Island', 1),
(156, 'Nigeria', 1),
(157, 'Nicaragua', 1),
(158, 'Niue', 1),
(159, 'Netherlands', 1),
(160, 'Norway', 1),
(161, 'Nepal', 1),
(162, 'Nauru', 1),
(163, 'New Zealand', 1),
(164, 'Oman', 1),
(165, 'Pakistan', 1),
(166, 'Panama', 1),
(167, 'Pitcairn', 1),
(168, 'Peru', 1),
(169, 'Philippines', 1),
(170, 'Palau', 1),
(171, 'Papua New Guinea', 1),
(172, 'Poland', 1),
(173, 'Puerto Rico', 1),
(174, 'North Korea', 1),
(175, 'Portugal', 1),
(176, 'Paraguay', 1),
(177, 'Palestine', 1),
(178, 'French Polynesia', 1),
(179, 'Qatar', 1),
(180, 'RÃ©union', 1),
(181, 'Romania', 1),
(182, 'Russian Federation', 1),
(183, 'Rwanda', 1),
(184, 'Saudi Arabia', 1),
(185, 'Sudan', 1),
(186, 'Senegal', 1),
(187, 'Singapore', 1),
(188, 'South Georgia and the South Sa', 1),
(189, 'Saint Helena', 1),
(190, 'Svalbard and Jan Mayen', 1),
(191, 'Solomon Islands', 1),
(192, 'Sierra Leone', 1),
(193, 'El Salvador', 1),
(194, 'San Marino', 1),
(195, 'Somalia', 1),
(196, 'Saint Pierre and Miquelon', 1),
(197, 'Sao Tome and Principe', 1),
(198, 'Suriname', 1),
(199, 'Slovakia', 1),
(200, 'Slovenia', 1),
(201, 'Sweden', 1),
(202, 'Swaziland', 1),
(203, 'Seychelles', 1),
(204, 'Syria', 1),
(205, 'Turks and Caicos Islands', 1),
(206, 'Chad', 1),
(207, 'Togo', 1),
(208, 'Thailand', 1),
(209, 'Tajikistan', 1),
(210, 'Tokelau', 1),
(211, 'Turkmenistan', 1),
(212, 'East Timor', 1),
(213, 'Tonga', 1),
(214, 'Trinidad and Tobago', 1),
(215, 'Tunisia', 1),
(216, 'Turkey', 1),
(217, 'Tuvalu', 1),
(218, 'Taiwan', 1),
(219, 'Tanzania', 1),
(220, 'Uganda', 1),
(221, 'Ukraine', 1),
(222, 'United States Minor Outlying I', 1),
(223, 'Uruguay', 1),
(224, 'United States', 1),
(225, 'Uzbekistan', 1),
(226, 'Holy See (Vatican City State)', 1),
(227, 'Saint Vincent and the Grenadin', 1),
(228, 'Venezuela', 1),
(229, 'Virgin Islands, British', 1),
(230, 'Virgin Islands, U.S.', 1),
(231, 'Vietnam', 1),
(232, 'Vanuatu', 1),
(233, 'Wallis and Futuna', 1),
(234, 'Samoa', 1),
(235, 'Yemen', 1),
(236, 'Yugoslavia', 1),
(237, 'South Africa', 1),
(238, 'Zambia', 1),
(239, 'Zimbabwe', 1),
(1, 'Aruba', 1),
(2, 'Afghanistan', 1),
(3, 'Angola', 1),
(4, 'Anguilla', 1),
(5, 'Albania', 1),
(6, 'Andorra', 1),
(7, 'Netherlands Antilles', 1),
(8, 'United Arab Emirates', 1),
(9, 'Argentina', 1),
(10, 'Armenia', 1),
(11, 'American Samoa', 1),
(12, 'Antarctica', 1),
(13, 'French Southern territories', 1),
(14, 'Antigua and Barbuda', 1),
(15, 'Australia', 1),
(16, 'Austria', 1),
(17, 'Azerbaijan', 1),
(18, 'Burundi', 1),
(19, 'Belgium', 1),
(20, 'Benin', 1),
(21, 'Burkina Faso', 1),
(22, 'Bangladesh', 1),
(23, 'Bulgaria', 1),
(24, 'Bahrain', 1),
(25, 'Bahamas', 1),
(26, 'Bosnia and Herzegovina', 1),
(27, 'Belarus', 1),
(28, 'Belize', 1),
(29, 'Bermuda', 1),
(30, 'Bolivia', 1),
(31, 'Brazil', 1),
(32, 'Barbados', 1),
(33, 'Brunei', 1),
(34, 'Bhutan', 1),
(35, 'Bouvet Island', 1),
(36, 'Botswana', 1),
(37, 'Central African Republic', 1),
(38, 'Canada', 1),
(39, 'Cocos (Keeling) Islands', 1),
(40, 'Switzerland', 1),
(41, 'Chile', 1),
(42, 'China', 1),
(43, 'CÃ´te dâ€™Ivoire', 1),
(44, 'Cameroon', 1),
(45, 'Congo, The Democratic Republic', 1),
(46, 'Congo', 1),
(47, 'Cook Islands', 1),
(48, 'Colombia', 1),
(49, 'Comoros', 1),
(50, 'Cape Verde', 1),
(51, 'Costa Rica', 1),
(52, 'Cuba', 1),
(53, 'Christmas Island', 1),
(54, 'Cayman Islands', 1),
(55, 'Cyprus', 1),
(56, 'Czech Republic', 1),
(57, 'Germany', 1),
(58, 'Djibouti', 1),
(59, 'Dominica', 1),
(60, 'Denmark', 1),
(61, 'Dominican Republic', 1),
(62, 'Algeria', 1),
(63, 'Ecuador', 1),
(64, 'Egypt', 1),
(65, 'Eritrea', 1),
(66, 'Western Sahara', 1),
(67, 'Spain', 1),
(68, 'Estonia', 1),
(69, 'Ethiopia', 1),
(70, 'Finland', 1),
(71, 'Fiji Islands', 1),
(72, 'Falkland Islands', 1),
(73, 'France', 1),
(74, 'Faroe Islands', 1),
(75, 'Micronesia, Federated States o', 1),
(76, 'Gabon', 1),
(77, 'United Kingdom', 1),
(78, 'Georgia', 1),
(79, 'Ghana', 1),
(80, 'Gibraltar', 1),
(81, 'Guinea', 1),
(82, 'Guadeloupe', 1),
(83, 'Gambia', 1),
(84, 'Guinea-Bissau', 1),
(85, 'Equatorial Guinea', 1),
(86, 'Greece', 1),
(87, 'Grenada', 1),
(88, 'Greenland', 1),
(89, 'Guatemala', 1),
(90, 'French Guiana', 1),
(91, 'Guam', 1),
(92, 'Guyana', 1),
(93, 'Hong Kong', 1),
(94, 'Heard Island and McDonald Isla', 1),
(95, 'Honduras', 1),
(96, 'Croatia', 1),
(97, 'Haiti', 1),
(98, 'Hungary', 1),
(99, 'Indonesia', 1),
(100, 'India', 1),
(101, 'British Indian Ocean Territory', 1),
(102, 'Ireland', 1),
(103, 'Iran', 1),
(104, 'Iraq', 1),
(105, 'Iceland', 1),
(106, 'Israel', 1),
(107, 'Italy', 1),
(108, 'Jamaica', 1),
(109, 'Jordan', 1),
(110, 'Japan', 1),
(111, 'Kazakstan', 1),
(112, 'Kenya', 1),
(113, 'Kyrgyzstan', 1),
(114, 'Cambodia', 1),
(115, 'Kiribati', 1),
(116, 'Saint Kitts and Nevis', 1),
(117, 'South Korea', 1),
(118, 'Kuwait', 1),
(119, 'Laos', 1),
(120, 'Lebanon', 1),
(121, 'Liberia', 1),
(122, 'Libyan Arab Jamahiriya', 1),
(123, 'Saint Lucia', 1),
(124, 'Liechtenstein', 1),
(125, 'Sri Lanka', 1),
(126, 'Lesotho', 1),
(127, 'Lithuania', 1),
(128, 'Luxembourg', 1),
(129, 'Latvia', 1),
(130, 'Macao', 1),
(131, 'Morocco', 1),
(132, 'Monaco', 1),
(133, 'Moldova', 1),
(134, 'Madagascar', 1),
(135, 'Maldives', 1),
(136, 'Mexico', 1),
(137, 'Marshall Islands', 1),
(138, 'Macedonia', 1),
(139, 'Mali', 1),
(140, 'Malta', 1),
(141, 'Myanmar', 1),
(142, 'Mongolia', 1),
(143, 'Northern Mariana Islands', 1),
(144, 'Mozambique', 1),
(145, 'Mauritania', 1),
(146, 'Montserrat', 1),
(147, 'Martinique', 1),
(148, 'Mauritius', 1),
(149, 'Malawi', 1),
(150, 'Malaysia', 1),
(151, 'Mayotte', 1),
(152, 'Namibia', 1),
(153, 'New Caledonia', 1),
(154, 'Niger', 1),
(155, 'Norfolk Island', 1),
(156, 'Nigeria', 1),
(157, 'Nicaragua', 1),
(158, 'Niue', 1),
(159, 'Netherlands', 1),
(160, 'Norway', 1),
(161, 'Nepal', 1),
(162, 'Nauru', 1),
(163, 'New Zealand', 1),
(164, 'Oman', 1),
(165, 'Pakistan', 1),
(166, 'Panama', 1),
(167, 'Pitcairn', 1),
(168, 'Peru', 1),
(169, 'Philippines', 1),
(170, 'Palau', 1),
(171, 'Papua New Guinea', 1),
(172, 'Poland', 1),
(173, 'Puerto Rico', 1),
(174, 'North Korea', 1),
(175, 'Portugal', 1),
(176, 'Paraguay', 1),
(177, 'Palestine', 1),
(178, 'French Polynesia', 1),
(179, 'Qatar', 1),
(180, 'RÃ©union', 1),
(181, 'Romania', 1),
(182, 'Russian Federation', 1),
(183, 'Rwanda', 1),
(184, 'Saudi Arabia', 1),
(185, 'Sudan', 1),
(186, 'Senegal', 1),
(187, 'Singapore', 1),
(188, 'South Georgia and the South Sa', 1),
(189, 'Saint Helena', 1),
(190, 'Svalbard and Jan Mayen', 1),
(191, 'Solomon Islands', 1),
(192, 'Sierra Leone', 1),
(193, 'El Salvador', 1),
(194, 'San Marino', 1),
(195, 'Somalia', 1),
(196, 'Saint Pierre and Miquelon', 1),
(197, 'Sao Tome and Principe', 1),
(198, 'Suriname', 1),
(199, 'Slovakia', 1),
(200, 'Slovenia', 1),
(201, 'Sweden', 1),
(202, 'Swaziland', 1),
(203, 'Seychelles', 1),
(204, 'Syria', 1),
(205, 'Turks and Caicos Islands', 1),
(206, 'Chad', 1),
(207, 'Togo', 1),
(208, 'Thailand', 1),
(209, 'Tajikistan', 1),
(210, 'Tokelau', 1),
(211, 'Turkmenistan', 1),
(212, 'East Timor', 1),
(213, 'Tonga', 1),
(214, 'Trinidad and Tobago', 1),
(215, 'Tunisia', 1),
(216, 'Turkey', 1),
(217, 'Tuvalu', 1),
(218, 'Taiwan', 1),
(219, 'Tanzania', 1),
(220, 'Uganda', 1),
(221, 'Ukraine', 1),
(222, 'United States Minor Outlying I', 1),
(223, 'Uruguay', 1),
(224, 'United States', 1),
(225, 'Uzbekistan', 1),
(226, 'Holy See (Vatican City State)', 1),
(227, 'Saint Vincent and the Grenadin', 1),
(228, 'Venezuela', 1),
(229, 'Virgin Islands, British', 1),
(230, 'Virgin Islands, U.S.', 1),
(231, 'Vietnam', 1),
(232, 'Vanuatu', 1),
(233, 'Wallis and Futuna', 1),
(234, 'Samoa', 1),
(235, 'Yemen', 1),
(236, 'Yugoslavia', 1),
(237, 'South Africa', 1),
(238, 'Zambia', 1),
(239, 'Zimbabwe', 1);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `fld_id` int(11) NOT NULL,
  `fld_name` varchar(128) COLLATE utf8_bin NOT NULL,
  `fld_code` varchar(2) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`fld_id`, `fld_name`, `fld_code`) VALUES
(1, 'Afghanistan', 'AF'),
(2, 'Albania', 'AL'),
(3, 'Algeria', 'DZ'),
(4, 'American Samoa', 'AS'),
(5, 'Andorra', 'AD'),
(6, 'Angola', 'AO'),
(7, 'Anguilla', 'AI'),
(8, 'Antarctica', 'AQ'),
(9, 'Antigua and Barbuda', 'AG'),
(10, 'Argentina', 'AR'),
(11, 'Armenia', 'AM'),
(12, 'Aruba', 'AW'),
(13, 'Australia', 'AU'),
(14, 'Austria', 'AT'),
(15, 'Azerbaijan', 'AZ'),
(16, 'Bahamas', 'BS'),
(17, 'Bahrain', 'BH'),
(18, 'Bangladesh', 'BD'),
(19, 'Barbados', 'BB'),
(20, 'Belarus', 'BY'),
(21, 'Belgium', 'BE'),
(22, 'Belize', 'BZ'),
(23, 'Benin', 'BJ'),
(24, 'Bermuda', 'BM'),
(25, 'Bhutan', 'BT'),
(26, 'Bolivia', 'BO'),
(27, 'Bosnia and Herzegowina', 'BA'),
(28, 'Botswana', 'BW'),
(29, 'Bouvet Island', 'BV'),
(30, 'Brazil', 'BR'),
(31, 'British Indian Ocean Territory', 'IO'),
(32, 'Brunei Darussalam', 'BN'),
(33, 'Bulgaria', 'BG'),
(34, 'Burkina Faso', 'BF'),
(35, 'Burundi', 'BI'),
(36, 'Cambodia', 'KH'),
(37, 'Cameroon', 'CM'),
(38, 'Canada', 'CA'),
(39, 'Cape Verde', 'CV'),
(40, 'Cayman Islands', 'KY'),
(41, 'Central African Republic', 'CF'),
(42, 'Chad', 'TD'),
(240, 'Channel Islands', 'CI'),
(43, 'Chile', 'CL'),
(44, 'China', 'CN'),
(45, 'Christmas Island', 'CX'),
(46, 'Cocos (Keeling) Islands', 'CC'),
(47, 'Colombia', 'CO'),
(48, 'Comoros', 'KM'),
(49, 'Congo', 'CG'),
(50, 'Cook Islands', 'CK'),
(51, 'Costa Rica', 'CR'),
(52, 'Cote D''Ivoire', 'CI'),
(53, 'Croatia', 'HR'),
(54, 'Cuba', 'CU'),
(55, 'Cyprus', 'CY'),
(56, 'Czech Republic', 'CZ'),
(57, 'Denmark', 'DK'),
(58, 'Djibouti', 'DJ'),
(59, 'Dominica', 'DM'),
(60, 'Dominican Republic', 'DO'),
(61, 'East Timor', 'TP'),
(62, 'Ecuador', 'EC'),
(63, 'Egypt', 'EG'),
(64, 'El Salvador', 'SV'),
(65, 'Equatorial Guinea', 'GQ'),
(66, 'Eritrea', 'ER'),
(67, 'Estonia', 'EE'),
(68, 'Ethiopia', 'ET'),
(69, 'Falkland Islands (Malvinas)', 'FK'),
(70, 'Faroe Islands', 'FO'),
(71, 'Fiji', 'FJ'),
(72, 'Finland', 'FI'),
(73, 'France', 'FR'),
(74, 'France, Metropolitan', 'FX'),
(75, 'French Guiana', 'GF'),
(76, 'French Polynesia', 'PF'),
(77, 'French Southern Territories', 'TF'),
(78, 'Gabon', 'GA'),
(79, 'Gambia', 'GM'),
(80, 'Georgia', 'GE'),
(81, 'Germany', 'DE'),
(82, 'Ghana', 'GH'),
(83, 'Gibraltar', 'GI'),
(84, 'Greece', 'GR'),
(85, 'Greenland', 'GL'),
(86, 'Grenada', 'GD'),
(87, 'Guadeloupe', 'GP'),
(88, 'Guam', 'GU'),
(89, 'Guatemala', 'GT'),
(90, 'Guinea', 'GN'),
(91, 'Guinea-bissau', 'GW'),
(92, 'Guyana', 'GY'),
(93, 'Haiti', 'HT'),
(94, 'Heard and Mc Donald Islands', 'HM'),
(95, 'Honduras', 'HN'),
(96, 'Hong Kong', 'HK'),
(97, 'Hungary', 'HU'),
(98, 'Iceland', 'IS'),
(99, 'India', 'IN'),
(100, 'Indonesia', 'ID'),
(101, 'Iran (Islamic Republic of)', 'IR'),
(102, 'Iraq', 'IQ'),
(103, 'Ireland', 'IE'),
(104, 'Israel', 'IL'),
(105, 'Italy', 'IT'),
(106, 'Jamaica', 'JM'),
(107, 'Japan', 'JP'),
(108, 'Jordan', 'JO'),
(109, 'Kazakhstan', 'KZ'),
(110, 'Kenya', 'KE'),
(111, 'Kiribati', 'KI'),
(112, 'North Korea', 'KP'),
(113, 'Korea, Republic of', 'KR'),
(114, 'Kuwait', 'KW'),
(115, 'Kyrgyzstan', 'KG'),
(116, 'Lao People''s Democratic Republic', 'LA'),
(117, 'Latvia', 'LV'),
(118, 'Lebanon', 'LB'),
(119, 'Lesotho', 'LS'),
(120, 'Liberia', 'LR'),
(121, 'Libyan Arab Jamahiriya', 'LY'),
(122, 'Liechtenstein', 'LI'),
(123, 'Lithuania', 'LT'),
(124, 'Luxembourg', 'LU'),
(125, 'Macau', 'MO'),
(126, 'Macedonia', 'MK'),
(127, 'Madagascar', 'MG'),
(128, 'Malawi', 'MW'),
(129, 'Malaysia', 'MY'),
(130, 'Maldives', 'MV'),
(131, 'Mali', 'ML'),
(132, 'Malta', 'MT'),
(133, 'Marshall Islands', 'MH'),
(134, 'Martinique', 'MQ'),
(135, 'Mauritania', 'MR'),
(136, 'Mauritius', 'MU'),
(137, 'Mayotte', 'YT'),
(138, 'Mexico', 'MX'),
(139, 'Micronesia, Federated States of', 'FM'),
(140, 'Moldova, Republic of', 'MD'),
(141, 'Monaco', 'MC'),
(142, 'Mongolia', 'MN'),
(143, 'Montserrat', 'MS'),
(144, 'Morocco', 'MA'),
(145, 'Mozambique', 'MZ'),
(146, 'Myanmar', 'MM'),
(147, 'Namibia', 'NA'),
(148, 'Nauru', 'NR'),
(149, 'Nepal', 'NP'),
(150, 'Netherlands', 'NL'),
(151, 'Netherlands Antilles', 'AN'),
(152, 'New Caledonia', 'NC'),
(153, 'New Zealand', 'NZ'),
(154, 'Nicaragua', 'NI'),
(155, 'Niger', 'NE'),
(156, 'Nigeria', 'NG'),
(157, 'Niue', 'NU'),
(158, 'Norfolk Island', 'NF'),
(159, 'Northern Mariana Islands', 'MP'),
(160, 'Norway', 'NO'),
(161, 'Oman', 'OM'),
(162, 'Pakistan', 'PK'),
(163, 'Palau', 'PW'),
(164, 'Panama', 'PA'),
(165, 'Papua New Guinea', 'PG'),
(166, 'Paraguay', 'PY'),
(167, 'Peru', 'PE'),
(168, 'Philippines', 'PH'),
(169, 'Pitcairn', 'PN'),
(170, 'Poland', 'PL'),
(171, 'Portugal', 'PT'),
(172, 'Puerto Rico', 'PR'),
(173, 'Qatar', 'QA'),
(174, 'Reunion', 'RE'),
(175, 'Romania', 'RO'),
(176, 'Russian Federation', 'RU'),
(177, 'Rwanda', 'RW'),
(178, 'Saint Kitts and Nevis', 'KN'),
(179, 'Saint Lucia', 'LC'),
(180, 'Saint Vincent and the Grenadines', 'VC'),
(181, 'Samoa', 'WS'),
(182, 'San Marino', 'SM'),
(183, 'Sao Tome and Principe', 'ST'),
(184, 'Saudi Arabia', 'SA'),
(185, 'Senegal', 'SN'),
(186, 'Seychelles', 'SC'),
(187, 'Sierra Leone', 'SL'),
(188, 'Singapore', 'SG'),
(189, 'Slovak Republic', 'SK'),
(190, 'Slovenia', 'SI'),
(191, 'Solomon Islands', 'SB'),
(192, 'Somalia', 'SO'),
(193, 'South Africa', 'ZA'),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS'),
(195, 'Spain', 'ES'),
(196, 'Sri Lanka', 'LK'),
(197, 'St. Helena', 'SH'),
(198, 'St. Pierre and Miquelon', 'PM'),
(199, 'Sudan', 'SD'),
(200, 'Suriname', 'SR'),
(201, 'Svalbard and Jan Mayen Islands', 'SJ'),
(202, 'Swaziland', 'SZ'),
(203, 'Sweden', 'SE'),
(204, 'Switzerland', 'CH'),
(205, 'Syrian Arab Republic', 'SY'),
(206, 'Taiwan', 'TW'),
(207, 'Tajikistan', 'TJ'),
(208, 'Tanzania, United Republic of', 'TZ'),
(209, 'Thailand', 'TH'),
(210, 'Togo', 'TG'),
(211, 'Tokelau', 'TK'),
(212, 'Tonga', 'TO'),
(213, 'Trinidad and Tobago', 'TT'),
(214, 'Tunisia', 'TN'),
(215, 'Turkey', 'TR'),
(216, 'Turkmenistan', 'TM'),
(217, 'Turks and Caicos Islands', 'TC'),
(218, 'Tuvalu', 'TV'),
(219, 'Uganda', 'UG'),
(220, 'Ukraine', 'UA'),
(221, 'United Arab Emirates', 'AE'),
(222, 'United Kingdom', 'GB'),
(223, 'United States', 'US'),
(224, 'United States Minor Outlying Islands', 'UM'),
(225, 'Uruguay', 'UY'),
(226, 'Uzbekistan', 'UZ'),
(227, 'Vanuatu', 'VU'),
(228, 'Vatican City State (Holy See)', 'VA'),
(229, 'Venezuela', 'VE'),
(230, 'Viet Nam', 'VN'),
(231, 'Virgin Islands (British)', 'VG'),
(232, 'Virgin Islands (U.S.)', 'VI'),
(233, 'Wallis and Futuna Islands', 'WF'),
(234, 'Western Sahara', 'EH'),
(235, 'Yemen', 'YE'),
(236, 'Yugoslavia', 'YU'),
(237, 'Democratic Republic of Congo', 'CD'),
(238, 'Zambia', 'ZM'),
(239, 'Zimbabwe', 'ZW');

-- --------------------------------------------------------

--
-- Table structure for table `customers_contact_person`
--

CREATE TABLE `customers_contact_person` (
  `id` int(11) NOT NULL,
  `customers_id` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers_contact_person`
--

INSERT INTO `customers_contact_person` (`id`, `customers_id`, `first_name`, `last_name`, `email`, `phone`) VALUES
(1, '94314', 'Debasish', 'Maiti', 'debasish@gmail.com', '9809090923'),
(2, '13588', 'Ovie', 'Oghenejobo', 'ovie.@buno.com', '344494994'),
(3, '23248', 'qw', 'wr', 'as@gmail.com', '787654321'),
(4, '15358', 'qw', 'wr', 'as@gmail.com', '787654321'),
(5, '72836', 'dd', 'wr', 'as@gmail.com', '787654321'),
(6, '11824', 'Ovie', 'Oghenejobo', 'ovie.oghenejobo@gmail.com', '4694084465'),
(7, '29970', 'qw', 'wr', 'as@gmail.com', '787654321');

-- --------------------------------------------------------

--
-- Table structure for table `dashboard`
--

CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL,
  `dividframe` varchar(255) NOT NULL,
  `dividelement` varchar(255) NOT NULL,
  `data-gs-x` varchar(255) NOT NULL,
  `data-gs-y` varchar(255) NOT NULL,
  `data-gs-width` varchar(255) NOT NULL,
  `data-gs-height` varchar(255) NOT NULL,
  `span_contents` varchar(255) NOT NULL,
  `span_text` varchar(255) NOT NULL,
  `loginid` varchar(255) NOT NULL,
  `dashdate` date NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `event_identity` varchar(255) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `start_date_time` datetime NOT NULL,
  `end_date_time` datetime NOT NULL,
  `location` blob NOT NULL,
  `event_image` varchar(255) NOT NULL,
  `organiser` varchar(255) NOT NULL,
  `post_date` date NOT NULL,
  `status` tinyint(4) NOT NULL,
  `event_price` varchar(100) NOT NULL,
  `event_type` int(11) NOT NULL,
  `event_category` int(11) NOT NULL,
  `ticket_quantity` int(11) NOT NULL,
  `hot_status` tinyint(4) NOT NULL,
  `event_description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_identity`, `event_name`, `start_date_time`, `end_date_time`, `location`, `event_image`, `organiser`, `post_date`, `status`, `event_price`, `event_type`, `event_category`, `ticket_quantity`, `hot_status`, `event_description`) VALUES
(1, '', 'Champions League1', '2016-10-14 04:19:00', '2016-10-21 17:06:00', 0x4f6e656e65737320556e6976657273697479, '3.jpg', 'Test Organiser', '2016-11-18', 2, '0000-00-00', 1, 1, 100, 2, 'ghghghghghghg'),
(3, '', 'October & November Travel to Oneness University', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0x4f6e656e65737320556e6976657273697479, '35.jpg', '10', '2016-10-04', 1, '0000-00-00', 1, 1, 0, 2, 'asdasdasdasdasdasd'),
(4, '', 'October & November Travel to Oneness University', '2016-10-13 18:10:00', '1970-01-01 01:00:00', 0x4f6e656e65737320556e6976657273697479, '36.jpg', '10', '2016-10-04', 1, '500', 1, 1, 0, 2, 'asdasdasdasdasdasd'),
(5, '', 'October & November Travel to Oneness University', '2016-10-11 18:19:00', '1970-01-01 01:00:00', 0x4f6e656e65737320556e6976657273697479, 'a_(2).jpg', 'Test Organiser', '2016-11-18', 1, '500', 1, 1, 0, 2, 'sfsdfsdfsdfsdfsdf'),
(6, '', 'Champions League1', '2016-10-12 07:27:00', '2016-10-22 17:44:00', 0x4f6e656e65737320556e697665727369747965646464, '579c666ae7faf.jpg', 'Test Organiser', '2016-10-05', 2, '500', 1, 1, 100, 2, 'Lorem Ipsum is simply dummy text of the printing a...'),
(7, 'vsWm', 'Football', '2016-10-04 07:31:00', '2016-10-21 15:31:00', 0x576573742062656e67616c, 'demo.jpg', '100', '2016-10-17', 1, '400', 1, 1, 0, 2, 'asdasdasdasdasd'),
(8, '', 'October & November Travel to Oneness University', '2016-10-17 00:00:00', '2016-10-21 16:39:00', 0x4f6e656e65737320556e6976657273697479, 'venture-capital1.jpg', 'Test Organiser', '2016-10-17', 1, '500', 1, 1, 0, 0, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry'),
(9, '', 'Ragbi League', '2016-10-15 16:48:00', '2016-10-20 16:48:00', 0x4f6e656e65737320556e6976657273697479, '39.jpg', '100', '2016-10-14', 2, '500', 1, 1, 0, 2, 'sfsdfsdfsdfsdf'),
(10, '', 'IEA Presidential Debate 2016 Ghana', '2016-11-08 13:00:00', '1970-11-08 16:00:00', 0x52616461736820486f74656c2c2054616d616c65, 'IEADebate.jpg', 'IEA Ghana', '2016-10-30', 2, 'By Invitation', 1, 1, 200, 1, '0'),
(11, '', 'New Test Event', '2016-11-22 13:11:00', '2016-11-30 13:11:00', 0x3c703e4b6f6c6b6174613c2f703e, 'deever-rockwell-designed-house-on-a-bluff-in-olympia-fields-for-sale.jpg', '333', '2016-11-22', 1, '0', 1, 1, 0, 1, '<p>Lorem Ipsum Dolor Sit Amet</p>');

-- --------------------------------------------------------

--
-- Table structure for table `event_category`
--

CREATE TABLE `event_category` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_category`
--

INSERT INTO `event_category` (`category_id`, `category_name`, `status`) VALUES
(1, 'Auto, Boat & Air', 0),
(2, 'Business & Professional', 0),
(4, 'Community & Culture', 1);

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE `event_type` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_type`
--

INSERT INTO `event_type` (`type_id`, `type_name`, `status`) VALUES
(1, 'Appearance or Signing', '1');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `gallery_image` varchar(255) DEFAULT NULL,
  `gallery_name` text NOT NULL,
  `gallery_status` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `gallery_image`, `gallery_name`, `gallery_status`, `date`) VALUES
(74, 'Jellyfish.jpg', 'Test Gallery', '1', '2016-09-15'),
(76, 'Koala.jpg', 'Test Gallery123', '1', '2016-09-16'),
(82, 'Chrysanthemum.jpg', 'Test Gallery123', '1', '2016-09-27');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `member_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `dob` datetime NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `useremail` varchar(255) NOT NULL,
  `user_avatar` varchar(255) NOT NULL,
  `gender` enum('M','F') NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `last_log_data_time` datetime NOT NULL,
  `usercreationdate` datetime NOT NULL,
  `user_ip` varchar(255) NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `first_name`, `last_name`, `dob`, `username`, `password`, `useremail`, `user_avatar`, `gender`, `address`, `city`, `state`, `country`, `zipcode`, `last_log_data_time`, `usercreationdate`, `user_ip`, `status`) VALUES
(1, 'James', 'Hastingfffff', '2016-08-10 00:00:00', 'Hastingfffff', '123456', '', '31.jpg', 'M', 'Test Address', 'durgapur', '', '', '0', '0000-00-00 00:00:00', '2016-10-03 00:00:00', '', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `news_image` varchar(255) NOT NULL,
  `news_title` text NOT NULL,
  `posted_by` varchar(255) NOT NULL,
  `news_desc` text NOT NULL,
  `news_status` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `news_image`, `news_title`, `posted_by`, `news_desc`, `news_status`, `date`) VALUES
(123, 'Chrysanthemum.jpg', 'Test News 1', 'admin', '<p>dsfdsf</p>', '1', '2016-09-27'),
(124, '1111.jpg', 'Test News One', 'Test admin', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', '1', '2016-09-30');

-- --------------------------------------------------------

--
-- Table structure for table `newslettermail`
--

CREATE TABLE `newslettermail` (
  `mail_id` int(11) NOT NULL,
  `mail_name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `mail_address` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `mail_date` date NOT NULL DEFAULT '0000-00-00',
  `reply_subject` text COLLATE latin1_general_ci NOT NULL,
  `reply_message` text COLLATE latin1_general_ci NOT NULL,
  `reply_status` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `reply_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `newslettermail`
--

INSERT INTO `newslettermail` (`mail_id`, `mail_name`, `mail_address`, `mail_date`, `reply_subject`, `reply_message`, `reply_status`, `reply_date`) VALUES
(14, 'Rama Sen', 'jack@gmail.com', '2015-06-11', ' dfdg', ' fgfdg', 'yes', '2016-09-14'),
(21, 'Jack', 'Fan@gmail.com', '2016-09-16', ' uuuuuuuuuuuuu', ' uuuuuuuuuuuuuuuuuuuuuuuu', 'yes', '2016-09-13');

-- --------------------------------------------------------

--
-- Table structure for table `news_details`
--

CREATE TABLE `news_details` (
  `NewsId` int(11) NOT NULL,
  `CatId` int(11) NOT NULL,
  `Uid` int(11) NOT NULL,
  `ImagePath` varchar(255) NOT NULL,
  `NewsTitle` varchar(255) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Author` varchar(255) NOT NULL,
  `MetaKeywords` text NOT NULL,
  `MetaDescription` text NOT NULL,
  `NewsDescriptions` text NOT NULL,
  `NewsDate` date NOT NULL,
  `PublishedDate` datetime NOT NULL,
  `NewsStatus` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_details`
--

INSERT INTO `news_details` (`NewsId`, `CatId`, `Uid`, `ImagePath`, `NewsTitle`, `City`, `Author`, `MetaKeywords`, `MetaDescription`, `NewsDescriptions`, `NewsDate`, `PublishedDate`, `NewsStatus`) VALUES
(20, 0, 0, '56fe6695d1538.jpg', 'Earn Money For your Company Waste Oil', '', 'Admin', 'Earn Money For your Company Waste Oil', 'Earn Money For your Company Waste Oil', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has tpen the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has tpen the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2016-04-01', '0000-00-00 00:00:00', 'No'),
(22, 0, 0, '56fe6675dce92.jpg', 'Clean Fuel Sales', '', 'Admin', 'Clean Fuel Sales', 'Clean Fuel Sales', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has tpen the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has tpen the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2016-04-01', '0000-00-00 00:00:00', 'No'),
(23, 0, 0, '56fe66568b177.jpg', 'Waste Oil Recycling', '', 'Admin', 'Waste Oil Recycling', 'Waste Oil Recycling', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has tpen the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has tpen the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2016-04-01', '0000-00-00 00:00:00', 'No'),
(26, 0, 0, '56fe661ee22cb.jpg', 'Waste Oil Trading', '', 'Admin', 'Waste Oil Trading', 'Waste Oil Trading', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has tpen the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has tpen the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2016-04-01', '0000-00-00 00:00:00', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `order_id` int(11) NOT NULL,
  `order_no` varchar(255) NOT NULL,
  `eve_id` int(11) NOT NULL,
  `tic_id` int(11) NOT NULL,
  `ticket_num` varchar(255) NOT NULL,
  `ticket_price` varchar(255) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_email` varchar(255) NOT NULL,
  `customer_phone` bigint(20) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postalcode` varchar(255) NOT NULL,
  `address` blob NOT NULL,
  `pdf` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderdetails`
--

INSERT INTO `orderdetails` (`order_id`, `order_no`, `eve_id`, `tic_id`, `ticket_num`, `ticket_price`, `event_name`, `user_id`, `customer_name`, `customer_email`, `customer_phone`, `country`, `city`, `postalcode`, `address`, `pdf`, `status`) VALUES
(1, '1nYdjomprKs7IvnBSvOcx3Ekp9', 9, 16, 'Ks7IvnBSvOcx3Ekp9', '0', 'Ragbi League', 1, 'Lex Luthor', 'luthor@lexcorp.com', 9987656786, 'India', 'Durgapur', '123456', 0x41424320537472656574, '', 1),
(2, '10SubfkedVKs7IvnBSvOcx3Ekp9', 9, 16, 'Ks7IvnBSvOcx3Ekp9', '1368', 'Ragbi League', 10, 'Oliver Queen', 'arrow@gmail.com', 9855544478, 'India', 'ASAS', 'A2323', 0x41534153, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `organisation`
--

CREATE TABLE `organisation` (
  `oid` int(11) NOT NULL,
  `organisation_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `adrs` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `imagename` varchar(255) NOT NULL,
  `userstatus` tinyint(4) NOT NULL,
  `dateofjoin` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organisation`
--

INSERT INTO `organisation` (`oid`, `organisation_name`, `email`, `phone`, `password`, `adrs`, `description`, `imagename`, `userstatus`, `dateofjoin`) VALUES
(1, 'Jumbo Event', 'jumbo@gmail.com', '9999999999', 'MTExMTEx', '', '', '', 1, '2016-11-07 07:20:47');

-- --------------------------------------------------------

--
-- Table structure for table `organizer`
--

CREATE TABLE `organizer` (
  `organizer_id` int(11) NOT NULL,
  `organizer_name` varchar(255) NOT NULL,
  `organizer_avatar` varchar(255) NOT NULL,
  `organizer_phone` bigint(20) NOT NULL,
  `organizer_email` varchar(255) NOT NULL,
  `organizer_password` varchar(255) NOT NULL,
  `organizer_address` varchar(255) NOT NULL,
  `added_date` datetime NOT NULL,
  `organizer_category` int(11) NOT NULL,
  `organiser_website` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizer`
--

INSERT INTO `organizer` (`organizer_id`, `organizer_name`, `organizer_avatar`, `organizer_phone`, `organizer_email`, `organizer_password`, `organizer_address`, `added_date`, `organizer_category`, `organiser_website`, `status`) VALUES
(1, 'New Organizer', '579c666ae7faf.jpg', 9855555588, 'org@gmail.com', '', 'Org Addresstttt', '0000-00-00 00:00:00', 0, 'org website', '1'),
(3, 'Best Of The Best', 'NPD_3D01.jpg', 9855555587, 'best@gmail.com', '', 'Sant Ana', '0000-00-00 00:00:00', 0, 'west.com', '0'),
(4, 'Jumbo Event', '3.jpg', 9999999999, 'jumbo55@gmail.com', 'MTIzNDU2', '', '0000-00-00 00:00:00', 0, '', '1'),
(5, 'Oliver Queen', '', 9855544478, 'flashh@gmail.com', 'MTIz', '', '2016-11-17 10:56:59', 0, '', '1'),
(6, 'Barry Allen', '', 9855544478, 'flash@gmail.com', 'MTIz', '', '2016-11-17 10:58:49', 0, '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `facebook_link` text NOT NULL,
  `twitter_link` text NOT NULL,
  `youtube_link` text NOT NULL,
  `googleplus_link` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `map` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `facebook_link`, `twitter_link`, `youtube_link`, `googleplus_link`, `email`, `phone`, `address`, `map`) VALUES
(1, 'https://www.facebook.com/tikitpass', 'http://twitter.com/', 'http://youtu.be/', 'https://plus.google.com/collections/featured', 'info@tikitpass.com', '+44 (115)998-4412', 'NBV Enterprise Centre, 6 David Lane Nottingham NG6 0JU. United Kingdom', 'https://www.google.co.uk/maps/place/NBV+Enterprise+Centre/@52.9845902,-1.1837555,19z/data=!4m5!3m4!1s0x0:0x8f2eb345476d9f6d!8m2!3d52.9846077!4d-1.1831794?hl=en');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `posted_by` text NOT NULL,
  `testimonial_desc` text NOT NULL,
  `testimonial_image` varchar(255) NOT NULL,
  `testimonial_title` text NOT NULL,
  `testimonial_status` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `posted_by`, `testimonial_desc`, `testimonial_image`, `testimonial_title`, `testimonial_status`, `date`) VALUES
(1, 'WWW', 'FG', 'images345.jpg', 'WEB', '1', '2016-09-09'),
(2, 'ADMIN', 'Hi.................', 'images12.jpg', 'GOIGI', '1', '2016-09-09');

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `ticket_name` varchar(255) NOT NULL,
  `ticket_description` blob NOT NULL,
  `ticket_no` varchar(255) NOT NULL,
  `ticket_quantity` int(11) NOT NULL,
  `ticket_type` varchar(255) NOT NULL,
  `ticket_price` varchar(255) NOT NULL,
  `dateofadd` datetime NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`id`, `event_id`, `ticket_name`, `ticket_description`, `ticket_no`, `ticket_quantity`, `ticket_type`, `ticket_price`, `dateofadd`, `status`) VALUES
(8, 7, '', '', 'vsWmtV8hsXHuXFMiLnYG7', 20, '1', '200', '2016-10-06 12:29:51', 1),
(10, 7, '', '', 'vsWmAwY5oPzp2gJWktph7', 100, '0', '200', '2016-10-06 12:54:07', 1),
(11, 6, '', '', 'vsWmtV8hsXHuXFMiLnYG7', 20, '0', '200', '2016-10-06 12:29:51', 1),
(12, 1, '', '', 'IB0EEVR8AICsvS2W1', 50, '0', '200', '2016-10-12 08:46:15', 1),
(13, 1, '', '', '9vSwVNhaHRQhjJR81', 100, '1', '100', '2016-10-12 08:46:37', 1),
(16, 9, 'Free Ticket', 0x3c703e4c6f72656d20426861693c2f703e, 'Ks7IvnBSvOcx3Ekp9', 5, '0', '0', '2016-11-17 12:31:51', 1),
(17, 9, '', '', 'H1M6KmiCfEygbm2u9', 5, '1', '456', '2016-11-17 12:32:07', 1),
(18, 11, 'ticket 1', 0x3c703e4669727374205469636b65743c2f703e, '8YJ3GArb8fhErlYa11', 20, '1', '200', '2016-11-18 09:39:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `adrs` longtext NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_type` enum('u','o') NOT NULL,
  `description` varchar(255) NOT NULL,
  `imagename` varchar(255) NOT NULL,
  `userstatus` tinyint(4) NOT NULL,
  `dateofjoin` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `name`, `email`, `phone`, `password`, `adrs`, `username`, `user_type`, `description`, `imagename`, `userstatus`, `dateofjoin`) VALUES
(1, 'Lex Luthor', 'luthor@lexcorp.com', '9987656786', 'MTExMQ==', 'Lexcorp, NY', 'lexluthor', '', '0', 'user1.png', 1, '2016-10-18'),
(5, 'Kaushik', 'web010320161@goigi.asia', '9999999999', 'cXdlcnR5', '', 'web010320161', 'u', '', '', 1, '2016-10-19 06:04:13'),
(6, 'AKD', 'web0712153@goigi.asia', '9854125684', 'MTIz', '', 'AKD', 'u', '', '', 1, '2016-10-19 06:13:13'),
(7, 'supra', 'web2@goigi.net', '345345353345', 'MTExMQ==', '', 'sup', 'o', '', '', 1, '2016-10-19 06:18:30'),
(8, 'Oliver Queen', 'arrow@a.com', '9855544478', 'MTIz', '', '', 'u', '', '', 1, '2016-11-14 09:43:26'),
(9, 'Oliver', 'abc@gmail.com', '123456789', 'MTIz', '', '', 'u', '', '', 1, '2016-11-14 09:47:20'),
(10, 'Oliver Queen', 'arrow@gmail.com', '9855544478', 'MTIz', 'Test Address', '', 'u', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, co', 'medium_ManuelPrint1.125InchWide_.jpg', 1, '2016-11-14 11:28:44'),
(11, 'new user', 'newuser@gmail.com', '9855544478', 'MTIz', '', '', 'u', '', '', 1, '2016-11-18 07:46:57');

-- --------------------------------------------------------

--
-- Table structure for table `user_contact`
--

CREATE TABLE `user_contact` (
  `ContactId` int(11) NOT NULL,
  `Uid` int(11) NOT NULL,
  `ContactName` varchar(255) NOT NULL,
  `Subject` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Message` text NOT NULL,
  `reply_subject` text NOT NULL,
  `ReplyMessage` text NOT NULL,
  `ReplyStatus` enum('Yes','No') NOT NULL DEFAULT 'No',
  `ReplyDate` varchar(255) NOT NULL,
  `ContactDate` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_contact`
--

INSERT INTO `user_contact` (`ContactId`, `Uid`, `ContactName`, `Subject`, `Phone`, `Email`, `Message`, `reply_subject`, `ReplyMessage`, `ReplyStatus`, `ReplyDate`, `ContactDate`) VALUES
(50, 0, 'Alex', 'Test', '1122554477', 'alex@gmail.com', 'Hi', 'Hi...................', '<p>Hello</p>', 'Yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ad`
--
ALTER TABLE `ad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `administrator_detail`
--
ALTER TABLE `administrator_detail`
  ADD PRIMARY KEY (`AdminId`);

--
-- Indexes for table `admin_mail`
--
ALTER TABLE `admin_mail`
  ADD PRIMARY KEY (`MailId`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookedticket`
--
ALTER TABLE `bookedticket`
  ADD PRIMARY KEY (`bookticketid`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indexes for table `booking_status`
--
ALTER TABLE `booking_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_cookies`
--
ALTER TABLE `ci_cookies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `cms`
--
ALTER TABLE `cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`fld_id`);

--
-- Indexes for table `customers_contact_person`
--
ALTER TABLE `customers_contact_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dashboard`
--
ALTER TABLE `dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `event_category`
--
ALTER TABLE `event_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `event_type`
--
ALTER TABLE `event_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newslettermail`
--
ALTER TABLE `newslettermail`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `news_details`
--
ALTER TABLE `news_details`
  ADD PRIMARY KEY (`NewsId`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `organisation`
--
ALTER TABLE `organisation`
  ADD PRIMARY KEY (`oid`);

--
-- Indexes for table `organizer`
--
ALTER TABLE `organizer`
  ADD PRIMARY KEY (`organizer_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `user_contact`
--
ALTER TABLE `user_contact`
  ADD PRIMARY KEY (`ContactId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ad`
--
ALTER TABLE `ad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `administrator_detail`
--
ALTER TABLE `administrator_detail`
  MODIFY `AdminId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `admin_mail`
--
ALTER TABLE `admin_mail`
  MODIFY `MailId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bookedticket`
--
ALTER TABLE `bookedticket`
  MODIFY `bookticketid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `booking_status`
--
ALTER TABLE `booking_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ci_cookies`
--
ALTER TABLE `ci_cookies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms`
--
ALTER TABLE `cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `fld_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;
--
-- AUTO_INCREMENT for table `customers_contact_person`
--
ALTER TABLE `customers_contact_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `dashboard`
--
ALTER TABLE `dashboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `event_category`
--
ALTER TABLE `event_category`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `event_type`
--
ALTER TABLE `event_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;
--
-- AUTO_INCREMENT for table `newslettermail`
--
ALTER TABLE `newslettermail`
  MODIFY `mail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `news_details`
--
ALTER TABLE `news_details`
  MODIFY `NewsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `orderdetails`
--
ALTER TABLE `orderdetails`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `organisation`
--
ALTER TABLE `organisation`
  MODIFY `oid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `organizer`
--
ALTER TABLE `organizer`
  MODIFY `organizer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `user_contact`
--
ALTER TABLE `user_contact`
  MODIFY `ContactId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
