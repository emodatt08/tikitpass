<?php 
class Home_model extends CI_Model{
	function __construct() {
        parent::__construct();
		$this->load->database();
		
   }
 
 function show_cms_id_2()
	{   $this->db->select('*');
		$this->db->from('cms');
		$this->db->where('id',2);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
 function show_cms_id_6()
	{   $this->db->select('*');
		$this->db->from('cms');
		$this->db->where('id',6);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
 function show_cms_id_5()
	{   $this->db->select('*');
		$this->db->from('cms');
		$this->db->where('id',5);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
 function show_cms_id_4()
	{   $this->db->select('*');
		$this->db->from('cms');
		$this->db->where('id',4);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}	
//===============country====================
function country()
	{   $this->db->select('*');
		$this->db->from('country');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

function sessiondetails($sesid)
	{   $this->db->select('*');
		$this->db->from('user');
		$this->db->where('uid',$sesid);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===============country====================
 
// ================================================For CMS section**********Start here=====================================   
 function show_cms()
	{   $this->db->select('*');
		$this->db->from('cms');
		$this->db->where('id',4);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===========================================For CMS section*******End here==========================================	
// ================================================For NEWS section**********Start here=====================================   
 function show_ad()
	{   $this->db->select('*');
		$this->db->from('ad');
		$this->db->where('status',1);
		$this->db->limit(3);
		$this->db->order_by("id", "desc");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===========================================For NEWS section*******End here==========================================		
// ================================================For CMS2 section**********Start here=====================================   
 function show_cms2()
	{   $this->db->select('*');
		$this->db->from('cms');
		$this->db->where('id',5);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===========================================For CMS 2 section*******End here==========================================

// ================================================For Gallery section**********Start here=====================================   
 function show_event()
	{   $this->db->select('*');
		$this->db->from('event');
		$this->db->where('status',1);
		$this->db->limit(3);
		$this->db->order_by("event_id", "desc");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	

//===========================================For Gallery section*******End here==========================================

//===========================================For Social============================================
	 function show_social()
	{   $this->db->select('*');
		$this->db->from('settings');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
//===========================================For Social============================================

// ================================================For Gallery section**********Start here=====================================   
 public function get_menu() {
    $query = $this->db->get_where('category', array('parent_id' => 0));
    if ($query->num_rows() > 0):
        return $query;
    endif;
}

public function get_submenu() {
    $query = $this->db->get_where('category', array('parent_id' => 0));
    foreach ($query->result() as $row):
        $cat_id = $row->category_id;
    endforeach;
    if ($cat_id != FALSE):
        $this->db->from('category');
        $this->db->where('parent_id', $cat_id);
        $query = $this->db->get();
        return $query;
    else:
        return FALSE;
    endif;
}
//===========================================For Gallery section*******End here==========================================
function update_ticketgen_status($ticketid, $data){
		$this->db->where('tkt_id', $ticketid);
		$this->db->update('booking',$data);
	}
function insert_booked_ticket($dataticket){
		$this->load->database();
	    $this->db->insert('bookedticket', $dataticket); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}	
function show_pdf($ticketid){
		$this->db->select ('booking.*,bookedticket.*'); 
		$this->db->from('booking');
		$this->db->join('bookedticket', 'bookedticket.tckt_id = booking.tkt_id');
		$this->db->where('booking.tkt_id',$ticketid);
		$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}	
function cmsfetch(){
		$this->db->select('*');
		$this->db->from('cms');
		$this->db->where('id',2);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
function mapfetch(){
		$this->db->select('*');
		$this->db->from('settings');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}	
}
?>
