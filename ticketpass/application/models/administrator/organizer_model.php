<?php 
class organizer_model extends CI_Model{
	function __construct() {
        parent::__construct();
   }
   
	public function insert_organizer($data) {
		$this->load->database();
	    $this->db->insert('organizer', $data); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}
	
	function show_organizer()
	{
		$sql ="select * from organizer ";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
	
	
	function show_organizer_id($id){
		$this->db->select('*');
		$this->db->from('organizer');
		$this->db->where('organizer_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
	function organizer_edit($id, $data, $organizer_img){
	
		$this->db->where('organizer_id', $id);
		
		$this->db->update('organizer',$data);
	}
	
	function updt($stat,$id){
	
		$sql ="update organizer set status=$stat where id=$id ";
		$query = $this->db->query($sql);
		//return($query->num_rows() > 0) ? $query->result(): NULL;
	}
	function show_organizerlist()
	{
		$sql ="select * from organizer";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
	
	
	function delete_organizer($id,$organizer_img){
		$this->db->where('organizer_id', $id);
		unlink("uploads/organizer/".$organizer_img);
		$this->db->delete('organizer');	
		}
}
?>
