<?php 
 class Member_model extends CI_Model{
	//================Insert Member================ 
	public function insert_member($data) {
		$this->load->database();
	    $this->db->insert('member', $data); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}
	//================Insert Member================
	
	//================Show Member================
	function show_member(){
		$sql ="select * from member where status='Yes'";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
	//================Show Member================
	
	//================Show Member By Id==============
	function member_view($id){
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('member_id', $id);
		$query = $this->db->get();
		return($query->num_rows() > 0) ? $query->result(): NULL;
		
	}
	//================Show Member By Id==============
	
	//================Delete Member==============
	function delete_member($id,$member_image){
	  $this->db->where('member_id', $id);
	  unlink("memberavatar/".$member_image);
      $this->db->delete('member'); 
	}
	//================Delete member==============
	//=============Show Member By Id=============
	function show_member_id($id){
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('member_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	//=============Show Member By Id=============
	//=============Member Edit===================
	function member_edit($id, $datalist,$user_avatar){
		$this->db->where('member_id', $id);
		@unlink("memberavatar/".$user_avatar);
		$this->db->update('member',$datalist);
	}
	//=============Member Edit===================
}



?>