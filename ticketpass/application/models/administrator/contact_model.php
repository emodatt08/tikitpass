<?php 
class Contact_model extends CI_Model{
	function __construct() {
        parent::__construct();
	$this->load->database();	
}
function show_contact()
	{
        $this->db->select('*');
		$this->db->from('user_contact');
	    $query = $this->db->get();
		$result = $query->result();
		return $result;
  }
function show_contact_id($id){
		$this->db->select('*');
		$this->db->from('user_contact');
		$this->db->where('ContactId', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
	 public function insert_contact($data,$id) {
		$this->load->database();
	    $this->db->update('user_contact',$data);
		$this->db->where('ContactId', $id); 
		//$this->db->limit(1,5);
		   if ($this->db->affected_rows() > 1) {
			return true;
		   }   else {
			  return false;
		     }
	    }
	
function delete_contact($id){

	  $this->db->where('ContactId', $id);

      $this->db->delete('user_contact'); 

	}
		function administratormail(){
		$this->load->database();
		$sql ="select * from admin_mail";
		
		$query1 = $this->db->query($sql);
		return($query1->num_rows() > 0) ? $query1->result(): NULL;
		}
}
?>