<?php 
 class Event_model extends CI_Model{
	//================Insert Category================ 
	public function insert_category($data) {
		$this->load->database();
	    $this->db->insert('event_category', $data); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}
	//================Insert Category================
	//================Show Member================
	function show_category(){
		$sql ="select * from event_category";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
	//================Show Member================
	//================Show Member By Id==============
	function member_view($id){
		$this->db->select('*');
		$this->db->from('member');
		$this->db->where('member_id', $id);
		$query = $this->db->get();
		return($query->num_rows() > 0) ? $query->result(): NULL;
		
	}
	//================Show Member By Id==============
	//================Delete Member==============
	function delete_category($id){
	  $this->db->where('category_id',$id);
	  $this->db->delete('event_category'); 
	}
	//================Delete member==============
	//=============Show Member By Id=============
	function show_category_id($id){
		$this->db->select('*');
		$this->db->from('event_category');
		$this->db->where('category_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	//=============Show Member By Id=============
	//=============Member Edit===================
	function edit_category($id, $datalist){
		$this->db->where('category_id', $id);
		$this->db->update('event_category',$datalist);
	}
	//=============Member Edit===================
	//***************============Event Type Section============****************
	//===============Insert=====================
	public function insert_type($data) {
		$this->load->database();
	    $this->db->insert('event_type', $data); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}
	//================Insert Type================
	//================Show Type==================
	function show_type(){
		$sql ="select * from event_type";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
	//================Show Type==================
	//================Delete Member==============
	function delete_type($id){
	  $this->db->where('type_id',$id);
	  $this->db->delete('event_type'); 
	}
	//================Delete member==============
	//=============Show Member By Id=============
	function show_type_id($id){
		$this->db->select('*');
		$this->db->from('event_type');
		$this->db->where('type_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	//=============Show Member By Id=============
	//*********===============Event Section===============********//
	public function insert_event($data) {
		$this->load->database();
	    $this->db->insert('event', $data); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}
	//================Insert Type================
	//================Show Type==================
	function show_event(){
		$sql ="select * from event order by event_id desc";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
	//================Show Type==================
	//================Get Total Free Sales======
	function show_free_sales($id){
		$this->db->select('count(booking_id) as freesales');
		$this->db->from('booking');
		$this->db->where('event_id', $id);
		$this->db->where('ticket_type', 0);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	//================Get Total Free Sales======
	//================Get Total Paid Sales======
	function show_paid_sales($id){
		$this->db->select('count(booking_id) as paidsales');
		$this->db->from('booking');
		$this->db->where('event_id', $id);
		$this->db->where('ticket_type', 1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	//================Get Total Paid Sales======
	//================Get Total Paid Sales======
	function show_paid_amount($id){
		$this->db->select('sum(amount) as sumamt');
		$this->db->from('booking');
		$this->db->where('event_id', $id);
		$this->db->where('ticket_type', 1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	//================Get Total Paid Sales======
	//================Get Total Free Ticket=====
	function show_free_total($id){
		$this->db->select('*');
		$this->db->from('ticket');
		$this->db->where('event_id',$id);
		$this->db->where('ticket_type',0);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	//================Get Total Free Ticket=====
	//================Get Total Paid Ticket=====
	function show_paid_total($id){
		$this->db->select('*');
		$this->db->from('ticket');
		$this->db->where('event_id',$id);
		$this->db->where('ticket_type',1);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	//================Get Total Paid Ticket=====
	//================Delete Member==============
	function delete_event($id){
	  $this->db->where('event_id',$id);
	  $this->db->delete('event'); 
	}
	//================Delete member==============
	//=============Show Member By Id=============
	function show_event_id($id){
		$this->db->select('*');
		$this->db->from('event');
		$this->db->where('event_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	//*********===============Event Section===============********//
	//=============Member Edit===================
	function event_edit($id, $datalist){
		$this->db->where('event_id', $id);
		$this->db->update('event',$datalist);
	}
	//=============Member Edit===================
	//***************============Event Type Section============****************
}



?>