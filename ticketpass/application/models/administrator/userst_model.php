<?php 
class Userst_model extends CI_Model{
	function __construct() {
        parent::__construct();
   }
public function insert_users($data) {
		$this->load->database();
	    $this->db->insert('user', $data); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}
function show_users()
	{
		$sql ="select * from user ";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
function show_users_id($id){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('uid', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
function show_countries()
	{
		$sql ="select * from countries ";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}	
	
function users_view($id){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('uid', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
function users_view_join($id){
		$this->db->select('user.*,countries.country_name');
		$this->db->from('user');
		$this->db->join('countries', 'countries.country_id = user.country', 'INNER'); 
		$this->db->where('user.uid', $id);
		$this->db->limit('1,0');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}	
	
function users_edit($id, $data){
	
		$this->db->where('uid', $id);
		$this->db->update('user',$data);
	}
function updt($stat,$id){
	 
		$sql ="update user set userstatus=$stat where uid=$id ";
		$query = $this->db->query($sql);
		//return($query->num_rows() > 0) ? $query->result(): NULL;
	}
function show_userslist()
	{
		$sql ="select * from user";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
function delete_users($id,$users_image){
		$this->db->where('uid', $id);
		unlink("users/".$users_image);
		$this->db->delete('user');	
		}
function delete_mul($ids)//Delete Multiple users
		{
			$ids = $ids;
			$count = 0;
			foreach ($ids as $id){
			$did = intval($id).'<br>';
			$this->db->where('uid', $did);
			unlink("users/".$users_image);
			$this->db->delete('user');  
			$count = $count+1;
			}
			
			echo'<div class="alert alert-success" style="margin-top:-17px;font-weight:bold">
			'.$count.' Item deleted successfully
			</div>';
			$count = 0;		
		}
	}
?>