<?php 
class Ad_model extends CI_Model{
	function __construct() {
        parent::__construct();
   }
   
	public function insert_ad($data) {
		$this->load->database();
	    $this->db->insert('ad', $data); 
		if ($this->db->affected_rows() > 1) {
			return true;
		} else {
			return false;
		}
	}
	
	function show_ad()
	{
		$sql ="select * from ad ";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
	
	
	function show_ad_id($id){
		$this->db->select('*');
		$this->db->from('ad');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
	function ad_edit($id, $data, $ad_img){
	
		$this->db->where('id', $id);
		
		$this->db->update('ad',$data);
	}
	
	function show_adlist()
	{
		$sql ="select * from ad";
		$query = $this->db->query($sql);
		return($query->num_rows() > 0) ? $query->result(): NULL;
	}
	
	
	function delete_ad($id,$ad_img){
		$this->db->where('id', $id);
		unlink("ad/".$ad_img);
		$this->db->delete('ad');	
		}
}
?>
