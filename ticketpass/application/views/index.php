﻿<style>
p {
 float:right;	
 margin: 0 0 10px;
}
</style>
  <!-- Body Content -->
  <div class="content_bg">
  	<div class="container">
    	<div class="row">
        	<div class="col-sm-3">
            	<div class="left_sec">
                	<div class="box">
                    	<h3 class="box_title">Hot Tickets</h3>
                        <ul class="list">
                        	<?php foreach($hotcategory as $hc){?>
                        		<li><a href="<?php echo base_url()?>eventdetails/show_event_details/<?php echo $hc->event_id?>"><?php echo $hc->event_name?></a></li>
                            <?php }?>
                        </ul>
                    </div>
                    <div class="box">
                    	<h3 class="box_title">Last Minute Deals</h3>
                    </div>
                    <div class="box cu_accordion">
                    	<h3 class="box_title">Events</h3>
                        <!-- Accordion -->
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<?php if(is_array($eevent)): ?>
                            <?php 
							$ctn=1;
							foreach($eevent as $c){ 
							?>
                              <div class="panel panel-default" id="<?php echo $ctn;?>">
                                <div class="panel-heading" role="tab" id="heading<?=$ctn?>">
                                  <h4 class="panel-title">
                                    <a role="button" id="<?php echo $ctn;?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$ctn?>" 
									<?php if($ctn=='1') {?>aria-expanded="true" <?php }else {?> aria-expanded="false" <?php }?> aria-controls="collapse<?=$ctn?>">
                                      <?php echo $c->event_name;?>
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapse<?=$ctn?>" class="panel-collapse collapse <?php if($ctn=='1') {?>in<?php }?>" role="tabpanel" aria-labelledby="heading<?=$ctn?>">
                                  <div class="panel-body">
                                  <?php if($c->event_image==''){?><?php }else{?>
                                    <a href="<?php echo base_url()?>eventdetails/show_event_details/<?php echo $c->event_id?>"><img src="<?php echo base_url()?>uploads/event/<?php echo $c->event_image;?>" class="img-responsive"></a><br>
                                    <?php }?>
                                    <p><?=substr($c->event_description, 0,50)?></p>
                                  </div>
                                </div>
                              </div>
                              <?php $ctn++;}?>
                    		  <?php endif; ?>
                            </div>
                        <!-- End Accordion -->
                    </div>
                    <div class="box">
                    	<h3 class="box_title">Movies</h3>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
            	<div class="box po_re">
                	<ul class="bxslider">
					<?php if(is_array($ebanner)): ?>
                    <?php foreach($ebanner as $b){ ?>
                      <li><img src="<?php echo base_url()?>banner/<?php echo $b->banner_img;?>" title="<?php echo $b->banner_description;?>" /></li>
                    <?php }?>
                    <?php endif; ?>  
                  </ul>
                </div>
                <div class="box">
                	<div class="tab">
                      <!-- Tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Hot Tickets</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Just Announced</a></li>
                      </ul>
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                        	<?php foreach($hotcategory as $hc){?>
                                <h4 class="eventsShowList"><a href="<?php echo base_url()?>eventdetails/show_event_details/<?php echo $hc->event_id?>"><?php echo $hc->event_name?></a></h4>
                                <p class="events_text"><?php echo substr($hc->event_description,0,10)?> <a href="<?php echo base_url()?>eventdetails/show_event_details/<?php echo $hc->event_id?>" class="buy_tickets_btn">Buy Tickets</a></p>
                                <div class="divider"></div>
                            <?php }?>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div role="tabpanel" class="tab-pane fade" id="profile">
                        	<?php foreach($latestevent as $lc){?>
                        	<h4 class="eventsShowList"><a href="<?php echo base_url()?>eventdetails/show_event_details/<?php echo $lc->event_id?>"><?php echo $lc->event_name?></a></h4>
                            <p class="events_text"><?php echo substr($lc->event_description,0,10)?></p>
                            <div class="divider"></div>
                            <?php }?>
                            <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
            	<!--<a class="btn btn-default cu_btn" href="#" role="button">Log In as a Customer</a>
                <a class="btn btn-default cu_btn" href="#" role="button">Log In as a Trader</a>-->
                <div class="box">
                	<h3 class="box_title">Type your ticket code below</h3>
                    <div class="form-group">
                        <input type="text" class="form-control cu_input" id="exampleInputEmail1" placeholder="Type your ticket code">
                        <button class="btn btn-default cu_btn" href="#" role="button">Click to Print your Ticket</button>
                    </div>
                </div>
                <div class="box">
                    	<a class="btn btn-default cu_btn" href="<?php echo base_url()?>organizersignup">Register Event</a>
                </div>
                <div class="box">
                	<h3 class="box_title">Social Media</h3>
                    <div class="social">
                    <?php if(is_array($esocial)): ?>
                    <?php foreach($esocial as $soc){ ?>
                    	<a href="http://<?php echo $soc->facebook_link;?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                        <a href="http://<?php echo $soc->twitter_link;?>" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                        <a href="http://<?php echo $soc->youtube_link;?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <?php }?>
                    <?php endif; ?>     
                    </div>
                </div>
                <div class="box">
                	<h3 class="box_title">Advertisement</h3>
                    <?php if(is_array($ead)): ?>
                    <?php foreach($ead as $a){ ?>
                    <div class="advertisement">
                    <a href="http://<?php echo $a->ad_link;?>" target="_blank">
                    <img src="<?php echo base_url()?>ad/<?php echo $a->ad_img;?>" class="img-responsive" style="width:223px; height:126px;">
                    </a>
                    </div>
                    <?php }?>
                    <?php endif; ?>
                <!--<div class="advertisement">
                    <img src="<?php echo base_url()?>images/ad1.jpg" class="img-responsive">
                    </div>
                    <div class="advertisement">
                    <img src="<?php echo base_url()?>images/ad1.jpg" class="img-responsive">
                    </div>-->
                </div>
            </div>
        </div>
    </div>
  </div>
  <!-- Body Content -->
