<style>
#errmsg
{
color: red;
}
</style>
<div class="contact_us_col">
            	<div class="container">
                	<div class="row">
                    	<div class="col-sm-12">
                        	<div class="contact-top">
                            	<img src="<?php echo base_url(); ?>image/contact_us_banner.jpg" alt="contact us">
									<h2>Contact Us</h2>	                            	
                          </div>
                          	
                            <div class="contact_bottom">
                            	<div class="col-sm-5">
                                	<div class="form_area">
                                    <div style="color:#ff6666;text-align:center;"> <strong><?php 
					if($this->session->flashdata('email_sent') !=""){
					echo $this->session->flashdata('email_sent');
					} ?></strong>
					</div>
                                    	<div class="account-create">
                                		
											<form class="form-inline" action="<?php echo base_url(); ?>/contact/send_mail" method="post" onSubmit="return Submit()">
                    							<div class="form-group">
                                       				 <label for="exampleInputName2">Name</label>
                                       					 <input type="text" class="form-control" id="name" placeholder="Name" name="ContactName" onkeyup="leftTrim(this)">
                                                         <p id="errorBox1" style="color:#ff0000;"></p>
                                                         <?php echo form_error('ContactName'); ?>
                                        		</div>
                                                
                                            		<div class="form-group">
                                            			<label for="exampleInputEmail2">Subject</label>
                                            				<input type="text" class="form-control" id="Subject" name="Subject" placeholder="Subject" onkeyup="leftTrim(this)" onkeypress="myFunction()">
                                                            <p id="errorBox2" style="color:#ff0000;"></p>
                                                            <?php echo form_error('Subject'); ?>
                                            		</div>
                                                   


                                            
                                            <div class="form-group">
                                                    	<label for="exampleInputEmail2">Phone</label>
                                                    	<input type="text" class="form-control" placeholder="Phone" name="Phone" onkeyup="leftTrim(this)" onkeypress="return isNumber(event)" id="phone"  maxlength="11">
                                                        <p id="errorBox3" style="color:#ff0000;"></p>
                                                        <?php echo form_error('Phone'); ?><span id="errmsg"></span>
                                                    </div>
                                            
                                                     <div class="form-group">
                                                    	<label for="exampleInputEmail2">Email</label>
                                                    	<input type="text" class="form-control" id="email" name="Email" placeholder="Email" onkeyup="leftTrim(this)">
                                                        <p id="errorBox4" style="color:#ff0000;"></p>
                                                        <?php echo form_error('Email'); ?>
                                                    </div>
                                                     
                                             
                                           
                                            
                                                                                
                                        <div class="form-group">


</div>
											
                                         <div class="form-group">
                                        <label for="exampleInputName2">Message</label>
                                        <textarea placeholder="Comment" cols="10" rows="5" id="message" name="Message" onkeyup="leftTrim(this)"></textarea>	
                                        <p id="errorBox5" style="color:#ff0000;"></p>
                                        <?php echo form_error('Message'); ?>
                                        </div>   



                                    	<button type="submit" class="btn btn-default">SUBMIT</button>
                                        
                                  </form>
                                  
								<script>
								function Submit(){
								
								if($("#name").val() == "" ){
								   $("#name").focus();
								   $("#errorBox1").html("Please Enter Name");
								   return false;
								  }else{
									$("#errorBox1").html("");  
								  }
								  if($("#Subject").val() == "" ){
								   $("#Subject").focus();
								   $("#errorBox2").html("Please Enter Subject");
								   return false;
								  }else{
									$("#errorBox2").html("");  
								  }
								  
								  if($("#phone").val() == "" ){
								   $("#phone").focus();
								   $("#errorBox3").html("Please Enter Phone Number");
								   return false;
								  }else{
										$("#errorBox3").html(""); 
								  }
								  
								   var emailRegex = /^[A-Za-z0-9._]*\@[A-Za-z]*\.[A-Za-z]{2,5}$/;
								  var formemail = $("#email").val();
								  if($("#email").val() == "" ){
									$("#email").focus();
									$("#errorBox4").html("Please Enter Email");
									return false;
								  }
								  else if(!emailRegex.test(formemail)){
									$("#email").focus();
									$("#errorBox4").html("Please Enter Proper Email");
									return false;
								  }else{
										$("#errorBox4").html(""); 
								  }
								  
								   
									 if($("#message").val() == "" ){
								   $("#message").focus();
								   $("#errorBox5").html("Please Enter any Message");
								   return false;
								  }else{
										$("#errorBox5").html(""); 
								  }
								  }




function leftTrim(element){
if(element)
element.value=element.value.replace(/^\s+/,"");
}
</script>
<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>

                                </div>
                                    </div>
                                </div>
                                <div class="col-sm-5 last">
                                	<div class="contact_address">
                                    	<h2>Find Us</h2>
                                    	<!-- Inner panel design start here-->
                                        	<div class="inner_panel">
           
                                            	<!-- Corporate panel design start here-->
                                                	<div class="corporate_boxes">
                                                    	<h3>Corporate Address:</h3>
                                                         <?php if(is_array($map)): ?>
                										 <?php  foreach($map as $m){ ?>		
                                                        <p><?php echo $m->address;?></p>
                                                        
                                                    </div>
                                                <!-- Corporate panel design End here-->
                                                
                                                <!-- Phone panel design start here-->
                                                	<div class="phone_boxes">
                                                    	<h3>Phone:</h3>
                                                        <p><?php echo $m->phone;?></p>
                                                    </div>
                                                <!-- Phone panel design End here-->
                                                
                                                 <!-- Email panel design start here-->
                                                	<div class="email_boxes">
                                                    	<h3>Email:</h3>
                                                        <p><a href="mailto:support@consutinghotpot.com"><?php echo $m->email;?></a></p>
                                                    </div>
                                                <!-- Email panel design End here-->
                                                
                                                <!-- Website panel design start here-->
                                                	
                                                <!-- Website panel design End here-->
                                              <?php }?>
            											<?php endif; ?> 
                                            </div>
                                        <!-- Inner panel design End Here-->
                                    </div>
                                </div>
                            </div>                            
                      </div>
                    </div>
                </div>
                
                
                
                
                                
            </div>