<style>
.newbtn{
	width:20%;
	padding:10px !important;
	}
</style>
<script>
document.onkeydown=disableF5;
var version = navigator.appVersion;

function disableF5(e) 
{   var keycode = (window.event) ? event.keyCode : e.keyCode;

    if ((version.indexOf('MSIE') != -1)) 
    {  if (keycode == 116) 
       {  event.keyCode = 0;
          event.returnValue = false;
          return false;
       }
    }
    else 
    {  if (keycode == 116) 
          return false;
    }
}

$(function() {
            $(this).bind("contextmenu", function(e) {
                e.preventDefault();
            });
        }); 
</script>
<script type="text/javascript"> 
	document.onselectstart=new Function ("return false")  
	document.oncontextmenu=new Function ("return false")  
</script>
<link href="<?php echo base_url(); ?>css/datepicker.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,300,500&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.9.1/highlight.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>js/datepicker.js"></script>
<script src="<?php echo base_url(); ?>js/datepicker.en.js"></script>
<style>
.tprice{
	background:#fff;
	padding:15px;
	box-shadow:0px 5px 5px rgba(0,0,0,0.5);
	}
.text-white{
	color:#fff;
	text-align:center;
	}
</style>
<div class="content_bg">
  <div class="login-bg">
    <div class="login_sec">
      <div style="padding:20px;"></div>
      <h2 class="text-white">Checkout</h2>
      <div class="tprice">
      <div>
    <!-- 
     Event Name: XYZ
      Event Date: 17-11-2016 5:55 pm
      Type: Paid
      Quantity: 2
      Total Price: $250-->
    	
      <table class="table">
                              <thead>
                                <tr>
                                  <th>Event Name</th>
                                  <th>Event Date</th>
                                  <th>Ticket Type</th>
                                  <th>Quantity</th>
                                  <th>Total Price</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th scope="row"><?php echo $eventname; ?></th>
                                  <td><?php echo $eventdate; ?></td>
                                  <td><?php if($tickettype==1){ echo "Paid"; } else { echo "Free"; }?></td>
                                  <td><?php if($tickettype==1){ echo $ticketquantitypaid; } else { echo $ticketquantityfree; }?></td>
                                  <td>$<?php 
								  if($tickettype==1){
								  $pr = ($ticketprice * $ticketquantitypaid);
								  		echo $pr;
										}
										else{
											echo "0";
											}
								  ?></td>
                                </tr>
                                <tr>
                              </tbody>
                            </table>
</div>      </div>
     <!-- <div class="login_heading">dsdfsdf</div>-->

		<?php 
        $sesid = $this->session->userdata('is_userlogged_id');
        $login = $data['sessiondetails'] = $this->home_model->sessiondetails($sesid);
		foreach($login as $l){
        ?>
      <form action="<?php echo base_url();?>checkout/payment" method="post" name="signupform" onSubmit="return Submit()">
      <input type="hidden" name="eve_id" value="<?php echo $eventid; ?>">
      <?php if($tickettype==0){?>
      <input type="hidden" name="ticketquantity" value="<?php echo $ticketquantityfree; ?>">
      <?php }else {?>
      <input type="hidden" name="ticketquantity" value="<?php echo $ticketquantitypaid; ?>">
      <?php }?>
      <input type="hidden" name="tic_id" value="<?php echo $ticketid; ?>">
      <input type="hidden" name="ticket_num" value="<?php echo $ticketno; ?>">
      <input type="hidden" name="event_name" value="<?php echo $eventname; ?>">
      <input type="hidden" name="ticket_type" value="<?php echo $tickettype; ?>">
      <input type="hidden" name="ticket_price" value="<?php if($tickettype==1) { echo $pr; } else {echo "0"; }?>">
      <input type="hidden" name="user_id" value="<?=$this->session->userdata('is_userlogged_id')?>">
    	<div class="login_box">
        
        	<label class="login_label">Name</label>
            <label id="errorBox1" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                <input type="text" name="customer_name" id="myname" value="<?php echo $l->name; ?>" class="login_input" placeholder="Name" onkeyup="leftTrim(this)">
          	</div>
            
            <label class="login_label">Email</label>
            <label id="errorBox2" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                <input type="email" name="customer_email" id="email" value="<?php echo $l->email; ?>" class="login_input" placeholder="Email" onkeyup="leftTrim(this)">
          	</div>
            
            <label class="login_label">Confirm Email</label>
            <label id="errorBoxce" style="color:#ff0000;"></label>
            <label id="errorBoxe" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                <input type="email" name="con_email" id="con_email" value="" class="login_input" placeholder="Confirm Email" onkeyup="leftTrim(this)">
          	</div>
            
            <label class="login_label">Mobile</label>
            <label id="errorBox4" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                <input type="tel" name="customer_phone" id="phone" value="<?php echo $l->phone; ?>" class="login_input" placeholder="Mobile" onkeyup="leftTrim(this)">
          	</div>
            <?php if($tickettype==0) {?>
            <?php }else {?>
            <label class="login_label">Choose Option for Payment</label>
        <label id="errorBox4" style="color:#ff0000;"></label>
        <div class="input-group"> <span class="input-group-addon"><i class="fa fa-dollar" aria-hidden="true"></i></span>
          <select class="form-control" name="choosepaymentoption">
            <option value="1">Stripe</option>
            <option value="2">Paypal</option>
          
          </select>
        </div>
        <?php }?>
<!--            <label class="login_label">Country</label>
            <label id="errorBox5" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-globe" aria-hidden="true"></i></span>
                <select name="country" id="country" class="login_input">
                <?php 
					$result = $data['country'] = $this->home_model->country();
				?>
                <option value="">Select Country</option>
                <?php foreach($result as $c){?>
                	<option value="<?php echo $c->fld_name;?>"><?php echo $c->fld_name;?></option>
                <?php }?>    
                </select>
          	</div>
            <label class="login_label">City</label>
            <label id="errorBox6" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                <input type="text" name="city" id="city" value="" class="login_input" placeholder="City" onkeyup="leftTrim(this)">
          	</div>
            
            <label class="login_label">Postal Code</label>
            <label id="errorBox7" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-globe" aria-hidden="true"></i></span>
                <input type="text" name="postalcode" id="postalcode" value="" class="login_input" placeholder="Postal Code" onkeyup="leftTrim(this)">
          	</div>
            
            <label class="login_label">Address</label>
            <label id="errorBox8" style="color:#ff0000;"></label>
          	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                <input type="text" name="address" id="address" value="" class="login_input" placeholder="Address" onkeyup="leftTrim(this)">
          	</div>-->
            
            <?php if($tickettype==1) {?>
            <input type="submit" class="login_btn" value="Proceed To Payment" onclick="return Validate()" id="signup">
            <?php } else {?>
            <input type="submit" class="login_btn" value="Generate Ticket" onclick="return Validate()" id="signup">
            <?php }?>
        </div>
        </form>
        <?php }?>
      <div style="padding:20px;"></div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script>
	function Submit(){
		var emailRegex = /^[A-Za-z0-9._]*\@[A-Za-z]*\.[A-Za-z]{2,5}$/;
		var formemail = $("#email").val();
		var myname = $("#myname").val();
		
		if($("#myname").val() == "" ){
			$("#myname").focus();
			$("#errorBox1").html("enter the Name");
			return false;
		}else{
			$("#errorBox1").html("");
			}
  
		if($("#email").val() == "" ){
			$("#email").focus();
			$("#errorBox2").html("enter the email");
			return false;
		}
		if(!emailRegex.test(formemail)){
			$("#email").focus();
			$("#errorBox2").html("Please enter the valid email");
			return false;
		}else{
			$("#errorBox2").html("");
			}
		
		if($("#con_email").val() == "" ){
			$("#con_email").focus();
			$("#errorBoxce").html("enter the confirm email");
			return false;
		}
		if(!emailRegex.test(formemail)){
			$("#con_email").focus();
			$("#errorBoxce").html("Please enter the valid email");
			return false;
		}else{
			$("#errorBoxce").html("");
			}
		if($("#phone").val() == "" ){
			$("#phone").focus();
			$("#errorBox4").html("enter the Phone");
			return false;
		}else{
			$("#errorBox4").html("");
			}
			
  
 }

</script> 

<script>	
function leftTrim(element){
if(element)
element.value=element.value.replace(/^\s+/,"");
}
</script>
 <script type="text/javascript">
	$(document).ready(function() {
		$("#phone").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
					 // let it happen, don't do anything
					 return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});
	});
  </script>
	<script type="text/javascript">
			function Validate() {
			var mail = document.getElementById("email").value;
			var con_mail = document.getElementById("con_email").value;
			if (mail != con_mail) {
				$("#con_email").focus();
				$("#errorBoxe").html("Email and Confirm Email Does Not match!!!");
				return false;
			}else{
				$("#errorBoxe").html("");
				}
		}
    </script>
    
    
  
 
