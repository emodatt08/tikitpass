﻿<style type="text/css">
	.stock-box {
		line-height: 30px;
	}
	#TicketReg {
    border-radius: 0;
    padding-left: 11px;
    padding-top: 3px;
}
.panel_body, .ticket_row, .passwd_info, .panel_628_share, .panel_footer, .panel_section {
    background-color: #ffffff;
    border-color: #dedede;
    color: #666666;
}
.panel_body {
    background-color: #fff;
    border-color: #d5d5d3;
    border-radius: 0 0 5px 5px;
    border-style: solid;
    border-width: 1px;
    font-size: 13px;
    line-height: 1.6em;
    overflow-wrap: break-word;
    padding: 13px 14px;
    width: auto;
}
.ticket_table td {
    border-top: 1px solid #dedede;
}
.ticket_table_head, .ticket_table_head td {
    background-color: #ffffff;
    color: #666666;
}
.ticket_table td {
    padding: 12px 5px;
    vertical-align: top;
}
.ticket_table_head {
    color: #005580;
    font-family: Helvetica,Arial,sans-serif;
    font-size: 11px;
    font-weight: 700;
    padding: 1px;
    text-transform: uppercase;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}
#ticketInfo {
    position: relative;
}

.panel_head2 h3 {
    color: #404040;
    font-size: 16px;
    font-weight: 500;
    line-height: 22px;
    padding: 0;
}
.panel_628 {
    min-width: 400px;
}
.panel_628, .panel_280 {
    border-radius: 5px;
    margin-bottom: 26px;
    width: 100%;
}.panel_628 {
    clear: both;
}
.panel_head2, .panel_footer {
    background-color: #fafafa;
    border-color: #dedede;
    color: #404040;
}
.panel_head2 {
    border-color: #d5d5d3;
    border-radius: 5px 5px 0 0;
    border-style: solid;
    border-width: 1px 1px 0;
    color: #005580;
    font-size: 16px;
    font-weight: 500;
    line-height: 22px;
    padding: 6px 11px 7px 14px;
    width: auto;
}
.panel_head2 h3 {
    color: #404040;
    font-size: 16px;
    font-weight: 500;
    line-height: 22px;
    padding: 0;
}
h3 {
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 16px;
    line-height: 1.2em;
    margin: 2px 0 0;
    padding: 0 0 8px;
}
#ticket_table .ticket_table_head td {
    border-top: 0 none;
}
#TicketReg .ticket_table_head td, #order_summary_data .ticket_table_head td {
    padding: 5px 5px 3px;
}
.panel_body, .ticket_row, .passwd_info, .panel_628_share, .panel_footer, .panel_section {
    background-color: #ffffff;
    border-color: #dedede;
    color: #666666;
}
#primary_cta, #primary_cta_disabled {
    display: block;
    margin: 15px 0 20px 5px;
}
#primary_cta a {
    background: #40E0D0;
    border: 1px solid #40E0D0;
    box-shadow: 0 1px 0 #40E0D0 inset, 0 -1px #40E0D0 inset;
    color: #fff;
    font-size: 20px;
    padding: 9px 26px;
    text-shadow: 0 1px #40E0D0;
}
.panel_footer {
    border-radius: 0 0 5px 5px;
    border-style: solid;
    border-width: 0 1px 1px;
    color: #000;
    width: auto;
}
#panel_when h2 {
    color: #666666;
    font-family: Helvetica,Arial,sans-serif;
    font-size: 1em;
    font-weight: normal;
    line-height: normal;
    margin: 0;
    padding: 0;
    text-align: left;
}
</style>
<!-- Body Content -->
<div class="content_bg">
  <div class="container">
    <!-- Breadcrumb -->
    <ol class="breadcrumb cu_breadcrumb">
      <li><a href="<?php echo base_url()?>">Home</a></li>
      <li class="active"><a href=""><?php echo $title;?></a></li>
    </ol>
    <!-- End Breadcrumb -->
    <!-- Details Panel Design start here-->
    <?php if($num_rows>0) {?>
    <?php foreach($eventdetails as $ed){ ?>
    <?php $a = $num_rows_booking;
	      $b = $num_rows_ticket_booked;
	      $c = ($a-$b);
 	?>
    <?php $afree = $num_rows_bookingfree;
	      $bfree = $num_rows_ticket_bookedfree;
	      $cfree = ($afree-$bfree);
 	?>
   	<form method="post" action="<?php echo base_url()?>checkout/book_ticket" name="checkoutform" id="checkoutform" enctype="multipart/form-data">
    <input type="hidden" name="bookingcount" id="bookingcount" value="<?php echo $c;?>">
    <input type="hidden" name="bookingcountfree" id="bookingcountfree" value="<?php echo $cfree;?>">
    <input type="hidden" name="ticketid" value="<?php echo $ed->id;?>">
    <input type="hidden" name="ticketno" value="<?php echo $ed->ticket_no;?>">
    <input type="hidden" name="eventid" value="<?php echo $ed->event_id;?>">
    <input type="hidden" name="eventname" value="<?php echo $ed->event_name;?>">
    <input type="hidden" name="eventdate" value="<?=date('Y-m-d g.i a', strtotime($ed->start_date_time)) ?>">
    <div class="single-product">
      <div class='col-md-12'>
        <div class="detail-block">
          <div class="row  wow fadeInUp">
          
          <div class='col-sm-6 col-md-7 product-info-block'>
              <div class="product-info">
                <h1 class="name"><?php echo $ed->event_name ?></h1>
                <!-- /.rating-reviews -->
                <div class="stock-container info-container m-t-10">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="stock-box"> <span class="label2" style="font-size:15px;">Event Start :</span> </div>
                    </div>
                    <div class="col-sm-9">
                      <div class="stock-box"> <span class="value2"><?=date('Y-m-d g.i a', strtotime($ed->start_date_time)) ?> </span></div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="stock-box"> <span class="label2" style="font-size:15px;">Event End :</span> </div>
                    </div>
                    <div class="col-sm-9">
                      <div class="stock-box"> <span class="value2"><?=date('Y-m-d g.i a', strtotime($ed->end_date_time)) ?></span></div>
                    </div>
                  </div>
                  <!-- /.row -->
                </div>
                <div class="stock-container info-container m-t-10">

                <!-- /.stock-container -->
                <div class="description-container m-t-20" style="margin-top:10px; line-height:20px;"> <?php echo $ed->event_description ?> </div>
                <!-- /.description-container -->
                <div class="price-container info-container m-t-20">
                  
                  <div class="row">
                  <?php 
				  	foreach($ticketdet as $td){ 
				  ?>
                    <div class="col-sm-3">
                    <input type="radio"  name="ticket_type" value="1" id="pt"> Paid Ticket
                      <!--<div class="price-box" id="ptd" style="display:none;"> <span class="price" style="color:#40e0d0;font-size: 20px; margin-left: 24px;">$<?php echo $td->ticket_price?></span> </div>-->
                      <label id="errorBox" style="color:#ff0000;"></label>
                    </div>
					<?php }?>
                    <div class="col-sm-3">
                    <input type="radio"  name="ticket_type" value="0" id="ft" checked="checked"> Free Ticket
                     <!-- <div class="price-box" id="ftd"> <span class="price" style="color:#40e0d0;">$0</span> </div>
                      <label id="errorBox" style="color:#ff0000;"></label>-->
                    </div>
                     
                   </div>
                   
                 
                  <!-- /.row -->
                </div>
                <!--Paid Ticket-->
                <?php foreach($ticketdet as $td){?>
             <div id="paid" class="panel_628" style="position: relative; display:none;">   
                <div class="l-mar-stack">
                	<div id="ticketInfo" class="panel_head2 l-mar-top-3">
                		<h3>Ticket Information</h3>
                	</div>
                </div>
            <div id="TicketReg" class="panel_body">
              <table id="ticket_table" class="ticket_table" data-automation="ticket_table" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr class="ticket_table_head">
                    <td width="40%" nowrap="nowrap"> Details </td>
                    <td> Type </td>
                    <td> Price </td>
                    <td></td>
                    <td width="70" align="right">Quantity</td>
                  </tr>
                  <tr class="ticket_row">
                    <td class="ticket_type_name">
                      <div id="descriptionDiv53726975" style="font-size: 11px; line-height: 12px; margin: 5px 0 0 0; float:left;"><?php echo $td->ticket_description; ?></div></td>
                    
                    <td class="price_td" nowrap="nowrap">
                      Paid </td>
                      <td class="price_td" nowrap="nowrap"><input name="ticketprice" value="<?php echo $td->ticket_price; ?>" type="hidden">
                      $<?php echo $td->ticket_price; ?></td>
                    <td class="fee_td" nowrap="nowrap"></td>
                    <td nowrap="nowrap" align="right"><label class="is-hidden-accessible" for="quant_53726975_None">Ticket Quantity Select</label>
                      <select id="ticketquantitypaid" class="ticket_table_select" name="ticketquantitypaid" style="padding: 4px;" onchange="return Validate()">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                      </select><br>
                      <label id="errorBoxer" style="color:#F00;"></label>
                      </td>
                  </tr>
                </tbody>
              </table>
            </div>
            
        <div id="OrderReg" class="panel_footer" style="text-align: right;  padding: 10px 10px 63px;">
        <table id="freeButton" style="display: block;" align="right">
          <tbody>
            <tr>
              <td></td>
              <td><span id="primary_cta" class="button_css"> <input class="btn btn-info" type="submit" name="submit" id="submitpaid" value="PROCEED TO CHECKOUT" onclick="return Validate()"> </span></td>
            </tr>
          </tbody>
        </table>
        <div id="paidButton" style="display: none;" data-automation="paidButton">
          <div class="clearfloat"></div>
        </div>
            
         </div>
                
                
                <!-- /.price-container -->
                
              </div>
              <?php }?>
              <!--Paid Ticket-->
              <!--Free Ticket-->
               <?php foreach($ticketdetails as $td){?>
             <div id="free" class="panel_628" style="position: relative;">   
                <div class="l-mar-stack">
                	<div id="ticketInfo" class="panel_head2 l-mar-top-3">
                		<h3>Ticket Information</h3>
                	</div>
                </div>
            <div id="TicketReg" class="panel_body">
              <table id="ticket_table" class="ticket_table" data-automation="ticket_table" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr class="ticket_table_head">
                    <td width="40%" nowrap="nowrap"> Details </td>
                    <td> Type </td>
                    <td></td>
                    <td width="70" align="right">Quantity</td>
                  </tr>
                  <tr class="ticket_row">
                    <td class="ticket_type_name">
                      <div id="descriptionDiv53726975" style="font-size: 11px; line-height: 12px; margin: 5px 0 0 0; float:left;"><?php echo $td->ticket_description; ?></div></td>
                    
                    <td class="price_td" nowrap="nowrap">
                      Free </td>
                      
                    <td class="fee_td" nowrap="nowrap"></td>
                    <td nowrap="nowrap" align="right"><label class="is-hidden-accessible" for="quant_53726975_None">Ticket Quantity Select</label>
                      <select id="ticketquantityfree" class="ticket_table_select" name="ticketquantityfree" style="padding: 4px;" onchange="return Validatefree()">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                      </select><br>
                      <label id="errorBoxfree" style="color:#936;"></label>
                      </td>
                  </tr>
                </tbody>
              </table>
            </div>
            
        <div id="OrderReg" class="panel_footer" style="text-align: right;  padding: 10px 10px 63px;">
        <table id="freeButton" style="display: block;" align="right">
          <tbody>
            <tr>
              <td></td>
              <td><span id="primary_cta" class="button_css"> <input <?php if($cfree<1){?> disabled value="No Ticket Available" <?php }?> class="btn btn-info" type="submit" name="submit" id="submitfree" value="PROCEED TO CHECKOUT" onclick="return Validate()"> </span></td>
            </tr>
          </tbody>
        </table>
        <div id="paidButton" style="display: none;" data-automation="paidButton">
          <div class="clearfloat"></div>
        </div>
            
         </div>
                
                
                <!-- /.price-container -->
                
              </div>
              <?php }?>
              <!--Free Ticket-->
            </div>
          </div>
        </div>
          
            <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
              <div class="product-item-holder size-big single-product-gallery small-gallery">
                <div id="owl-single-product">
                  <div class="single-product-gallery-item" id="slide1"> <a data-lightbox="image-1" data-title="Gallery" href=""> 
                  <?php if($ed->event_image=="") {?>
                  <img class="img-responsive" src="<?php echo base_url()?>uploads/nopic.png" data-echo="<?php echo base_url()?>uploads/nopic.png" /> 
                  <?php }else {?>
                  <img class="img-responsive" src="<?php echo base_url()?>uploads/event/<?php echo $ed->event_image ?>" data-echo="<?php echo base_url()?>uploads/event/<?php echo $ed->event_image ?>" /> 
					<?php }?>
                  </a> </div>
                  <!-- /.single-product-gallery-item -->
                </div>
                <!-- /.single-product-slider -->
                <!-- /.gallery-thumbs -->
              </div>
              <!-- /.single-product-gallery -->
              
        <div id="col_280">
          <div class="panel_280">
            <div class="panel_head2">
              <h3> When & Where </h3>
            </div>
            <div id="panel_when" class="panel_body">
            <div >
            	<?php
				/*	$addr = str_replace(" ",addslashes($ed->location));  
					
				$address=$addr;
				$url="https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=AIzaSyB-YeP4y7BsbFY6rnrLp5UgSzyONI_a6Q0";
				$ch = curl_init($url);
				
				$fp = fopen("map.json", "w");
				curl_setopt($ch, CURLOPT_FILE, $fp);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				$data = curl_exec($ch);
				curl_close($ch);
				fclose($fp);
				
				$json_string = 'http://site.goigi.me/bunorrenergy/map.json';
				$jsondata = file_get_contents($json_string);
				$obj = json_decode($jsondata,true);
				
				$lat = $obj[results][0][geometry][location][lat];
				$long = $obj[results][0][geometry][location][lng];*/
?>
            </div>
              <h2 class="location" style="margin-top: 3px;"> <br>
                <b><?php echo $ed->location ?></b> <br>
              </h2>
              <br>
	            </div>
          </div>
        </div>


            </div>
            <!-- /.gallery-holder -->
            
      </div>
    </div>
  </div>
  </div>
  	</form>
    <?php }?>
    <?php }else { ?>
    <div style="color:#F00; background-color:#CCC; padding:100px; margin:50px;"><h1>Sorry.... No Ticket Available!!!</h1></div>
    <?php }?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
		$("#pt").click(function(){
		$("#ptq").show();
		$("#ftq").hide();
		$("#paid").show();
		$("#free").hide();
		
    });	
		$("#ft").click(function(){
		$("#ftq").show();
		$("#ptq").hide();
		$("#free").show();
		$("#paid").hide();
		
    });
});
</script>

<script type="text/javascript">
		function Validate() {
			
		}
    </script>
	<script type="text/javascript">
		function Validate() {
			var bookingcount = document.getElementById("bookingcount").value;
			//alert (bookingcount);
			var ticketquantitypaid = document.getElementById("ticketquantitypaid").value;
			//alert (ticketquantitypaid);
			var res = (bookingcount - ticketquantitypaid);
			//alert(res);
			if (res < 0) {
				//alert("Error!!");
				$("#ticketquantitypaid").focus();
				$("#errorBoxer").html("The no. of ticket you have entered is not available");
				//$('input[type="submit"]').prop('disabled', true);
				$('#submitpaid').attr('disabled', true);
				return false;
			}else{
				$("#errorBoxer").html("");
				$('#submitpaid').attr('disabled', false);
				}
		}
    </script> 
    	<script type="text/javascript">
		function Validatefree() {
			var bookingcountfree = document.getElementById("bookingcountfree").value;
			//alert (bookingcountfree);
			var ticketquantityfree = document.getElementById("ticketquantityfree").value;
			//alert (ticketquantityfree);
			var res = (bookingcountfree - ticketquantityfree);
			//alert(res);
			if (res < 0) {
				//alert("Error!!");
				$("#ticketquantityfree").focus();
				$("#errorBoxfree").html("The no. of ticket you have entered is not available");
				//$('input[type="submit"]').prop('disabled', true);
				$('#submitfree').attr('disabled', true);
				return false;
			}else{
				$("#errorBoxfree").html("");
				$('#submitfree').attr('disabled', false);
				}
		}
    </script>    