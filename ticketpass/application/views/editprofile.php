<div class="content_bg">
  <div class="login-bg">
    <div class="login_sec">
      <div style="padding:20px;"></div>
      <?php foreach($info as $in){?>
      <div class="col-md-12">
        <div class="col-md-3">
                        <strong>
                        <?php 
                        if($this->session->flashdata('propic_change') !=""){
						?>
                        <p style="color:#0F0; margin-left: 25px;">
                        <?php 	
                        echo $this->session->flashdata('propic_change');
                        ?>
						</p>
                        <?php
                        } 
                        ?>
                        </strong>
                    	<div class="box" align="center">
                        <form method="post" action="<?php echo base_url();?>profile/changeprofileimage" id="uploadFileForm" enctype="multipart/form-data">
            			<input type="hidden" name="oldimg" value="<?php echo $in->imagename?>" />
                        <?php if($in->imagename == '') {?>
                        <img src="<?php echo base_url()?>uploads/nopic.png" style="height: 170px;"/>
                        <?php } else {?>
                        <img src="<?php echo base_url()?>uploads/profileimage/<?php echo $in->imagename?>" style="height: 170px;"/>
                        <?php }?>
                        <div class="fileUpload btn btn-primary" align="center">
                        <span>Upload</span>
                       	<input type="file" class="upload" name="userfile" id="file-2"/>
                        </div>
                        </form> 
                        <?php $this->load->view('sidebar');?>
                    </div>
                    </div>
        <div class="col-md-9">
          <div class="box">
          
          <?php if($this->session->flashdata('edit_pro')!=''){?>
			<div class="alert alert-success text-center"><b style="color:#0C0;"><?php echo @$this->session->flashdata('edit_pro');?></b></div>
	  	<?php }?>
            <form class="" action="<?php echo base_url();?>profile/edit_pro" method="post" name="signupform" onSubmit="return Submit()">
            <input type="hidden" name="uid" value="<?php echo $in->uid; ?>">
              <div class="login_box">
                <div style="text-align:center;"> <strong>
                  <?php 
                    if($this->session->flashdata('success') !=""){
                        echo $this->session->flashdata('success');
                    } 
                ?>
                  </strong> </div>
                <label class="login_label">Name</label>
                <label id="errorBox1" style="color:#ff0000;"></label>
                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                  <input type="text" name="name" id="myname" value="<?php echo $in->name; ?>" class="new_input" placeholder="Name" onkeyup="leftTrim(this)">
                </div>
                <label class="login_label">Email</label>
                <label id="errorBox2" style="color:#ff0000;"></label>
                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                  <input type="email" name="email" id="email" value="<?php echo $in->email; ?>" class="new_input" placeholder="Email" readonly="readonly" style="background-color:#CCC;">
                </div>
                <label class="login_label">Mobile</label>
                <label id="errorBox4" style="color:#ff0000;"></label>
                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                  <input type="tel" name="phone" id="phone" value="<?php echo $in->phone; ?>" class="new_input" placeholder="Mobile" onkeyup="leftTrim(this)">
                </div>
                <label class="login_label">Address</label>
                <label id="errorBox5" style="color:#ff0000;"></label>
                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i>
</span>
                  <input type="text" name="adrs" id="address" value="<?php echo $in->adrs; ?>" class="new_input" placeholder="Address" onkeyup="leftTrim(this)">
                </div>
                <input type="submit" class="submit_btn_new" value="Edit Account" onclick="return Validate()" id="signup">
                 </div>
            </form>
          <?php } ?>
          </div>
        </div>
      </div>
      <div style="padding:20px;"></div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
	function Submit(){
		var emailRegex = /^[A-Za-z0-9._]*\@[A-Za-z]*\.[A-Za-z]{2,5}$/;
		var formemail = $("#email").val();
		var myname = $("#myname").val();
		
		if($("#myname").val() == "" ){
			$("#myname").focus();
			$("#errorBox1").html("enter the Name");
			return false;
		}
  
		if($("#email").val() == "" ){
			$("#email").focus();
			$("#errorBox2").html("enter the email");
			return false;
		}
		if(!emailRegex.test(formemail)){
			$("#email").focus();
			$("#errorBox2").html("Please enter the valid email");
			return false;
		}
  
		if($("#username").val() == "" ){
			$("#username").focus();
			$("#errorBox3").html("enter the UserName");
			return false;
		}
  
		if($("#phone").val() == "" ){
			$("#phone").focus();
			$("#errorBox4").html("enter the Phone Number");
			return false;
		}
  
		if($("#password").val() == "" ){
			$("#password").focus();
			$("#errorBox5").html("enter the Password");
			return false;
		}
  
		if($("#conf_password").val() == "" ){
			$("#conf_password").focus();
			$("#errorBox6").html("enter the Confirm Password");
			return false;
		}
  
		if($("#user_type").val() == "" ){
			$("#user_type").focus();
			$("#errorBox7").html("enter the Account type");
			return false;
		}
 }

</script>
<script>	
function leftTrim(element){
if(element)
element.value=element.value.replace(/^\s+/,"");
}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#phone").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
					 // let it happen, don't do anything
					 return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});
	});
  </script>
<script type="text/javascript">
		function Validate() {
			var password = document.getElementById("password").value;
			var confirmPassword = document.getElementById("conf_password").value;
			if (password != confirmPassword) {
				$("#conf_password").focus();
				$("#errorBox8").html("Password and Confirm password Does Not match!!!");
				return false;
			}
		}
    </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#file-2").change(function() {
		$("#uploadFileForm").submit();
	});

});
</script>
