<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Tikitpass | <?php echo $title;?></title>

    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,700i,900" rel="stylesheet"> 
    <link href="<?php echo base_url(); ?>usercss/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>usercss/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>usercss/jquery.bxslider.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>usercss/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>usercss/media.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <![endif]-->
		<style>
        .blink_me {
        animation: blinker 3s linear infinite;
        }
        
        @keyframes blinker {  
        50% { opacity: 0; }
        }
        </style>
  </head>
  <body>
  <!-- Header -->
  <div class="hearder_top">
  	<div class="container">
    	<div class="row">
        	<div class="col-sm-4">
            	<h1 class="logo"><a href="<?php echo base_url()?>">Tikitpass</a></h1>
            </div>
            <div class="col-sm-8">
            	<div class="top_menu">
                	<ul style="display:inline-block;">
                    	<!--<li><a href="#">My Tickitpass</a></li>
                        <li><a href="#">Retail Outlets</a></li>-->
                        <li class="blink_me">The Most Cost-Effective and Convenient Way to Buy Tickets</li>
                        <!--<li><a href="<?php echo base_url();?>organizerlogin">Log In</a></li>
                        <li><a href="<?php echo base_url()?>signuporganisation">Sign Up</a></li>
                        <li><a href="#">Help</a></li>-->
                    </ul>
                	<ul style="display:inline-block;">
                    	<!--<li><a href="#">My Tickitpass</a></li>
                        <li><a href="#">Retail Outlets</a></li>-->
                        
                        <?php if(!$this->session->userdata('is_userlogged_id')){?>
                        <li><a href="<?php echo base_url();?>login">Log In</a></li>
                        <li><a href="<?php echo base_url()?>signup">Sign Up</a></li>
                        <?php }else{?>
                        <li><a href="<?php echo base_url();?>profile/myprofile">Dashboard</a></li>
                        <li><a href="<?php echo base_url()?>login/logout">Logout</a></li>
                        <?php }?>
                        <li><a href="<?php echo base_url()?>pages/help">Help</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="chlo_searchbox">
                    <div class="serach_box">
                    	<input type="submit" class="serach_btn" value="search">
                    	<input type="text" class="serach_input">
                    </div>
                    <!--<select class="cu_select">
                    	<option>Change location</option>
                        <option>Ghana</option>
                        <option>Select 1</option>
                        <option>Select 2</option>
                    </select>-->
                </div>
            </div>
        </div>
    </div>
  </div>
  <!-- End Header -->
  
  <!-- Navbar -->
  	<div class="cu_navbar">
    	<div class="container">
        <div class="row">
        	<nav class="navbar navbar-default">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!--<a class="navbar-brand" href="#">Brand</a>-->
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class=""><a href="<?php echo base_url()?>">HOME <span class="sr-only">(current)</span></a></li>
        <li><a href="<?php echo base_url()?>event">EVENTS</a></li>
       <!-- <li><a href="#">BUS TICKETS</a></li>-->
        <li><a href="#">AIRLINE TICKETS</a></li>
        <li><a href="<?php echo base_url()?>pages/whatwedo">WHAT WE DO</a></li>
       <!-- <li><a href="#">ABOUT</a></li>-->
        <li><a href="<?php echo base_url()?>contact">CONTACT</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      </ul>
    </div>
</nav>
        </div>
        </div>
    </div>
  <!-- End Navbar -->