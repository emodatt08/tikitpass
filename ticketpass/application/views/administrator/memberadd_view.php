<?php //$this->load->view ('header');?>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript">
$(function() {
setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
$('#btnclick').click(function() {
$('#testdiv').show();
setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
})
})
</script>
<!-- BEGIN CONTAINER -->

<div class="page-container">
  <div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
      <?php $this->load->view ('administrator/sidebar');?>
      <!-- END SIDEBAR MENU -->
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
  </div>
  <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>administrator panel</span> </li>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions <i class="fa fa-angle-down"></i> </button>
            <ul class="dropdown-menu pull-right" role="menu">
              <li> <a href="#"> <i class="icon-bell"></i> Action</a> </li>
              <li> <a href="#"> <i class="icon-shield"></i> Another action</a> </li>
              <li> <a href="#"> <i class="icon-user"></i> Something else here</a> </li>
              <li class="divider"> </li>
              <li> <a href="#"> <i class="icon-bag"></i> Separated link</a> </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- END PAGE BAR -->
      <!-- BEGIN PAGE TITLE-->
      <!-- END PAGE TITLE-->
      <!-- END PAGE HEADER-->
      <?php if (isset($success_msg)) { echo $success_msg; } ?>
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Add banner</div>
                    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form">
                    <form action="<?php echo base_url().'administrator/memberdata/add_member' ?>" class="form-horizontal form-bordered" method="post" enctype="multipart/form-data" onsubmit="return Submit();">
                      <div class="form-body">
                        <div class="form-group">
                          <label class="control-label col-md-3"> First Name</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'first_name', 'name' => 'first_name','class'=>'form-control', 'onkeyup'=> 'leftTrim(this)')); ?> <?php echo form_error('cms_heading'); ?>
                            <label id="errorBox"></label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3"> Last Name</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'last_name', 'name' => 'last_name','class'=>'form-control', 'onkeyup'=> 'leftTrim(this)')); ?> <?php echo form_error('last_name'); ?>
                            <label id="errorBox1"></label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Date Of Birth</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'dob', 'name' => 'dob','class'=>'form-control', 'onkeyup'=> 'leftTrim(this)')); ?> <?php echo form_error('dob'); ?>
                            <label id="errorBox1"></label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Username</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'username', 'name' => 'username','class'=>'form-control', 'onkeyup'=> 'leftTrim(this)')); ?> <?php echo form_error('username'); ?>
                            <label id="errorBox4"></label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Password</label>
                          <div class="col-md-8"> <?php echo form_password(array('id' => 'password', 'name' => 'password','class'=>'form-control', 'onkeyup'=> 'leftTrim(this)')); ?> <?php echo form_error('password'); ?>
                            <label id="errorBox5"></label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Email</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'useremail', 'name' => 'useremail','class'=>'form-control', 'onkeyup'=> 'leftTrim(this)')); ?> <?php echo form_error('useremail'); ?>
                            <label id="errorBox4"></label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Gender</label>
                          <div class="col-md-8"> <?php echo form_label('Male', 'male') . form_radio('gender', 'M', '', 'id=male'); ?><?php echo form_label('Female', 'female') . form_radio('gender', 'F', '', 'id=female'); ?> </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Address</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'address', 'name' => 'address','class'=>'form-control', 'onkeyup'=> 'leftTrim(this)')); ?> <?php echo form_error('address'); ?>
                            <label id="errorBox6"></label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">City</label>
                          <div class="col-md-8"> <?php echo form_input(array('id' => 'city', 'name' => 'city','class'=>'form-control', 'onkeyup'=> 'leftTrim(this)')); ?> <?php echo form_error('city'); ?>
                            <label id="errorBox7"></label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Member Avatar</label>
                          <div class="col-md-8">
                            <?php
                    $file = array('type' => 'file','name' => 'userfile', 'id'=>'chkimg','onchange'=>'checkFile(this);');
					echo form_input($file);
                    ?>
                            <label>Please select jpg, png, jpeg or gif file.</label>
                            <label id="errorBox1"></label>
                            <!-- <input type='text' class="form-control datepicker" id="example1" value="" name="dateofbirth">-->
                          </div>
                        </div>
                      </div>
                      <div class="form-actions">
                        <div class="row">
                          <div class="col-md-offset-3 col-md-9">
                            <!--<button type="submit" class="btn red" name="submit" value="Submit"> <i class="fa fa-check"></i> Submit</button>-->
                            <?php echo form_submit(array('id' => 'submit', 'value' => 'Submit' ,'class'=>'btn red')); ?> <a href="<?php echo base_url(); ?>administrator/gallery/show_gallery">
                            <button type="button" class="btn default">Cancel</button>
                           
                            </a> </div>
                        </div>
                      </div>
                    </form>
                    <script>
            $('INPUT[type="file"]').change(function () {
				var ext = this.value.match(/\.(.+)$/)[1];
				switch (ext) {
					case 'jpg':
					case 'jpeg':
					case 'png':
					case 'gif':
					$('#banner').attr('disabled', false);
					break;
					default:
					alert('This is not an allowed file type.');
					this.value = '';
					return false;
					}
            });
            </script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js"></script>
        	<script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
    
			<script type="text/javascript">
				$(document).ready(function () {
					alert("sdfsd");
					$('#example1').datepicker({
					format: "dd-mm-yyyy"
					}); 
				});
           </script>
                    <!-- END FORM-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
