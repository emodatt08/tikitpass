<style>
input[type="radio"]{
	margin-left:-6px !important;
	}
.login-heading{
	color:#fff;
	}
</style>
<body class="login">
        <div class="menu-toggler sidebar-toggler"></div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <div class="logo" style="padding-bottom:0;">
        <!--<h3 style="color:#ffffff;text-shadow:2px 2px 3px #000; font-size:23px; font-weight:300;"><?//=strtoupper(PROJECT_NAME)?></h3>-->
        <h3 class="login-heading">Ticketpass</h3>
            <a><!--<img src="assets/pages/img/logo-big.png" alt="" />--> <?php //echo $title;?></a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            
   	
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
           <?php echo form_open('administrator/main/login_validation'); ?>
                <h3 class="form-title font-green"><i class="fa fa-user"></i> Login to your account</h3>
                <!--<div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>-->
                 <?php if(validation_errors()){?>
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo validation_errors(); ?>  </span>
                </div>
                <?php }?>
                <?php 
				if(@$error!=''){
				?>
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo @$error; ?>  </span>
                </div>
                <?php }?>
                
                <?php
					if($this->session->flashdata('logerror')!=''){
					?>	
						 <div class="alert alert-danger">
							 <button class="close" data-close="alert"></button>
							<span> <?php echo $this->session->flashdata('logerror'); ?> </span>
						</div>
					 <?php   
					}
					?>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                <div class="form-actions">
                    
                   <div class="col-sm-12">
                            <div class="row">
                            	<div class="col-sm-6">
                   			<label><input type="radio" name="log_type" value="ad" onClick="OptionSelected()" /> Master Admin </label>
                            <label><input type="radio" name="log_type" value="csa" onClick="OptionSelected()" /> Services Agent </label>
                            	</div>
                                <div class="col-sm-6">
                            <label><input type="radio" name="log_type" value="csm" onClick="OptionSelected()"/> Service Manager </label>
                            <label><input type="radio" name="log_type" value="sa" onClick="OptionSelected()"/> System Admin </label>
                            	</div>
                            </div>
                            </div>
                   <div style="clear:both"></div>
                    
                </div>
                
                <div class="create-account">
                	<div class="form-group2" style="text-align:left; margin-left:12%">
                        <input type="submit" class="btn green uppercase" value="Login" id="submitButton">
                    </div>
                    <script src="http://code.jquery.com/jquery-1.7.1.js"></script>
                 <script type="text/javascript">
    				function OptionSelected() {
						document.getElementById("submitButton").disabled = false;
						var selected = $('input[name="log_type"]:checked').val();
						if (selected == "4") {
							document.getElementById("submitButton").disabled = true;;
						}
					}

					$(document).ready(function () {
						$("#submitButton").click(function (e) {
							if (!$('input[name="log_type"]').is(':checked')) {
								return false;
							}
							else {
								return true;
							}
						});
					});
  				</script>
                </div>
          <?php echo form_close(); ?>
            
            <!-- END REGISTRATION FORM -->
        </div>
      
