<?php //$this->load->view ('header');?>
<?php 
 $usertype = $this->session->userdata['logged']['log_type'];
 if($usertype=='csa'){
	 redirect('administrator/home');
 }
 ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
      <?php $this->load->view ('administrator/sidebar');?>
    </div>
  </div>
  <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <!-- BEGIN PAGE BAR -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="index.html">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>administrator Panel</span> </li>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions <i class="fa fa-angle-down"></i> </button>
            <ul class="dropdown-menu pull-right" role="menu">
              <li> <a href="#"> <i class="icon-bell"></i> Action</a> </li>
              <li> <a href="#"> <i class="icon-shield"></i> Another action</a> </li>
              <li> <a href="#"> <i class="icon-user"></i> Something else here</a> </li>
              <li class="divider"> </li>
              <li> <a href="#"> <i class="icon-bag"></i> Separated link</a> </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- END PAGE HEADER-->
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Banner edit</div>
                    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php foreach($eorg as $i){?>
                    <form method="post" class="form-horizontal form-bordered" action="<?php echo base_url()?>administrator/organizer/edit_organizer" enctype="multipart/form-data">
                      <div class="form-body">
                        <input type="hidden" name="organizer_id" value="<?=$i->organizer_id;?>">
                        <input type="hidden" name="organizer_avatar" value="<?php echo $i->organizer_avatar;?>">
                        
                        <!--<div class="form-group">
                          <label class="control-label col-md-3">Image Upload</label>
                          <div class="col-md-7">
                            <?php
                    		$file = array('type' => 'file','name' => 'userfile','onchange' => 'readURL(this);');
							echo form_input($file);
                    		?>
                          </div>
                        </div>-->
                        
                        <div class="form-group last">
                                                <label class="control-label col-md-3">Profile Image</label>
                                                <div class="col-md-9">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <?php if($i->organizer_avatar==''){ ?>
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                        <?php }else{?> 
                                                        	<img src="<?php echo base_url()?>/uploads/organizer/<?php echo $i->organizer_avatar;?>" alt="" /> 
                                                        <?php }?>      
                                                            </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <!--<input type="file" name="...">-->
																<?php
                                                                $file = array('type' => 'file','name' => 'userfile','onchange' => 'readURL(this);');
                                                                echo form_input($file);
                                                                ?>
                                                            </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix margin-top-10">
                                                        <span class="label label-danger">Format</span> jpg, jpeg, png, gif </div>
                                                </div>
                                            </div>
                        
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Organizer Name</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="organizer_name" value="<?php echo $i->organizer_name;?>" name="organizer_name">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Organizer Phone</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="organizer_phone" value="<?php echo $i->organizer_phone;?>" name="organizer_phone">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Organizer Email</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="organizer_email" value="<?php echo $i->organizer_email;?>" name="organizer_email">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Organizer Address</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="banner_heading" value="<?php echo $i->organizer_address;?>" name="organizer_address">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Organizer Category</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="organizer_category" value="<?php echo $i->organizer_category;?>" name="organizer_category">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Organiser Website</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="organiser_website" value="<?php echo $i->organiser_website;?>" name="organiser_website">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Status</label>
                          <div class="col-md-5">
                            <select name="status" class="form-control">
                            	<option value="1" <?php if($i->status==1){?> selected="selected" <?php }?>>Active</option>
                                <option value="0" <?php if($i->status==0){?> selected="selected" <?php }?> <?php ?>>Inactive</option>
                            </select>
                          </div>
                        </div>
                        
                        
                      </div>
                      <div class="form-actions">
                        <div class="row">
                          <div class="col-md-offset-3 col-md-9">
                            <!--<button type="submit" class="btn red" name="submit" value="Submit"> <i class="fa fa-check"></i> Submit</button>-->
                            <input type="submit" class="btn red" id="submit" value="Submit" name="update">
                            <button class="btn default" type="button">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </form>
                    <?php }?>
                    <!-- END FORM-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
