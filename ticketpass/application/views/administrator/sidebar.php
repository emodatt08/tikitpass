<?php 
$nl=$this->uri->segment(2); 
$nl2=$this->uri->segment(3); 
$nl3=$this->uri->segment(4);
$usertype = $this->session->userdata['logged']['log_type'];
?>
<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
  <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
  <li class="sidebar-toggler-wrapper hide"> 
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="sidebar-toggler"> </div>
    <!-- END SIDEBAR TOGGLER BUTTON --> 
  </li>
  <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
  <li class="sidebar-search-wrapper"> 
    <!-- END RESPONSIVE QUICK SEARCH FORM --> 
  </li>
  <li class="nav-item start "><a href="<?php echo base_url()?>administrator/home" class="nav-link"><span class="title">View Dashboard </span> </a>
    <ul class="sub-menu">
      <li class="nav-item start "><a href="index.html" class="nav-link "><i class="icon-bar-chart"></i> <span class="title">View Dashboard</span> </a> </li>
    </ul>
  </li>
  <li class="heading">
    <h3 class="uppercase">Features</h3>
  </li>
  <!--Supratim Sen (CMS Section Start- 20/04/2016)-->
 <?php if($usertype!='csa' && $usertype!='csm'){?>
  <li class="nav-item <?php if($nl=="cms"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">CMS Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="cms" && $nl2!="view_addcms" ){?>active open<?php }?>"> <a href="<?php echo base_url()?>administrator/cms/show_cms" class="nav-link "> <span class="title">Manage CMS Page</span> </a> </li>
     </ul>
  </li>
  <?php }?>
  <?php if($usertype!='csa' && $usertype!='csm'){?>
  <li class="nav-item <?php if($nl=="event" || $nl=="ticket"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Event management</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">]
    
       <li class="nav-item  <?php if($nl3=="addtypeview"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/event/addtypeview" class="nav-link "> <span class="title">Add Type</span> </a> </li>
      <li class="nav-item  <?php if($nl3=="showtype"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/event/showtype" class="nav-link "> <span class="title">Type List</span> </a> </li>
      
      <li class="nav-item  <?php if($nl3=="addcategoryview"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/event/addcategoryview" class="nav-link "> <span class="title">Add Category</span> </a> </li>
      <li class="nav-item  <?php if($nl3=="showcategory"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/event/showcategory" class="nav-link "> <span class="title">Category List</span> </a> </li>
      
      <li class="nav-item  <?php if($nl2=="addform"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/event/addeventview" class="nav-link "> <span class="title">Add Event</span> </a> </li>
       <li class="nav-item  <?php if($nl2=="addform"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/event/showevent" class="nav-link "> <span class="title">Show Event</span> </a> </li>
       
    </ul>
  </li>
  <?php }?>
  
   <!--User Management[18-10-2016]-->
	<?php if($usertype!='csa' && $usertype!='csm'){?>
    <li class="nav-item <?php if($nl=="users"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-diamond"></i> <span class="title">User Management</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
    
    <li class="nav-item  <?php if($nl2=="users_add_form"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/users/users_add_form" class="nav-link "> <span class="title">Add User</span> </a> </li>
    
      <li class="nav-item  <?php if($nl=="show_users"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/users/show_users" class="nav-link "> <span class="title">Manage User</span> </a> </li>
    
    </ul>
    </li>
    <?php }?>
    <!--User Management[18-10-2016]-->
  
  <li class="nav-item <?php if($nl=="booking"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Booking Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_booking"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/booking/show_booking" class="nav-link "> <span class="title">Manage Booking</span> </a> </li>
    </ul>
  </li>
  <?php if($usertype!='csa'){?>
  <li class="nav-item <?php if($nl=="organizer"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Organizer Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl3=="showorganizer"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/organizer/showorganizer" class="nav-link "> <span class="title">Organizer List</span> </a> </li>
      
      <li class="nav-item  <?php if($nl3=="addorganizer"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/organizer/addorganizer" class="nav-link "> <span class="title">Add Organizer</span> </a> </li>
    </ul>
  </li>
  <?php }?>
  
  <!--Supratim Sen (CMS Section End- 20/04/2016)-->
   <?php if($usertype!='csa' && $usertype!='csm'){?>
  <li class="nav-item  <?php if($nl=="banner"){?>active open<?php  }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-diamond"></i> <span class="title">Banner Management</span> <span class="arrow"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item <?php if($nl2 == "Add Banner"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>administrator/banner/addbanner" class="nav-link "> <span class="title">Add Banner</span> </a> </li>
      <li class="nav-item <?php if($nl2 == "Show Banner"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>administrator/banner/showbanner" class="nav-link "> <span class="title">Show Banner</span> </a> </li>
    </ul>
  </li>
  <?php }?>
   <?php if($usertype!='csa' && $usertype!='csm'){?>
  <li class="nav-item <?php if($nl=="gallery"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Gallery Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_gallery"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/gallery/show_gallery" class="nav-link "> <span class="title">Manage Gallery</span> </a> </li>
      <li class="nav-item  <?php if($nl2=="addform"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/gallery/addform" class="nav-link "> <span class="title">Add Gallery</span> </a> </li>
    </ul>
  </li>
  <?php }?>
    <!--Ads Management[14-10-2016]-->
     <?php if($usertype!='csa' && $usertype!='csm'){?>
  <li class="nav-item  <?php if($nl=="ad"){?>active open<?php  }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-diamond"></i> <span class="title">Ads Management</span> <span class="arrow"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item <?php if($nl2 == "Add Ads"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>administrator/ad/addad" class="nav-link "> <span class="title">Add Ads</span> </a> </li>
      <li class="nav-item <?php if($nl2 == "Show Ads"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>administrator/ad/showad" class="nav-link "> <span class="title">Show Ads</span> </a> </li>
    </ul>
  </li>
  <?php }?>
  <!--Ads Management[14-10-2016]-->
   <?php if($usertype!='csa' && $usertype!='csm'){?>
  <li class="nav-item <?php if($nl=="news"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">News Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
    <li class="nav-item  <?php if($nl2=="news_add_form"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/news/news_add_form" class="nav-link "> <span class="title">Add News</span> </a> </li>
      <li class="nav-item  <?php if($nl=="show_news"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/news/show_news" class="nav-link "> <span class="title">Manage News</span> </a> </li>
    </ul>
  </li>
  <?php }?>
  <li class="nav-item <?php if($nl=="contact"){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Contact Mgmt</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_contact"  ){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/contact/show_contact" class="nav-link "> <span class="title">Manage Contact</span> </a> </li>
    </ul>
  </li> 
   <?php if($usertype!='csa'){?>
   <li class="nav-item <?php if($nl=="adminprofile" || $nl=="settings" ||$nl=="user" ){?>active open<?php }?>"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs"></i> <span class="title">Tools</span> <span class="addindividual"></span> <span class="arrow addindividual"></span> </a>
    <ul class="sub-menu">
      <li class="nav-item  <?php if($nl=="show_adminprofile_id"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/adminprofile/show_adminprofile_id/1" class="nav-link "> <span class="title">administrator profile</span> </a> </li>
      <li class="nav-item  <?php if($nl=="show_settings_id"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/settings/show_settings_id/1" class="nav-link "> <span class="title">Settings</span> </a> </li>
      
       <li class="nav-item  <?php if($nl=="show_reset_pass"){?>active open<?php }?>"> <a href="<?php echo base_url();?>administrator/user/show_reset_pass/1" class="nav-link "> <span class="title">Change Password</span> </a> </li>
       
       <li class="nav-item  <?php if($nl=="logout"){?>active open<?php }?>"> <a href="<?php echo base_url(); ?>administrator/user/logout" class="nav-link "> <span class="title">Log out</span> </a> </li>  
</ul>
  </li> 
   <?php }?>
  
</ul>
