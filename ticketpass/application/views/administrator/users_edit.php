<?php //$this->load->view ('header');?>

<div class="page-container">
  <div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
      <?php $this->load->view ('administrator/sidebar');?>
    </div>
  </div>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="<?php echo base_url(); ?>administrator/user/dashboard">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>Admin Panel</span> </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Userer edit</div>
                    <div class="tools"> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php foreach($ecms as $i){?>
                    <form method="post" class="form-horizontal form-bordered" action="<?php echo base_url().'administrator/users/edit_users '?>" enctype="multipart/form-data">
                      <div class="form-body">
                        <input type="hidden" name="user_id" value="<?=$i->uid;?>">
                        <input type="hidden" name="user_image" value="<?=$i->imagename;?>">
                        <!--<div class="form-group">
                          <label class="control-label col-md-3">Profile Image</label>
                          <div class="col-md-7">
							<?php
                            $file = array('type' => 'file','name' => 'userfile','onchange' => 'readURL(this);');
                            echo form_input($file);
                            ?>
                            <div id='default_img'><img id="select" src="<?php echo base_url()?>uploads/profileimage/<?php echo $i->imagename;?>" alt="your image" style="width:150px;"/></div>
                          </div>
                        </div>-->
                        <div class="form-group last">
                                                <label class="control-label col-md-3">Profile Image</label>
                                                <div class="col-md-9">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <?php if($i->imagename==''){ ?>
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                                        <?php }else{?> 
                                                        	<img src="<?php echo base_url()?>/uploads/profileimage/<?php echo $i->imagename;?>" alt="" /> 
                                                        <?php }?>      
                                                            </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <!--<input type="file" name="...">-->
																	<?php
                                                                    $file = array('type' => 'file','name' => 'userfile','onchange' => 'readURL(this);');
                                                                    echo form_input($file);
                                                                    ?>
                                                            </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix margin-top-10">
                                                        <span class="label label-danger">Format</span> jpg, jpeg, png, gif </div>
                                                </div>
                                            </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Name</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="name" value="<?php echo $i->name;?>" name="name">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Phone</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="phone" value="<?php echo $i->phone;?>" name="phone">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Email</label>
                          <div class="col-md-5">
                            <input type="email" class="form-control" id="email" value="<?php echo $i->email;?>" name="email">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Password</label>
                          <div class="col-md-5">
                            <input type="password" class="form-control" id="password" value="<?php echo base64_decode($i->password);?>" name="password">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3">Username</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control" id="username" value="<?php echo $i->username;?>" name="username">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Address</label>
                          <div class="col-md-7">
                            <textarea class="form-control" name="address" rows="6" id="address"><?php echo $i->adrs;?></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Descriptions</label>
                          <div class="col-md-7">
                            <textarea class="form-control" name="description" rows="6" id="pagedes"><?php echo $i->description;?></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                    <label class="control-label col-md-3">User Type</label>
                    <div class="col-md-4">
                    <label class="control-label col-md-3">Organizer</label>  
                    <input type="radio" name="user_type" id="user_type" value="o" <?php if($i->user_type=="o") {?> checked="checked" <?php }?>> 
                    </div>
                    
                    <div class="col-md-4">
                    <label class="control-label col-md-3">User</label>  
                    <input type="radio" name="user_type" id="user_type" value="u" <?php if($i->user_type=="u"){ ?> checked="checked" <?php }?>>
                    </div>
                    </div>
                      </div>
                      <div class="form-actions">
                        <div class="row">
                          <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn red" id="submit" value="Submit" name="update">
                            <button class="btn default" type="button">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </form>
                    <?php }?>
                    <!-- END FORM-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
