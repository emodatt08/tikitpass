<?php //$this->load->view ('header');?>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript">
$(function() {
setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
$('#btnclick').click(function() {
$('#testdiv').show();
setTimeout(function() { $("#testdiv").fadeOut(1500); }, 5000)
})
})
</script>
<link href="<?php echo base_url(); ?>css/datepicker.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,300,500&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.9.1/highlight.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>js/datepicker.js"></script>
<script src="<?php echo base_url(); ?>js/datepicker.en.js"></script>
<!-- BEGIN CONTAINER -->
<div class="page-container">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar-wrapper">
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
      <!-- BEGIN SIDEBAR MENU -->
      <?php $this->load->view ('administrator/sidebar');?>
      <!-- END SIDEBAR MENU -->
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
  </div>
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>administrator panel</span> </li>
        </ul>
      </div>
      <!-- END PAGE BAR -->
      <!-- END PAGE HEADER-->
      <?php if (isset($success_msg)) { echo $success_msg; } ?>
      <div class="row">
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Add Ticket</div>
                    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form">
                  <?php 
                        $id = $this->uri->segment(4);
                        $this->db->select('*');
                        $this->db->from('ticket');
                        $this->db->where('event_id', $id);
                        $this->db->where('ticket_type',1);
                        $query = $this->db->get();
                        
                        $this->db->select('*');
                        $this->db->from('ticket');
                        $this->db->where('event_id', $id);
                        $this->db->where('ticket_type',0);
                        $query1 = $this->db->get();
                        ?>
                        <?php if($query1->num_rows > 0 &&  $query->num_rows > 0){?>
                         <div class="col-md-12" style="text-align:center; margin-top:10%;">
                         Ticket against this event is already generated !!!
                         </div>
                        <?php }else{?>
                    	<form action="<?php echo base_url().'administrator/ticket/add_ticket'?>" class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
                    <?php $id = $this->uri->segment(4);?>
                     <?php $tid = $this->uri->segment(5);?>
                    <input type="hidden" name="eventid" value="<?=$id?>">
                    <input type="hidden" name="tid" value="<?=$tid?>">
                      <div class="form-body">
                        	<div class="form-group">
                          <label class="control-label col-md-3">Ticket Type</label>
                        <div class="col-md-8">
                        <div class="col-md-4" <?php if($query1->num_rows > 0 ){?> style="display:none;" <?php }?>> 
                        <label>Free Ticket</label>
                        <input type="radio"  name="ticket_type" value="0" id="ft">
                        </div>
                        <div class="col-md-4" <?php if($query->num_rows > 0 ){?> style="display:none;"<?php }?>>
                        <label>Paid Ticket</label>
                        <input type="radio"  name="ticket_type" value="1" id="pt">
                        </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-3">Ticket Name</label>
                        <div class="col-md-8"> 
                      
                        <?php echo form_input(array('id' => 'ticket_name', 'name' => 'ticket_name', 'class'=>'form-control' )); ?> 
                        <?php echo form_error('ticket_name'); ?> 
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-3">Ticket Price</label>
                        <div class="col-md-8"> 
                      
                        <?php echo form_input(array('id' => 'ticket_price', 'name' => 'ticket_price', 'class'=>'form-control' )); ?> 
                        <?php echo form_error('ticket_price'); ?> 
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Ticket Quantity</label>
                        <div class="col-md-8"> 
                        <?php echo form_input(array('id' => 'ticket_quantity', 'name' => 'ticket_quantity','class'=>'form-control' )); ?> 
                        <?php echo form_error('ticket_quantity'); ?> </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-3">Ticket Description</label>
                        <div class="col-md-8"> 
                        <?php echo form_textarea(array('id' => 'pagedes', 'name' => 'ticket_description','class'=>'form-control' )); ?> 
                        <?php echo form_error('ticket_description'); ?> </div>
                        </div>
                      	</div>
                        <div class="form-actions">
                        <div class="row">
                        <div class="col-md-offset-3 col-md-9"> 
						
                        <input type="submit" id="submit" name="Submit" value="Submit" class="btn red" <?php if($query1->num_rows > 0 &&  $query->num_rows > 0){?> style="display:none;" <?php }?>/>
                        <button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
                        </div>
                        </div>
                        </div>
                       
                        </form>
                        <?php }?>
                    	<!-- END FORM-->
                  		</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <!-- END QUICK SIDEBAR -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#ft").click(function(){
		$("#ticket_price").hide();	
		$("#ticket_priceft").show();
		$("#ticket_priceft").val("0");
		
    });
    $("#pt").click(function(){
		//alert("jjjj");
        $("#ticket_price").show();	
		$("#ticket_priceft").hide();
		
    });
});
</script>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
