<style>
#sample_1_filter {
	padding: 8px;
	float: right;
}
#sample_1_length {
	padding: 8px;
}
#sample_1_info {
	padding: 8px;
}
#sample_1_paginate {
	float: right;
	padding: 8px;
}
</style>
<!-- BEGIN CONTAINER -->
<div class="page-container">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
      <!-- BEGIN SIDEBAR MENU -->
      <?php $this->load->view ('administrator/sidebar');?>
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
  </div>
  <!-- END SIDEBAR -->
  <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <!-- BEGIN PAGE BAR -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <a href="<?php echo base_url(); ?>user/dashboard">Home</a> <i class="fa fa-circle"></i> </li>
          <li> <span>administrator Panel</span> <i class="fa fa-circle"></i> </li>
          <li> <span>Show Ticket</span> </li>
        </ul>
      </div>
      <!-- END PAGE HEADER-->
      <?php if (isset($success_msg)) { echo $success_msg; } ?>
      <div class="row">
		<?php if($this->session->flashdata('success_add')!=''){?>
        	<div class="alert alert-success alert-dismissable" style="padding:10px;">
        		<button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>
        		<strong> <?php echo $this->session->flashdata('success_add');?></strong>
        	</div>
        <?php }?>
		<?php if($this->session->flashdata('success_update')!=''){?>
            <div class="alert alert-success alert-dismissable" style="padding:10px;">
            	<button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>
            	<strong> <?php echo $this->session->flashdata('success_update');?></strong>
            </div>
        <?php }?> 
		<?php if($this->session->flashdata('success_delete')!=''){?>
        	<div class="alert alert-success alert-dismissable" style="padding:10px;">
        		<button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>
        		<strong> <?php echo $this->session->flashdata('success_delete');?></strong>
        	</div>
        <?php }?>    
        <div class="col-md-12">
          <div class="tabbable-line boxless tabbable-reversed">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_0">
                <div class="portlet box blue-hoki">
                  <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-gift"></i>Show Ticket</div>
                    <div class="tools"> <a href="javascript:;" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="javascript:;" class="reload"> </a> <a href="javascript:;" class="remove"> </a> </div>
                  </div>
                  <div class="portlet-body form" style="padding:5px;clear:both; float:left; width:100%; margin-top:5px;">
                    	<div class="col-md-6">
                        	<table style="width:100%" cellpadding="2" cellspacing="2">	
                            	<tr>
                                	<td colspan="2" style="background-color:#36C6D3; padding:6px;color:#FFF;">Free Ticket</td>
                                </tr>
                                <tr style="border-bottom:1px solid #CCC;">
                                	<td>Total Free Ticket</td>
                                    <td align="center"><?php foreach($queryft as $ft){ echo $ft->ticket_quantity;}?></td>
                                </tr>
                                <tr style="border-bottom:1px solid #CCC;">
                                	<td>Free Sales Till Date</td>
                                    <td align="center"><?php foreach($queryfs as $fs){ echo $fs->freesales;}?></td>
                                </tr>
                                <tr style="border-bottom:1px solid #CCC;">
                                	<td>Ticket Available</td>
                                    <td align="center">
                                    <?php 
										foreach($queryft as $ft){ 
											 $ft->ticket_quantity;
											foreach($queryfs as $fs){ 
												echo  ($ft->ticket_quantity - $fs->freesales);
											}
										}
									?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                        	<table style="width:100%" cellpadding="2" cellspacing="2">	
                            	<tr>
                                	<td colspan="4" style="background-color:#36C6D3; padding:6px; color:#FFF;">Paid Ticket</td>
                                </tr>
                                <tr style="border-bottom:1px solid #CCC;">
                                	<td>Total Paid Ticket</td>
                                    <td align="center"><?php foreach($querypt as $pt){ echo $pt->ticket_quantity;}?></td>
                                    <td>Price</td>
                                    <td><?php foreach($querypt as $pt){ echo $pt->ticket_price;}?></td>
                                </tr>
                                <tr style="border-bottom:1px solid #CCC;">
                                	<td style="width:32%;">Paid Sales Till Date</td>
                                    <td align="center"><?php foreach($queryps as $fs){ echo $fs->paidsales;}?></td>
                                    <td>Total Earn</td>
                                    <td>
										<?php 
											foreach($querysm as $sm){ 
												if($sm->sumamt==''){
													echo '0';
												}else{
													echo $sm->sumamt;
												}
											}
										?>
                                    </td>
                                </tr>
                                <tr style="border-bottom:1px solid #CCC;">
                                	<td>Ticket Available</td>
                                    <td align="center">
                                    <?php 
										foreach($querypt as $pt){ 
											 $ft->ticket_quantity;
											foreach($queryps as $ps){ 
												echo  ($pt->ticket_quantity - $ps->paidsales);
											}
										}
									?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END CONTENT BODY -->
  </div>
  <!-- END CONTENT -->
  <!-- BEGIN QUICK SIDEBAR -->
  <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
