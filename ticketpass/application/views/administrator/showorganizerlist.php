<?php //$this->load->view ('header');?>
<style>
#sample_1_filter{
	padding: 8px;
    float: right;
	}
#sample_1_length{
	padding: 8px;
	}
#sample_1_info{
	padding: 8px;	
	}
#sample_1_paginate{
	float: right;
    padding: 8px;
	}	
</style>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <?php $this->load->view ('administrator/sidebar');?>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="<?php echo base_url();?>administrator">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>administrator Panel</span>
                                 <i class="fa fa-circle"></i>
                            </li>
                            
                             <li>
                                <span>Show Banners </span>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
					
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <div class="row">
                    <?php 
					if($this->session->flashdata('success_message')!=''){
					?>
                    <div class="alert alert-success text-center"><?php echo $this->session->flashdata('success_message')?></div>
                    <?php }?>
                        <div class="col-md-12">
                        <?php if (isset($success_msg)) { echo $success_msg; } ?>
                            <div class="tabbable-line boxless tabbable-reversed">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box blue-hoki">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                 
                                                    <i class="fa fa-gift"></i>Show Banner</div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                            <table class="table table-striped table-bordered table-hover table-checkable order-column dt-responsive" id="sample_1">
                                                <thead>
                                                    <tr>
                                                        <th width="110" style="max-width:98px;">Image</th>
                                                        <th width="35">Name</th>
                                                        <th width="55">Phone</th>
                                                        <th width="55">Email</th>
                                                        <th width="55">Website</th>
                                                        <th width="55">Address</th>
                                                        <th width="20">Status</th>
                                                        <th width="35">Edit</th>
                                                        <th width="35">Delete</th>
                                                    </tr>
                                                </thead>
                                            <tbody>
                                            <?php if(is_array($eorg)): ?>
                                            <?php
                                            foreach($eorg as $i){
                                            ?>
                                                <tr class="table table-striped table-bordered table-hover table-checkable order-column dt-responsive" id="sample_1">
                                                    <td width="110"><?php if($i->organizer_avatar==""){?>No Image<?php }else{?><img src="<?php echo base_url()?>/uploads/organizer/<?php echo $i->organizer_avatar;?>" width="100" height="100" style="border: 5px solid #dddddd;"/><?php }?></td>
                                                    <td  style="max-width:200px;"><?php echo $i->organizer_name;?></td>
                                                    <td  style="max-width:250px;"><?php echo $i->organizer_phone;?></td>
                                                    <td  style="max-width:250px;"><?php echo $i->organizer_email;?></td>
                                                    <td  style="max-width:250px;"><?php echo $i->organiser_website;?></td>
                                                    <td  style="max-width:250px;"><?php echo $i->organizer_address;?></td>
                                                    <td  style="max-width:200px;"><div class="form-group">
                                                    <div class="col-md-5">
                                                    <?php if($i->status==1){?><span style="color:#060;">Active</span><?php }else{?><span style="color:#900;">Inactive</span><?php }?>
                                                   </div>
                                                    </div>
                                                    </td>
                                                    <td style="max-width:50px;"><a class="btn green btn-sm btn-outline sbold uppercase" href="<?php echo base_url()?>administrator/organizer/show_organizer_id/<?php echo $i->organizer_id; ?>">Edit</a></td>
                                                    <td style="max-width:50px;"><a class="btn red btn-sm btn-outline sbold uppercase" onclick="return confirm('Are you sure about this delete?');" href="<?php echo base_url()?>administrator/organizer/delete_organizer/<?php echo $i->organizer_id;?>">Delete</a></td>
                                                </tr>
                                            <?php }?>
                                            <?php endif; ?>
                                            
                                            </tbody>
                                            </table>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            
            <!-- END QUICK SIDEBAR -->
        </div>
		<script>
			function f1(stat,id)
	{
		
		//alert(stat);
		//alert(id);
		$.ajax({
                    type:"get",
                    url:"<?php echo base_url(); ?>administrator/banner/statusbanner",
                     data:{stat : stat, id :id}
					 
					  });
		//$.get('<?php echo base_url(); ?>banner/statusbanner',{ stat : stat , id : id });
	}
</script>
        <!-- END CONTAINER -->
<?php //$this->load->view ('footer');?>
        