<?php
    $this->db->from('category');
    $this->db->where('parent_id', 0);
    $menu_list = $this->db->get();

    foreach ($menu_list->result() as $menu): ?>
        <li class="nav-parent">
            <a href="<?php echo $menu->category_id?>"><?php echo ucwords($menu->category_name) ?></a> <em class="fa">&#xf107;</em>
            <?php
            $cat_id = $menu->category_id;
            $this->db->from('category');
            $this->db->where('parent_id', $cat_id);
            $submenu_list = $this->db->get();

            ?>
            <ul>
                <?php foreach ($submenu_list->result() as $submenu): ?>
                    <li>
                        <a href="<?php echo $submenu->category_id?>"><?php echo ucwords($submenu->category_name) ?></a> <em class="fa">&#xf105;</em><em class="fa">&#xf105;</em>
                        <?php //================================?>
                    	<?php
							$sub_cat_id = $submenu->category_id;
							$this->db->from('category');
							$this->db->where('parent_id', $sub_cat_id);
							$subsubmenu_list = $this->db->get();
						?>
                            <ul>
                                <?php foreach ($subsubmenu_list->result() as $subsubmenu): ?>
                                    <li>
                                        <a href="<?php echo $subsubmenu->category_id?>"><?php echo ucwords($subsubmenu->category_name) ?></a>
                                        	 <ul>
                                             	<?php
													$subpcat = $subsubmenu->category_id;
													$this->db->from('property');
													$this->db->where('pid', $subpcat);
													$location_list = $this->db->get();
												 ?>
                                                 <?php foreach ($location_list->result() as $locname): ?>
                                      			<li><a href="<?php echo base_url()?>property/propertydetails/<?php echo $locname->id?>"> <?php echo $locname->Property_name?></a></li>
                                                <?php endforeach; ?>
                                      		</ul>
                                    </li>
                                <?php endforeach; ?>
                                
                            </ul>
                    <?php //================================?>
                    </li>
                    
                <?php endforeach; ?>
            </ul>
        </li>
    <?php endforeach; ?>
   <?php /*?> <?php foreach ($menu_list->result() as $menu): ?>
   <li class="nav-parent">
        <a href="<?php echo $menu->category_id?>"><?php echo ucwords($menu->category_name) ?> <em class="fa">&#xf107;</em> </a>
        <ul>
            <?php foreach ($submenu_list->result() as $submenu): ?>
                <li>
                    <a href="<?php echo $submenu->category_id?>"><?php echo ucwords($submenu->category_name) ?> <em class="fa">&#xf107;</em></a>
                </li>
                
            <?php endforeach; ?>
        </ul>
    </li>
<?php endforeach; ?><?php */?>
