<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//include_once (dirname(__FILE__) . "/my_controller.php");
class Profile extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	$this->load->library(array('form_validation','session'));
			if(!$this->session->userdata('is_userlogged_id')){
			redirect('login', 'refresh');
        }
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
	$this->load->database();
	$this->load->model('contact_model');
	$this->load->model('profile_model');
	$this->load->model('home_model');
}
	public function index(){
		//$query=$this->contact_model->select_map();
		//$data['map'] = $query;
		$data['title'] = "Contact";
	    $this->load->view('header',$data);
		$this->load->view('contact',$data);
		$this->load->view('footer',$data);
	}
	
	function myprofile(){
		$this->load->library(array('form_validation','session'));
			if(!$this->session->userdata('is_userlogged_id')){
			redirect('login', 'refresh');
        }
		$uid = $this->session->userdata('is_userlogged_id'); 
		//echo $email = $this->session->userdata('email'); exit();
		$is_admin = $this->session->userdata('isAdmin');
		$team_id = $this->session->userdata('teamId');

		$result=$this->profile_model->profiledetails($uid);
		$data['profile']=$result;
		
		
		@$eventid = $result[0]->event_id;
		//==========Ticket Generation============
		$result=$this->profile_model->ticket($uid);
		
		$data['ticket']=$result;
		//echo $this->db->last_query(); exit();
		$data['num_rows_tkt'] = count(@$data['ticket']);
		@$eventid = $result[0]->event_id;
		$result=$this->profile_model->eventdet($eventid); 
		$data['eventdet']=$result;
		//==========Ticket Generation============
		$data['title'] = "Profile";
		$this->load->view('header',$data);
		$this->load->view('profile',$data);
		$this->load->view('footer');
	}
	
	function changepassword(){
		$this->load->library(array('form_validation','session'));
			if(!$this->session->userdata('is_userlogged_id')){
			redirect('login', 'refresh');
        }
		$uid = $this->session->userdata('is_userlogged_id');
		$this->load->model('profile_model');
		$query = $this->profile_model->user_details($uid);
		$data['info'] = $query;
		$data['title'] = "Change Password";
		$this->load->view('header',$data);
		$this->load->view('changepassword',$data);
		$this->load->view('footer');
	}
	
	function reset_pass(){
		$this->load->library(array('form_validation','session'));
			if(!$this->session->userdata('is_userlogged_id')){
			redirect('login', 'refresh');
        }
		//$id = $this->uri->segment(3);
			$datalist = array(			
				'password' => base64_encode($this->input->post('new_pass'))
				);
			$uid = $this->session->userdata('is_userlogged_id');
			$this->load->database();
			//Calling Model
			$this->load->model('profile_model');
			//Transfering data to Model
			$query = $this->profile_model->new_pass($datalist,$uid);
			$data['pass'] = $query;
			$this->session->set_flashdata('pass_change', 'Password Changed Successfully !!!!');
			redirect('profile/changepassword');
		   }
	
	function bookedticket(){
		
		$this->load->library(array('form_validation','session'));
			if(!$this->session->userdata('is_userlogged_id')){
			redirect('login', 'refresh');
        }
		$uid = $this->session->userdata('is_userlogged_id'); 
		//echo $email = $this->session->userdata('email'); exit();
		$is_admin = $this->session->userdata('isAdmin');
		$team_id = $this->session->userdata('teamId');

		$result=$this->profile_model->profiledetails($uid);
		$data['profile']=$result;
		
		
		//==========Ticket Generation============
		$result=$this->profile_model->ticket($uid);
		$data['ticket']=$result;
		//$data['num_rows_tkt'] = $ticket;
		$data['num_rows_tkt'] = count($data['ticket']);
		@$eventid = $result[0]->event_id;
		$result=$this->profile_model->eventdet($eventid); 
		$data['eventdet']=$result;
		//==========Ticket Generation============
		$data['title'] = "Booked Ticket";
		$this->load->view('header',$data);
		$this->load->view('bookedticket',$data);
		$this->load->view('footer');
	}
	
	function editprofile(){
		$this->load->library(array('form_validation','session'));
			if(!$this->session->userdata('is_userlogged_id')){
			redirect('login', 'refresh');
        }
		$uid = $this->session->userdata('is_userlogged_id');
		$this->load->model('profile_model');
		$query = $this->profile_model->user_details($uid);
		$data['info'] = $query;
		$data['title'] = "Edit Profile";
		$this->load->view('header',$data);
		$this->load->view('editprofile',$data);
		$this->load->view('footer');
	}
	
	function edit_pro(){
				$datalist = array(			
					'name' => $this->input->post('name'),
				    'email' => $this->input->post('email'),
				    'phone' => $this->input->post('phone'),
					'adrs' => $this->input->post('adrs'),
					'description' => $this->input->post('description')
				);
				$id = $this->input->post('uid');
				$data['title'] = "Edit Profile";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('profile_model');
				//Transfering data to Model
				$query = $this->profile_model->pro_edit($id,$datalist);
				$this->session->set_flashdata('edit_pro', 'Profile Edited Successfully !!!!');
				redirect('profile/editprofile');			
			
		}
	
	function changeprofileimage(){
		$oldimg = $this->input->post('oldimg');
		$uid = $this->session->userdata('is_userlogged_id'); 
		$config = array(
			'upload_path' => "uploads/profileimage/",
			'upload_url' => base_url() . "uploads/profileimage/",
			'allowed_types' => "gif|jpg|png|jpeg"
			);
        $this->load->library('upload', $config);
		if ($this->upload->do_upload('userfile')){
			@unlink("uploads/profileimage/".$oldimg);
			$data['img'] = $this->upload->data();
			$datalist = array(			
				'imagename' => $data['img']['file_name'],
				);
			$this->load->database();
		//Calling Model
		$this->load->model('profile_model');
		$query = $this->profile_model->change_pro_pic($uid,$datalist);
		$this->session->set_flashdata('propic_change', 'Profile Picture Updated Successfully !!!!');
		redirect('profile/myprofile');
			
		}else{
		$this->session->set_flashdata('propic_change', 'error!!!!');
		redirect('profile/myprofile');
	}
	}
}
?>