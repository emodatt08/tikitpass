<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Organizerlogin extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	$this->load->database();
	$this->load->library('session');
	$this->load->model('organizerlogin_model');
	$this->load->model('home_model');
}
	public function index(){
		$data['title'] = "Organizer Login";
	    $this->load->view('headerlogin',$data);
		$this->load->view('organizerlogin',$data);
		$this->load->view('footerlogin',$data);
	}
	
	  //==================================Form validation============================================
	public function login_validation(){
		$this->load->library('session');
		$this->load->model('organizerlogin_model');
		$query = $this->organizerlogin_model->user_login($this->input->post('organizer_email'),base64_encode($this->input->post('organizer_password')));
		if(@$query[0]->organizer_email!=''){
		$user_status = $query[0]->status;
		$organizer_email = $query[0]->organizer_email;
		$organizer_id = $query[0]->organizer_id;
		if($query==true && $user_status==1){
			$sessiondata=array(
				'organizer_email'=>$organizer_email,
				'organizer_id'=>$organizer_id,
				'is_userlogged_in' => 1
			);
			$this->session->set_userdata('logged',$sessiondata);
			//print_r($sessiondata);
			$this->session->set_userdata('is_userlogged_in',$organizer_email);
			$this->session->set_userdata('is_userlogged_in',$organizer_id);
			redirect('organizerdashboard',$sessiondata);
		}else{
			$data['title'] = "Ticket Pass Login";
			$data['error'] = "Invalid Username or Password !!!";
			$this->load->view('headerlogin',$data);
			$this->load->view('oragnizerlogin',$data);
			$this->load->view('footerlogin',$data);
		}
		}else{
			$this->session->set_flashdata('logerror', 'Invalid Username or Password !!!');
			redirect('organizerlogin','refresh');
		}
	}
	//================================Form Validation==========================================
	//================================Validating administrator credentials against database============
	public function validate_credentials(){
		$this->load->model('login_model');
		if($this->model_users->can_log_in()){
			return true;
		}else{
			$this->form_validation->set_message('validate_credentials','Incorrect Username or password');
		return false;
		}
	}
	//==============================Validating administrator credentials against database=============
		public function is_logged_in(){
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $is_logged_in = $this->session->userdata('logged_in');
        if(!isset($is_logged_in) || $is_logged_in!==TRUE){
            redirect('administrator/login');
        }
    }
	//==============================Logout==============================================
    public function logout(){
		$this->session->sess_destroy();
		redirect('organizerlogin/organizerlogin');
	}
	//==============================Logout==============================================

}
?>