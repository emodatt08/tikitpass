<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//include_once (dirname(__FILE__) . "/my_controller.php");
class Contact extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	$this->load->database();
	$this->load->model('contact_model');
	$this->load->model('home_model');
}
	public function index(){
		$query=$this->contact_model->select_map();
		$data['map'] = $query;
		$data['title'] = "Contact";
	    $this->load->view('header',$data);
		$this->load->view('contact',$data);
		$this->load->view('footer',$data);
	}
	
	function send_mail(){ 
	  
            $this->form_validation->set_rules('ContactName','ContactName','required');
			$this->form_validation->set_rules('Subject','Subject','required');
			$this->form_validation->set_rules('Phone','Phone','required');
			$this->form_validation->set_rules('Email', 'Email', 'required|valid_email|is_unique[user_contact.Email]');
			$this->form_validation->set_rules('Message','Message','required');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			 if($this->form_validation->run()==FALSE){
						 
				 $this->session->set_flashdata("email_sent","Email Allready Exists !!!"); 
		         redirect('contact');
				
}else{	
         $to_email = "info@ticketpass.com"; 
         $ContactName = $this->input->post('ContactName');
		 $from_email = $this->input->post('Email');
		 $Phone = $this->input->post('Phone');
		 //$subject = $this->input->post('subject'); 
		 $Message = $this->input->post('Message');
   
   			  $data = array(
					'ContactName' => $this->input->post('ContactName'),
					'Email' => $this->input->post('Email'),
					'Subject' => $this->input->post('Subject'),
					'Phone' => $this->input->post('Phone'),
					'Message' => mysql_real_escape_string(strip_tags($this->input->post('Message'))),
					'ContactDate'=> date('Y-m-d H:i:s'),
					'ReplyStatus' => 'No'
				);
         //Load email library 
         $this->load->library('email'); 
   
         $this->email->from($from_email); 
         $this->email->to($to_email);
        $this->email->subject('query or question');
		//$this->email->subject('query or question'); 
		 $message.=$this->email->message($ContactName);
		 $message.=$this->email->message($Phone); 
         $message.=$this->email->message($Message); 
   
         //Send mail 
         if($this->email->send()){ 
		 $this->contact_model->insert_contact($data);
		 $this->session->set_flashdata("email_sent","Email sent successfully !!!"); 
		redirect('contact');
		 }else{ 
		 $this->contact_model->insert_contact($data);
         $this->session->set_flashdata("email_sent","Error in sending Email."); 
		 redirect('contact');
         
		 }
   }  
   
    }
}
?>