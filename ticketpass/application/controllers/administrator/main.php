<?php
class main extends CI_Controller
{
public function index(){
 $this->login();
}
  public function login() {
   $data['title'] = "administrator Login";
   $this->load->view('administrator/headerlogin');
   $this->load->view('administrator/login_view',$data);
   $this->load->view('administrator/footerlogin');
  }
  //==================================Form validation===========================================
	public function login_validation(){
		$this->load->model('administrator/users_model');
		$this->load->database();
		$query = $this->users_model->admin_login($this->input->post('username'),md5($this->input->post('password')),$this->input->post('log_type'));
		$this->session->set_userdata($this->input->post('log_type'));
		$username = $this->session->userdata('UserName');
		if($query==true){
			$sessiondata=array(
				'username'=>$this->input->post('username'),
				'log_type'=>$this->input->post('log_type'),
				'is_logged_in' => 1
			);
			$this->session->set_userdata('is_logged_in',1);
			$this->session->set_userdata('logged',$sessiondata);
			redirect('administrator/home',$sessiondata);
		}else{
			$data['title'] = "Ticket Pass administrator Login";
			$data['error'] = "Invalid Username or password !!!";
			$this->load->view('administrator/headerlogin',$data);
			$this->load->view('administrator/login_view',$data);
			$this->load->view('administrator/footerlogin',$data);
		}
	}
	//================================Form Validation==========================================
	//================================Validating administrator credentials against database============
	public function validate_credentials(){
		$this->load->model('administrator/model_users');
		if($this->model_users->can_log_in()){
			return true;
		}else{
			$this->form_validation->set_message('validate_credentials','Incorrect Username or password');
		return false;
		}
	}
	//==============================Validating administrator credentials against database=============
	//==============================administrator reset Password======================================
	public function is_logged_in(){
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $is_logged_in = $this->session->userdata('logged_in');
        if(!isset($is_logged_in) || $is_logged_in!==TRUE){
            redirect('administrator/login');
        }
    }
	public function reset_password(){
			$this->load->model('administrator/model_users');
			$this->form_validation->set_rules('old_password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('newpassword', 'New Password', 'required|matches[re_password]');
			$this->form_validation->set_rules('re_password', 'Retype Password', 'required');
			if($this->form_validation->run() == FALSE){
				$data['title'] = "administrator Reset Password";
				$sessionData = $this->session->userdata('is_logged_in');
				$this->data['id'] = $sessionData['id'];
				$id=$this->input->post('id');
				$this->data['UserName'] = $sessionData['UserName'];
				$this->load->view('administrator/header',$data);
				$this->load->view('administrator/resetpassword', $data);
				$this->load->view('administrator/footer');
			}else{
				$this->load->database();
				 $id=$this->input->post('id');
				 //echo $id; 
				$query = $this->model_users->checkOldPass(md5($this->input->post('old_password')),$id);
				$data['oldpass'] = $query;
				//echo $this->db->last_query(); exit();
				if($query){
				$this->load->model('administrator/users_model');
				$query = $this->users_model->show_pass();
				$data['pass'] = $query;
				if($query){
					echo $id=$this->input->post('id');
					$query = $this->model_users->saveNewPass(md5($this->input->post('newpassword')),$id);
					//echo $this->db->last_query(); exit();
					if($query){
						redirect('administrator/main/reset_success');
					}else{
						redirect('administrator/main/change_password');
					}
				}
			}else{
						redirect('administrator/main/change_password');
				}
		}
	}
		
		function reset_success(){
			$data['title'] = "administrator Reset Password";
			$datamsg['titlemessage'] = "Password Reset Successfully";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/resetpassword', $datamsg);
			$this->load->view('administrator/footer');
		}
		
		function change_password(){
			$data['title'] = "administrator Reset Password";
			$datamsg['titlemessage'] = "Old Password does not match";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/resetpassword', $datamsg);
			$this->load->view('administrator/footer');
		}
	
	//==============================administrator reset Password======================================
	
	//==============================administrator Logout==============================================
    public function logout(){
		$this->session->sess_destroy();
		redirect('administrator/main/login');
	}
	//==============================administrator Logout==============================================

}



?>