<?php
class Ad extends CI_Controller {
		//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('administrator/home', 'refresh');
			}
			$this->load->model('administrator/ad_model');
			$this->load->library('image_lib');
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			
			//****************************backtrace prevent*** END HERE*************************
			
		}
		//============Constructor to call Model====================
		function index(){
			if($this->session->userdata('is_logged_in')){
				redirect('administrator/adadd_view');
			}else{
				$this->load->view('administrator/main/login');	
			}
		}
		
		
		//=======================Insert Page Data============
		function add_ad(){
			$config = array(
			'upload_path' => "ad/",
			'upload_url' => base_url() . "ad/",
			'allowed_types' => "gif|jpg|png|jpeg"
			);
			
			//load upload class library
        	$this->load->library('upload', $config);
				//=====================+++++++++++++++++++++++===================
				$this->form_validation->set_rules('ad_heading','Advertisement Heading', 'required');
				$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
				//=====================+++++++++++++++++++++++===================
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('administrator/header');
					$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';
					$this->load->view('administrator/header');
            		$this->load->view('administrator/adadd_view',$data);
					$this->load->view('administrator/footer');
					//redirect('/add',$data);
				}else{
					if (!$this->upload->do_upload('userfile')){
            			$data['success_msg'] = '<div class="alert alert-success text-center">You Must Select An Image File!</div>';
						$this->load->view('administrator/header');
            			$this->load->view('administrator/adadd_view', $data);
						$this->load->view('administrator/footer');
        			}else{
						 $data['userfile'] = $this->upload->data();
						 $filename=$data['userfile']['file_name'];
						 $data = array(
							'ad_heading' => $this->input->post('ad_heading'),
							'ad_link' => $this->input->post('ad_link'),
							'ad_img' => $filename,
							'status' => 1
						);
						$this->ad_model->insert_ad($data);
            			$upload_data = $this->upload->data();
						$query = $this->ad_model->show_ad();
						$data['ead'] = $query;
            			$data['success_msg'] = '<div class="alert alert-success text-center">Your file <strong>' . $upload_data['file_name'] . '</strong> was successfully uploaded!</div>';
						$this->load->view('administrator/header',$data);
						$this->load->view('administrator/showadlist',$data);
						$this->load->view('administrator/footer');
				
					}
				}
		}
		//=======================Insert Page Data============
  		//=======================Insertion Success message=========
		function success(){
			$data['h1title'] = 'Data Inserted Successfully';
			$data['title'] = 'Add Advertisement';
			$this->load->view('administrator/header');
			$this->load->view('administrator/add_view',$data);
			$this->load->view('administrator/footer');
		}
		//=======================Insertion Success message=========	
		
		
		//================Add  form=============
		function addad(){
			
			$data['title'] = "Ad add";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/adadd_view');
			$this->load->view('administrator/footer');
		}
		//================View Individual Data List=============
		
		//================View Individual Data List=============
  		//================Show Individual by Id=================
		function show_ad_id($id) {
			 $id = $this->uri->segment(4); 
			//exit();
			$data['title'] = "Edit Advertisement";
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/ad_model');
			//Transfering data to Model
			$query = $this->ad_model->show_ad_id($id);
			
			$data['ead'] = $query;
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/ad_edit', $data);
			$this->load->view('administrator/footer');
		}
		
   		//================Show Individual by Id=================
  	 	//================Update Individual ====================
		function edit_ad(){
			
			 //============================================
		 $ad_img = $this->input->post('ad_img');
			  
			 $config = array(
				'upload_path' => "ad/",
				'upload_url' => base_url() . "ad/",
				'allowed_types' => "gif|jpg|png|jpeg"
			);
			$this->load->library('upload', $config);
			if ($this->upload->do_upload("userfile")) {
				//echo $path = base_url(). "/";exit();
				//echo $path1 = "/"; 
				@unlink("ad/".$ad_img);
				
				//echo $image_data = $this->upload->data();
				$data['img'] = $this->upload->data();
				//*********************************
				//============================================
				$datalist = array(			
				'ad_heading' => $this->input->post('ad_heading'),
				'ad_link' => $this->input->post('ad_link'),
				'ad_img' => $data['img']['file_name']
				);
				//print_r($datalist); exit();
				$_img = $this->input->post('ad_img');
				//====================Post Data===================
				
				$id = $this->input->post('ad_id');
				$data['title'] = "Advertisement Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/ad_model');
				//Transfering data to Model
				$query = $this->ad_model->ad_edit($id,$datalist,$ad_img);
				$data1['message'] = 'Data Update Successfully';
				$query = $this->ad_model->show_adlist();
				$data['ead'] = $query;
				$data['title'] = "Advertisement Page List";
				$this->load->view('administrator/header',$data);
				$this->load->view('administrator/showadlist', $data1);
				$this->load->view('administrator/footer');
				//*********************************
		
			}else{
				$datalist = array(			
				'ad_heading' => $this->input->post('ad_heading'),
				'ad_link' => $this->input->post('ad_link')
				);
			
				
				//====================Post Data===================
				$ad_img = $this->input->post('ad_img');
				$id = $this->input->post('ad_id');
				$data['title'] = "Advertisement Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/ad_model');
				//Transfering data to Model
				$query = $this->ad_model->ad_edit($id,$datalist,$ad_img);
				$data1['message'] = 'Data Update Successfully';
				$query = $this->ad_model->show_adlist();
				$data['ead'] = $query;
				$data['title'] = "Advertisement Page List";
				$this->load->view('administrator/header',$data);
				$this->load->view('administrator/showadlist', $data1);
				$this->load->view('administrator/footer');
			}
			
		}
		//================Update Individual ====================
  		//=======================Update Success message=========
		function successupdate(){
			//loading database
			$this->load->database();
			//Calling Model
			$this->load->model('ad_model');
			//Transfering data to Model
			$query = $this->ad_model->view_ad();
			$data['ead'] = $query;
			$data['title'] = "Individual Data List";
			$datamsg['h1title'] = 'Data Updated Successfully';
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/showadlist',$datamsg);
			$this->load->view('administrator/footer');
		}
		//=======================Update Success message=========

		
		//======================Show CMS========================
		function showad(){
		
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/ad_model');
			//Transfering data to Model
			$query = $this->ad_model->show_ad();
			$data['ead'] = $query;
			$data['title'] = "Advertisement List";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/showadlist');
			$this->load->view('administrator/footer');
		
	}
		//======================Show CMS========================
		
		//=====================DELETE ====================
		
			function delete_ad() {
			$id = $this->uri->segment(4);
			$result=$this->ad_model->show_ad_id($id);
			//print_r($result);
			$ad_img = $result[0]->ad_img; 
			//echo $_img;exit();
			//Loading Database
			$this->load->database();

			//Transfering data to Model
			$query = $this->ad_model->delete_ad($id,$ad_img);
			$data['ead'] = $query;
			$this->load->view('administrator/header',$data);
			//$this->load->view('showlist', $data);
			redirect('administrator/ad/showad');
			$this->load->view('administrator/footer');
		}

		//=====================DELETE ====================
		
		//======================Logout==========================
		public function Logout(){
        	$this->session->sess_destroy();
        	redirect('administrator/login');
    	}
		//======================Logout==========================
}

?>

