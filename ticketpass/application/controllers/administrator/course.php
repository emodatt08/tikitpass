<?php
class Course extends CI_Controller {
		//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('administrator/home', 'refresh');
			}
			$this->load->model('administrator/course_model');
			$this->load->library('image_lib');
				//****************************backtrace prevent*** START HERE*************************
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			
			//****************************backtrace prevent*** END HERE*************************
		}
		//============Constructor to call Model====================
		function index(){
		if($this->session->userdata('is_logged_in')){
			redirect('administrator/courseadd_view');
        }else{
        	$this->load->view('administrator/main/login');	
        }
	}
  //=======================Insert Page Data============
		function add_course(){
			$my_date = date("Y-m-d", time()); 
			 $config = array(
				'upload_path' => "course/",
				'upload_url' => base_url() . "course/",
				'allowed_types' => "gif|jpg|png|jpeg|pdf"
			);
			 $this->load->library('upload', $config);
			 if ($this->upload->do_upload("userfile")) {
				 //echo $image_data = $this->upload->data();
				 $data['img'] = $this->upload->data();
			 	 $data['img']['file_name'];
				//exit();
				//*********************************
				//============================================
				
				$data = array(
					'gallery_name' => $this->input->post('gallery_name'),
					'gallery_image' => $data['img']['file_name'],
					'gallery_status' => 1,
					'date' =>  $my_date
				);
				//Transfering data to Model
				$this->gallery_model->insert_gallery($data);
				$data1['message'] = 'Data Inserted Successfully';
				redirect('administrator/gallery/success');
			 }
			 else{
				   	$data = array(
					'gallery_name' => $this->input->post('gallery_name'),
					'gallery_image' => $data['img']['file_name'],
					'gallery_status' => 1,
					'date' =>  $my_date
				);
				//Transfering data to Model
				$this->gallery_model->insert_gallery($data);
				$data1['message'] = 'Data Inserted Successfully';
				redirect('administrator/gallery/success');
			}
		}
         //=======================Insert Page Data============
  		
		//================Add banner form=============
		function addcategoryview(){
			$data['title'] = "Gallery add";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/categoryadd_view');
			$this->load->view('administrator/footer');
		}
		//====================Insert Page Data============
		//====================Add Category=================
		function add_news(){
			//Validating Name Field
			$this->form_validation->set_rules('category_name', 'Category Name', 'required|min_length[1]');
			if ($this->form_validation->run() == FALSE) {
			$data['success_msg'] = '<div class="alert alert-success text-center">Category Name Can Not Be Blank</div>';
			$data['title'] = "Add Category";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/categoryadd_view',$data);
			$this->load->view('footer');
			}else {
			//Setting values for tabel columns
			$data = array(
				'day' => $this->input->post('day'),
				'entry_time' => $this->input->post('entry_time'),
				'exit_time' => $this->input->post('exit_time'),
				'status' => 1
			 );
			
			//Transfering data to Model
			$this->time_model->insert_time($data);
			$data1['message'] = 'Data Inserted Successfully';
			redirect('time/view_time');
			
			}
		}
		//====================Add Category=================
		function add_category(){
			$my_date = date("Y-m-d", time()); 
			$config = array(
			'upload_path' => "uploads/category/",
			'upload_url' => base_url() . "uploads/category/",
			'allowed_types' => "gif|jpg|png|jpeg"
			);
        	$this->load->library('upload', $config);
				//=====================+++++++++++++++++++++++===================
				$this->form_validation->set_rules('category_name','Category Name', 'required');
				$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
				//=====================+++++++++++++++++++++++===================
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('administrator/header');
					$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';
					$this->load->view('administrator/header');
            		$this->load->view('administrator/categoryadd_view',$data);
					$this->load->view('administrator/footer');
					//redirect('banner/addbanner',$data);
				}else{
					if (!$this->upload->do_upload('userfile')){
            			$data['success_msg'] = '<div class="alert alert-success text-center">You Must Select An Image File!</div>';
						$this->load->view('administrator/header');
            			$this->load->view('administrator/categoryadd_view', $data);
						$this->load->view('administrator/footer');
        			}else{
						 $data['userfile'] = $this->upload->data();
						 $filename=$data['userfile']['file_name'];
						 $data = array(
							'category_name' => $this->input->post('category_name'),
							'parent_id' => $this->input->post('parent_id'),
							'news_image' => $filename,
							'news_desc' => $this->input->post('news_desc'),
							'date' => $my_date,
							'news_status' => 1
						);
						$this->news_model->insert_news($data);
            			$upload_data = $this->upload->data();
						$query = $this->news_model->show_news();
						$data['ecms'] = $query;
            			$data['success_msg'] = '<div class="alert alert-success text-center">Your file <strong>' . $upload_data['file_name'] . '</strong> was successfully uploaded!</div>';
						$this->load->view('administrator/header',$data);
						$this->load->view('administrator/shownewslist',$data);
						$this->load->view('administrator/footer');
				
					}
				}
		
		}
		//=======================Insert Page Data============
		//================View Individual Data List=============
		
		//================View Individual Data List=============
  		//================Show Individual by Id=================
		function show_gallery_id($id) {
			 $id = $this->uri->segment(4); 
			$data['title'] = "Edit Gallery";
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/gallery_model');
			//Transfering data to Model
			$query = $this->gallery_model->show_gallery_id($id);
			$data['ecms'] = $query;
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/gallery_edit', $data);
			$this->load->view('administrator/footer');
		}
    	function statusgallery (){
	 	$stat= $this->input->get('stat'); 
	 	$id= $this->input->get('id');   
		$this->load->model('administrator/gallery_model');
		$this->gallery_model->updt($stat,$id);
		}
   		//================Show Individual by Id=================
  	 	//================Update Individual ====================
	function edit_gallery(){
			 //============================================
			 $config = array(
				'upload_path' => "gallery/",
				'upload_url' => base_url() . "gallery/",
				'allowed_types' => "gif|jpg|png|jpeg"
			);
			$this->load->library('upload', $config);
			if ($this->upload->do_upload("userfile")) {
				
				//echo $image_data = $this->upload->data();
				$data['img'] = $this->upload->data();
			 	//echo $data['img']['file_name'];
				//exit();
				//*********************************
				//============================================
				
				$datalist = array(			
				'gallery_name' => $this->input->post('gallery_name'),
				'gallery_image' => $data['img']['file_name']
				
				);
				$old_file = $this->input->post('old_file');
				$id = $this->input->post('gallery_id');
				$data['title'] = "Gallery Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/gallery_model');
				//Transfering data to Model
				$query = $this->gallery_model->gallery_edit($id,$datalist,$old_file);
				// echo $ddd=$this->db->last_query();
				
				$data1['success_msg1'] = '<div class="alert alert-success text-center"> <button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>Data Updated Successfully.....</div>';
				$query = $this->gallery_model->show_gallerylist();
				$data['ecms'] = $query;
				$data['title'] = "Gallery Page List";
				$this->load->view('administrator/header',$data);
				$this->load->view('administrator/showgallerylist', $data1);
				$this->load->view('administrator/footer');
				//*********************************
		
			}else{
				$datalist = array(			
				'gallery_name' => $this->input->post('gallery_name')
				
				);
				//echo $gallery_name=$_POST['gallery_name'];
				//echo $ddd=$this->db->last_query();
				//print_r($datalist);
				//exit();
				//====================Post Data===================
				$old_file = $this->input->post('gallery_image');
				$id = $this->input->post('gallery_id');
				$data['title'] = "Gallery Edit";
				//loading database
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/gallery_model');
				//Transfering data to Model
				$query = $this->gallery_model->gallery_edit($id,$datalist,$old_file);
				//echo $ddd=$this->db->last_query();
				//exit();
				$data1['success_msg1'] = '<div class="alert alert-success text-center"> <button class="close" aria-hidden="true" data-dismiss="alert" type="button" style="right:0;"></button>Data Updated Successfully.....</div>';
				$query = $this->gallery_model->show_gallerylist();
				$data['ecms'] = $query;
				$data['title'] = "Gallery Page List";
				$this->load->view('administrator/header',$data);
				$this->load->view('administrator/showgallerylist', $data1);
				$this->load->view('administrator/footer');
			}
			
		}
	//================Update Individual ====================
    //======================Show CMS========================
	function show_gallery(){
		
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/gallery_model');
			//Transfering data to Model
			$query = $this->gallery_model->show_gallery();
			$data['ecms'] = $query;
			$data['title'] = "Gallery List";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/showgallerylist');
			$this->load->view('administrator/footer');
		
	}
	
	function delete_gallery($id){
			//Loading  Database
			$this->load->database();
			$this->load->model('administrator/gallery_model');
			////echo $id;
			//exit();
			//Transfering data to Model
			$id = $this->uri->segment(4);
			//echo $id; exit();
			$result=$this->gallery_model->gallery_view($id); 
			//print_r($result); exit();
			$gallery_image = $result[0]->gallery_image;
			$this->gallery_model->delete_gallery($id,$gallery_image);
			//$data['emember'] = $query; 
			//echo $this->db->last_query(); exit();
			$data1['message'] = 'Data Deleted Successfully'; 
			redirect('administrator/gallery/successdelete');
		}
	//======================Delete teacher===============
	//======================Show CMS========================
		
	function addform(){
		$data['title'] = "Add Gallery ";
		$this->load->view('administrator/header',$data);
		$this->load->view('administrator/galleryadd_view');
		$this->load->view('administrator/footer');
	}
		//======================Logout==========================
	function logout(){
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('administrator/home', 'refresh');
	}
		//======================Logout==========================
}

?>
