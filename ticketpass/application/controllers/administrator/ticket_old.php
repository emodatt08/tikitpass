<?php
class Ticket extends CI_Controller {
			//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('administrator/home', 'refresh');
			}
			$this->load->model('administrator/ticket_model');
			$this->load->database();
			$this->load->library('image_lib');
			//****************************backtrace prevent*** START HERE*************************
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			
		//****************************backtrace prevent*** END HERE*************************
		}
		//============Constructor to call Model====================
		function index(){
		if($this->session->userdata('is_logged_in')){
			redirect('ticket/ticket_view');
        }else{
        	$this->load->view('administrator/main/login');	
        }
	}
  	
	//*********===============Event Section===============********//
		function addticketview(){
			$id = $this->uri->segment(4);
			$data['title'] = "Add Ticket";
			
			$query = $this->ticket_model->show_type();
			$data['type'] = $query;
			$query1 = $this->ticket_model->show_category();
			$data['category'] = $query1;
			
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/ticketadd_view',$data);
			$this->load->view('administrator/footer');
		}
		//====================Insert Page Data===============
		//====================Add Event Type=================
		function add_ticket_id(){
			$id = $this->uri->segment(4);
			$query1 = $this->ticket_model->check_paidfree($id);
			//echo $query1;
			$data['checkfree'] = $query1;
			
			$query = $this->ticket_model->check_paid($id);
			//echo $query;
			$data['checkpaid'] = $query;
			
			//exit();
			$this->load->view('administrator/header');
			$data['success_msg'] = '';
			$this->load->view('administrator/header');
			$this->load->view('administrator/ticketadd_view',$data);
			$this->load->view('administrator/footer');
		}
		//====================Add Event Type=================
		//====================Add Event Ticket===============
		function add_ticket(){
			$id = $this->uri->segment(4);
			$this->form_validation->set_rules('ticket_price','Ticket Price', 'required');
				$this->form_validation->set_rules('ticket_quantity', 'Ticket Quantity', 'required');
				$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
				//=====================+++++++++++++++++++++++===================
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('administrator/header');
					$data['success_msg'] = '<div class="alert alert-success text-center">Some Fields Can Not Be Blank</div>';
					$this->load->view('administrator/header');
            		$this->load->view('administrator/ticketadd_view',$data);
					$this->load->view('administrator/footer');
					//redirect('banner/addbanner',$data);
				}else{
					 $data = array(
							'event_id' => $this->input->post('eventid'),
							'ticket_quantity' => $this->input->post('ticket_quantity'),
							'ticket_price' => $this->input->post('ticket_price'),
							'ticket_type' => $this->input->post('ticket_type'),
							'ticket_no' =>$this->input->post('tid').random_string('alnum', 16).$this->input->post('eventid'),
							'dateofadd' => date('Y-m-d H:i:s'),
							'status' => 1
						);
						$this->ticket_model->insert_ticket($data);
						$this->session->set_flashdata('success_add', 'Ticket Added Successfully !!!!');
						redirect('administrator/ticket/showticket/'.$this->input->post('eventid').'');
				
				}
		}
		//====================Add Event Ticket===============
		//==================Show Event Type==================
		function showticket(){
			$data['title'] = "Ticket List";
			$id = $this->uri->segment(4);
			$query = $this->ticket_model->show_ticket($id);
			//echo $this->db->last_query();
			//exit();
			$data['ticket'] = $query;
			$data['title'] = "Ticket List";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/ticketlist',$data);
			$this->load->view('administrator/footer');
		}
		//==================Show Event Type==================
		
		//================Show Ticket By Id================
		function show_ticket_id() {
		$id = $this->uri->segment(4); 
		$urlid = $this->uri->segment(5);
		$data['urlid']=$urlid;
		$data['title'] = "Edit Ticket";
		$query = $this->ticket_model->show_ticket_id($id);
		$data['eticket'] = $query;
		$this->load->view('administrator/header',$data);
		$this->load->view('administrator/ticket_edit', $data);
		$this->load->view('administrator/footer');
		}
		//================Show Ticket By Id================
		
		//==================Edit category====================
		function edit_ticket(){
		//============================================
				$datalist = array(			
				//**********************************************
				'ticket_quantity' => $this->input->post('ticket_quantity'),
				'ticket_price' => $this->input->post('ticket_price'),
				'status' => $this->input->post('status')
				//**********************************************
				);
				$id = $this->input->post('ticket_id');
				$data['title'] = "Ticket Edit";
				$this->load->database();
				//Calling Model
				$this->load->model('administrator/ticket_model');
				//Transfering data to Model
				$query = $this->ticket_model->ticket_edit($id,$datalist);
				$data1['message'] = 'Data Update Successfully';
				$query = $this->ticket_model->show_ticket($id);
				$this->session->set_flashdata('success_update', 'Ticket Updated Successfully !!!!');
				redirect('administrator/ticket/showticket/'.$this->input->post('urlid').'',TRUE);
			
		}
		

		//================Delete Category====================	
		function delete_ticket(){
			$id = $this->uri->segment(4);
			$urlid = $this->uri->segment(5);
			$result=$this->ticket_model->delete_ticket($id); 
			$this->session->set_flashdata('success_delete','Ticket Deleted Successfully !!!!');
			redirect('administrator/ticket/showticket/'.$urlid.'',TRUE);
		}
	//*********===============Event Section===============********//
	
	//======================Logout==========================
	function logout(){
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('administrator/home', 'refresh');
	}
		//======================Logout==========================
}

?>
