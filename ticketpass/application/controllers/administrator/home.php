<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {

 function __construct()
 {
   parent::__construct();
 }
public function is_logged_in(){
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        //header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        $is_logged_in = $this->session->userdata('logged_in');
        
        if(!isset($is_logged_in) || $is_logged_in!==TRUE){
            redirect('administrator/main/login');
        }
    }
 function index()
 {
	 
   if($this->session->userdata('is_logged_in')==1)
   {
     $session_data = $this->session->userdata('logged_in');
	  $this->session->userdata('is_logged_in');
     $data['UserName'] = $session_data['username'];
	 $data2['title'] = "Dashbord";
	 $this->load->view('administrator/header',$data2);
     $this->load->view('administrator/home_view', $data);
	 $this->load->view('administrator/footer',$data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('administrator/main', 'refresh');
   }
 }

 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('administrator/home', 'refresh');
 }

}

?>