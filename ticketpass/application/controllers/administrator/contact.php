<?php
class Contact extends CI_Controller {
		//============Constructor to call Model====================
		function __construct() {
			parent::__construct();
			$this->load->library(array('form_validation','session'));
			if($this->session->userdata('is_logged_in')!=1){
			redirect('administrator/home', 'refresh');
			}
			$this->load->model('administrator/contact_model');
			$this->load->library('image_lib');
				//****************************backtrace prevent*** START HERE*************************
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
			
			//****************************backtrace prevent*** END HERE*************************
		}
		//============Constructor to call Model====================
		function index()
	{
		if($this->session->userdata('is_logged_in')){
			redirect('administrator/showcontactlist');
        }else{
        	$this->load->view('administrator/login');	
        }
	}
//======================Show Blog List **** START HERE========================
		function show_contact(){
		
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/contact_model');
			$query = $this->contact_model->show_contact();
			$query1 = $this->contact_model->administratormail();
			$data['administratormail'] = $query1;
			//Transfering data to Model
			$query = $this->contact_model->show_contact();
			$data['ecms'] = $query;
			$data['title'] = "Contact List";
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/showcontactlist');
			$this->load->view('administrator/footer');
		
	}
//======================Show Blog List **** END HERE========================
//================Show Individual by Id for BLOG *** START HERE=================
		function show_contact_id($id) {
			 $id = $this->uri->segment(4); 
			//exit();
			$data['title'] = "View Contact";
			//Loading Database
			$this->load->database();
			//Calling Model
			$this->load->model('administrator/contact_model');
			//Transfering data to Model
			$query = $this->contact_model->show_contact_id($id);
			$data['ecms'] = $query;
			$this->load->view('administrator/header',$data);
			$this->load->view('administrator/contact_edit', $data);
			$this->load->view('administrator/footer');
		}
		
//================Show Individual by Id for BLOG *** END HERE=================

	function delete_row()
{

  //Loading  Database
           $id = $this->uri->segment(4); 

			$this->load->database();

			//Transfering data to Model
             $this->load->model('administrator/contact_model');
			$this->contact_model->delete_contact($id);
             	$this->load->database();
			//Calling Model
			$this->load->model('administrator/contact_model');
			//Transfering data to Model
			$query = $this->contact_model->show_contact();
			$data['ecms'] = $query;
			$this->session->set_flashdata('delete_message', 'Contact Deleted Successfully !!!!');
			redirect('administrator/contact/show_contact',TRUE);
			
}

//================FOR MAIL BACK==================
function send_contact(){
					
			   $date1 = date('d-m-Y g:i:s a');  
			 //  $this->is_logged_in();		
			   $this->form_validation->set_rules('reply_subject','Subject','required');			
			        if($this->form_validation->run()==FALSE){
                          $this->session->set_flashdata('success', 'Please Enter Reply Subject ');
			              redirect('administrator/contact/show_contact');
                      }
         else{
		 	    
               	$data = array(
					'reply_subject' => $this->input->post('reply_subject'),
					'ReplyMessage' => $this->input->post('ReplyMessage'),
					'ReplyStatus' => 'Yes',
					'ReplyDate' =>$date1
				);	
				//print_r($data);
				//exit;			
				$id=$this->input->post('ContactId');
				$this->contact_model->insert_contact($data,$id);
				//$this->load->model('contact_model');
			   // $query2 = $this->contact_model->show_user();
			   
			        $from_email = $this->input->post('administratormail');
					$to_email = $this->input->post('Email');
			
		 
		   $data1 = array(
					'reply_subject' => $this->input->post('reply_subject'),
					'ReplyMessage'=>$this->input->post('ReplyMessage'),
					'ReplyDate' =>$date1
										
				);
			$msg = $this->load->view('administrator/mailtemplate',$data1,TRUE);
		    $config['mailtype'] = 'html';
		    $this->load->library('email');
		    $this->email->initialize($config);
		    $msg = $msg;
		    $subject = 'Reply Message From Ticket Pass';   
              
          
			$this->email->from($this->input->post('administratormail'), 'Ticket Pass Administrator'); 
		    $this->email->to($to_email);
        // $this->email->to($to_email);
            $this->email->subject($subject); 
            $this->email->message($msg);
			   
            if($this->email->send()){
                $query = $this->contact_model->show_contact();
				$this->session->set_flashdata('success', 'Message Sent successfully!!!!');
			    redirect('administrator/contact/show_contact');
		        }
          else{          
		         $query = $this->contact_model->show_contact();
				 $this->session->set_flashdata('success', 'Email can not sent!!!!');
			     redirect('administrator/contact/show_contact');
	          }
	     }
				
	 }
//================FOR MAIL BACK==================

//======================Logout==========================
		public function Logout(){
        	$this->session->sess_destroy();
        	redirect('administrator/login');
    	}
//======================Logout==========================
}

?>
