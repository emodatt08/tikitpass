<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	$this->load->database();
	$this->load->library('session');
	$this->load->model('login_model');
	$this->load->model('profile_model');
	$this->load->model('home_model');
}
	public function index(){
		
		$data['title'] = "Login";
	    $this->load->view('headerlogin',$data);
		$this->load->view('login',$data);
		$this->load->view('footerlogin',$data);
	}
	
	  //==================================Form validation============================================
	public function login_validation(){
		$this->load->library('session');
		$this->load->model('login_model');
		$query = $this->login_model->user_login($this->input->post('email'),base64_encode($this->input->post('password')));
		if(@$query[0]->email!=''){
		$user_status = $query[0]->userstatus;
		$email = $query[0]->email;
		$uid = $query[0]->uid;
		if($query==true && $user_status==1){
			$this->session->set_userdata('logged',$session_data);
			$session_data = array(
				'email'=>$email,
				'uid'=>$uid,
				'is_userlogged_in' => 1
			);
		// Add user data in session
		$this->session->set_userdata('logged_in', $session_data);
		//$this->session->set_userdata('is_userlogtype',$user_type);
		$this->session->set_userdata('is_userlogged_in',$email);
		$this->session->set_userdata('is_userlogged_id',$uid);
		/*$result=$this->profile_model->profiledetails($email);
		$data['profile']=$result;*/
		redirect('profile/myprofile',$session_data);
		}else{
			$data['title'] = "Ticket Pass Login";
			$data['error'] = "Invalid Username or Password !!!";
			$this->load->view('headerlogin',$data);
			$this->load->view('login',$data);
			$this->load->view('footerlogin',$data);
		}
		}else{
			$this->session->set_flashdata('logerror', 'Invalid Username or Password !!!');
			redirect('login','refresh');
		}
	}
	//================================Form Validation==========================================
	//================================Validating administrator credentials against database============
	public function validate_credentials(){
		$this->load->model('login_model');
		if($this->model_users->can_log_in()){
			return true;
		}else{
			$this->form_validation->set_message('validate_credentials','Incorrect Username or password');
		return false;
		}
	}
	//==============================Validating administrator credentials against database=============
		public function is_logged_in(){
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $is_logged_in = $this->session->userdata('logged_in');
        if(!isset($is_logged_in) || $is_logged_in!==TRUE){
            redirect('administrator/login');
        }
    }
	//==============================Logout==============================================
    public function logout(){
		$this->session->sess_destroy();
		redirect('login/login');
	}
	//==============================Logout==============================================

}
?>