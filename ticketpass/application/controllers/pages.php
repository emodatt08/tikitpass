<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pages extends CI_Controller {
 function __construct() {
	parent::__construct();
	$this->load->database();
	$this->load->model('home_model');
}
	function about(){
		$this->load->database();
		$this->load->model('home_model');
		$query = $this->home_model->show_cms_id_2();
		$data['cms2'] = $query;
		$data['title'] = "About Us";
		$this->load->view('header',$data);
		$this->load->view('about',$data);
		$this->load->view('footer',$data);
	}
	
		function whatwedo(){
		$this->load->database();
		$this->load->model('home_model');
		$query = $this->home_model->show_cms_id_6();
		$data['cms3'] = $query;
		$data['title'] = "What We Do";
		$this->load->view('header',$data);
		$this->load->view('whatwedo',$data);
		$this->load->view('footer',$data);
	}
	
	function career(){
		$this->load->database();
		$this->load->model('home_model');
		$query = $this->home_model->show_cms_id_5();
		$data['cms4'] = $query;
		$data['title'] = "Career";
		$this->load->view('header',$data);
		$this->load->view('career',$data);
		$this->load->view('footer',$data);
	}
	
	function help(){
		$this->load->database();
		$this->load->model('home_model');
		$query = $this->home_model->show_cms_id_4();
		$data['cms5'] = $query;
		$data['title'] = "Help";
		$this->load->view('header',$data);
		$this->load->view('help',$data);
		$this->load->view('footer',$data);
	}
}
?>
