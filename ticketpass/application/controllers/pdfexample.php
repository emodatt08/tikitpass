<?php 
    class Pdfexample extends CI_Controller{
          function __construct() { 
     	  parent::__construct();
		  $this->load->model('event_model');
		  $this->load->model('home_model');
     	} 
		function index(){
			$this->load->library('Pdf');
			
			$hotquery = $this->event_model->show_my_event($eventid);
			$data['event']=$hotquery;
			$data['location']=$hotquery[0]->location;
			$data['start_date_time'] = date('L, M d, Y',strtotime($hotquery[0]->start_date_time));
			$data['from_time'] = date('H:i A',strtotime($hotquery[0]->start_date_time));
			$data['to_time'] = date('H:i A',strtotime($hotquery[0]->start_date_time));
			$data['price'] = $hotquery[0]->event_price;
		    $data['title']="Test pdf";
			$this->load->view('pdfticket',$data);
		}
 }
?>