<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//include_once (dirname(__FILE__) . "/my_controller.php");
class Eventdetails extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	//$this->load->model('home_model');
	$this->load->database();
	$this->load->model('home_model');
	$this->load->model('eventdetails_model');
}
	public function index(){
		
		$data['title'] = "Event Details";
	    $this->load->view('header',$data);
		$this->load->view('eventdetails',$data);
		$this->load->view('footer',$data);
	}
	
	public function show_event_details(){
		$eventid = $this->uri->segment(3);
		
		//======================For Paid Ticket========================
		$query =$this->eventdetails_model->show_ticketcount($eventid);
		$data['ticketquantity']= $query;
		@$paidticketquantity = $query[0]->ticket_quantity;
		//$data['num_rows_booking'] = count($data['ticketquantity']);
		$data['num_rows_booking'] = $paidticketquantity;
		//echo $this->db->last_query(); exit();
		
		$query =$this->eventdetails_model->show_ticketbookedcount($eventid);
		$data['ticketbookedquantity']= $query;
		$data['num_rows_ticket_booked'] = count($data['ticketbookedquantity']);
		//echo $this->db->last_query(); exit();
		//=======================For Paid Ticket========================
		
		//========================For Free Ticket========================
		
		$query =$this->eventdetails_model->show_ticketcountfree($eventid);
		$data['ticketquantityfree']= $query;
		@$freeticketquantity = $query[0]->ticket_quantity;
		//$data['num_rows_bookingfree'] = count($data['ticketquantityfree']);
		$data['num_rows_bookingfree'] = $freeticketquantity;
		//echo $this->db->last_query();
		
		$query =$this->eventdetails_model->show_ticketbookedcountfree($eventid);
		$data['ticketbookedquantityfree']= $query;
		$data['num_rows_ticket_bookedfree'] = count($data['ticketbookedquantityfree']);
		//echo $this->db->last_query(); exit();
		
		//========================For Free Ticket========================
		
		$query =$this->eventdetails_model->show_eventdetails($eventid);
		$data['eventdetails']= $query;
		$data['num_rows'] = count($data['eventdetails']);
		@$ticketeventid = $query[0]->event_id;
		
		$query = $this->eventdetails_model->show_ticketdet($ticketeventid);
		$data['ticketdet']= $query;
		
		$query = $this->eventdetails_model->show_ticketdetails($ticketeventid);
		$data['ticketdetails']= $query;
		
		$data['title'] = "Event Details";
	    $this->load->view('header',$data);
		$this->load->view('eventdetails',$data);
		$this->load->view('footer',$data);
		}
		
		function paysuccess(){
			$data['title']="Payment Success";
			$data['topsuccess']="Thank You For Your Payment";
			$data['bottomsuccess']="Please Check Your Account To Download Your Ticket";
			$this->load->view('header',$data);
			$this->load->view('paysuccess',$data);
			$this->load->view('footer');
		}
		
		function payreject(){
			$data['title']="Payment Unsuccessful";
			$data['topsuccess']="Your Payment Can Not Be Accept Now";
			$data['bottomsuccess']="Please Try Again later For Buying The Ticket";
			$this->load->view('header',$data);
			$this->load->view('payreject',$data);
			$this->load->view('footer');
		}
}
