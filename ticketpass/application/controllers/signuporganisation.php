<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//include_once (dirname(__FILE__) . "/my_controller.php");
class Signuporganisation extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	//$this->load->model('home_model');
	$this->load->database();
		$this->load->model('organisation_model');
		$this->load->model('home_model');
		//$this->load->model('administrator/banner_model');
}
	public function index(){
		$data['title'] = "Signup";
	    $this->load->view('headerlogin',$data);
		$this->load->view('signuporganisation',$data);
		$this->load->view('footerlogin',$data);
	}
	
	function registration(){
		$date1 = date('d-m-Y g:i:s a');  
		$this->form_validation->set_rules('organizer_name','Organizer Name','required');
		$this->form_validation->set_rules('organizer_phone','Organizer Phone','required');
		$this->form_validation->set_rules('organizer_email', 'Organizer Email', 'required|valid_email|is_unique[organizer.organizer_email]');
		$this->form_validation->set_rules('organizer_password','Password','trim|required');
		$this->form_validation->set_rules('organizer_conf_password','Confirm Password','trim|required|matches[organizer_password]');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata("message","Email  Allready Exists !!!"); 
			redirect('signuporganisation');
		}else{
			$data = array(
				'organizer_name' => $this->input->post('organizer_name'),
				'organizer_email' => $this->input->post('organizer_email'),
				'organizer_phone' => $this->input->post('organizer_phone'),
				'organizer_password' => base64_encode($this->input->post('organizer_password')),
				'added_date'=> date('Y-m-d H:i:s'),
				'status' => '1'
			);	
			$query = $this->organisation_model->register($data);
				//echo $this->db->last_query(); exit();
			$from_email = "info@tikitpass.com";
			$to_email = $this->input->post('email');
			$data1 = array(
				'organizer_name' => $this->input->post('organizer_name'),
				'organizer_email' => $this->input->post('organizer_email'),
				'organizer_phone' => $this->input->post('organizer_phone'),
				'organizer_password' => base64_encode($this->input->post('organizer_password')),
				'added_date'=> date('Y-m-d H:i:s')
			);
			$msg = $this->load->view('mailtemplateorganisation',$data1,TRUE);
		    $config['mailtype'] = 'html';
		    $this->load->library('email');
		    $this->email->initialize($config);
		    $msg = $msg;
		    $subject = 'Registration Success Message From TicketPass.';   
            $this->email->from($from_email, 'TicketPass Administrator'); 
		    $this->email->to($to_email);
            $this->email->subject($subject); 
            $this->email->message($msg);
			   
            if($this->email->send()){
				$this->session->set_flashdata("message", "<b style='color:green;'>You Have Registered Successfully !!!</b>");
			    redirect('signuporganisation');
		        }else{          
				 $this->session->set_flashdata("message", "<b style='color:green;'>You Have Registered Successfully !!!</b>");
			     redirect('signuporganisation');
	          }
	     }
	 }
}
?>