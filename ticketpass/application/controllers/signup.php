<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Signup extends CI_Controller{ 
 function __construct() {
	parent::__construct();
	$this->load->database();
		$this->load->model('signup_model');
		$this->load->model('home_model');
}
	public function index(){
		
		$data['title'] = "Signup";
	    $this->load->view('headerlogin',$data);
		$this->load->view('signup',$data);
		$this->load->view('footerlogin',$data);
	}
	
	function registration(){
		$date1 = date('d-m-Y g:i:s a');  
		$this->form_validation->set_rules('name','Name','trim|required');
		$this->form_validation->set_rules('phone','Phone','trim|required|max_length[12]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[user.email]');
		$this->form_validation->set_rules('password','Password','trim|required');
		$this->form_validation->set_rules('conf_password','Confirm Password','trim|required|matches[password]');
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata("email_sent"," <b style='color:red;'>Email Allready Exists. Please try another one!</b>"); 
			redirect('signup');
		}else{
			$data = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'password' => base64_encode($this->input->post('password')),
				'dateofjoin'=> date('Y-m-d H:i:s'),
				'userstatus' => '1'
			);	
			$query = $this->signup_model->register($data);
			//echo "kkkk".$this->db->last_query(); exit();
			$from_email = "info@tikitpass.com";
			$to_email = $this->input->post('email');
			 $data1 = array(
						'name' => $this->input->post('name'),
						'email' => $this->input->post('email'),
						'phone' => $this->input->post('phone'),
						'password' => base64_encode($this->input->post('password')),
						'dateofjoin'=> date('Y-m-d H:i:s')
				   );
			$msg = $this->load->view('mailtemplate',$data1,TRUE);
		    $config['mailtype'] = 'html';
		    $this->load->library('email');
		    $this->email->initialize($config);
		    $msg = $msg;
		    $subject = 'Registration Success Message From TicketPass.';   
            $this->email->from($from_email, 'TicketPass Administrator'); 
		    $this->email->to($to_email);
            $this->email->subject($subject); 
            $this->email->message($msg);
			
            if($this->email->send()){
               //$query = $this->signup_model->register($data);
				$this->session->set_flashdata("success", "<b style='color:green;'>You Have Registered Successfully !!!</b>");
			    redirect('signup');
		        }
          else{          
		        // $query = $this->signup_model->register($data);
				 $this->session->set_flashdata("success", "<b style='color:blue;'>You Have Registered Successfully !!!</b>");
			     redirect('signup');
	          }
	     }
				
	 }
}



?>