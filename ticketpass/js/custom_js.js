// JavaScript Document

$(document).ready(function(){

/*----------menu-hover-in-desktop----------------*/
if($(window).width()>769){
        $('.navbar .dropdown').hover(function() {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

        }, function() {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

        });

        $('.navbar .dropdown > a').click(function(){
            location.href = this.href;
        });

    }


/*--------------Animation---------------------------*/
wow = new WOW(
{
boxClass:     'wow',
animateClass: 'animated',
offset:       100
}
);
wow.init();








})